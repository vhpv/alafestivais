<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['libraries'] = array('worker','session','database','cart','encrypt');
$autoload['helper'] = array('url','html','file','number','messages','multibanco','estados','utf8','reservas');
$autoload['config'] = array('admin_config');
$autoload['language'] = array();
$autoload['model'] = array('configuracoes_model');