<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->data['title']			= $this->configuracoes_model->get_site('title')->value;
		$this->data['description']		= $this->configuracoes_model->get_site('description')->value;
		$this->data['keywords']			= $this->configuracoes_model->get_site('keywords')->value;
		setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );
		date_default_timezone_set('Europe/Lisbon');
    }

	function cliente_logged_in(){
		$cliente_logged_in = $this->session->userdata('cliente_logged_in');
        if(!isset($cliente_logged_in) || $cliente_logged_in != true) {
			$data = array('title' => 'Login');
			redirect('/home/login');
        }
    }
	
	function admin_logged_in(){
		$admin_logged_in = $this->session->userdata('admin_logged_in');
        if(!isset($admin_logged_in) || $admin_logged_in != true) {
			$data = array('title' => 'Login');
			redirect('/admin/home/login');
        }
    }
	
	function visitante_logged_in(){
		$visitante_logged_in = $this->session->userdata('visitante_logged_in');
        if(!isset($visitante_logged_in) || $visitante_logged_in != true) {
			redirect('/');
        }
    }

	function notifica_admin($assunto='',$mensagem='')
	{
		$header = 	"From: Ala Festivais - WEBMASTER <no-reply@alafestivais.com>\r\n".
					"X-Priority: 1\r\n" .
					"Priority: Urgent\r\n";
		$mensagem .="\n\nIP: " . $this->input->ip_address() . "\tHora: " . date('Y-m-d H:i:s');
		mail('vitorvilela@sapo.pt',$assunto,$mensagem,$header);	
	}
	
	function protected_area($assunto='',$mensagem='')
	{
		$this->notifica_admin($assunto,$mensagem);
		redirect('/');
		exit('No direct script access allowed');
	}	
}