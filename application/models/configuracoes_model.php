<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Configuracoes_model extends CI_Model {
	
	function get_site($campo) {
		$this->db->where('item','site_' . $campo);
		$query = $this->db->get('configuracoes');
		return $query->row();
	}
	
	function update_site() {
		foreach ($_POST as $key => $value) {
			$valores = array(
				'value' => $value
			);
			$this->db->update('configuracoes', $valores, array('item' => $key));
		}
    }
	
	function editar($item,$data)
	{	
		$this->db->where(array('item'=> $item));
		$this->db->update('configuracoes', $data);
	}
}