<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Cidades_model extends CI_Model {
	
	var $tabela = 'cidades';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get()
	{
		$this->db->order_by("nome","asc");
		$query = $this->db->get($this->tabela);
		return $query->result();
	}

	function get_by_id($id)
	{
		$query = $this->db->get_where($this->tabela, array(
			'id'			=> $id
			)
		);
		return $query->row();
	}
	
	function get_by_evento($id){
		/*
		
		SELECT distinct c.nome
FROM autocarros as a
inner join autocarros_cidades as ac on (a.id = ac.id_autocarro)
inner join cidades as c on(c.id = ac.id_cidade)
WHERE a.id_evento = 135
order by ac.id asc
		
		*/
		$this->db->select('cidades.id,cidades.nome');
		$this->db->from('autocarros');
		$this->db->join('autocarros_cidades', 'autocarros_cidades.id_autocarro = autocarros.id','inner');
		$this->db->join('cidades', 'cidades.id = autocarros_cidades.id_cidade','inner');
		$this->db->where("autocarros.id_evento",$id);
		$this->db->group_by('cidades.id');
		$query = $this->db->get();
		return $query->result();
	}

	function adicionar($data)
	{		
		$this->db->insert($this->tabela, $data);
	}

	function editar($id,$data)
	{	
		$this->db->where(array('id'=> $id));
		$this->db->update($this->tabela, $data);
	}
	
	function apagar($data) {
		$this->db->delete($this->tabela, $data); 
	}
}