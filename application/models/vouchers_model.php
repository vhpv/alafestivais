<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Vouchers_model extends CI_Model {
	
	var $tabela 			= 'vouchers';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get(){
		$query = $this->db->get($this->tabela);
		return $query->result();
	}
	
	function get_by_id($id)
	{
		$query = $this->db->get_where($this->tabela, array(
			'id'			=> $id
			)
		);
		return $query->row();
	}
	
	function get_by_code($code){
		$this->db->where('code',$code);
		$query = $this->db->get($this->tabela);
		return $query->row();
	}

	function add($data)
	{		
		$this->db->insert($this->tabela, $data);
		return $this->db->insert_id();
	}

	function update($id,$data)
	{	
		$this->db->where(array('id'=> $id));
		$this->db->update($this->tabela, $data);
	}
	
	function delete($data) {
		$this->db->delete($this->tabela, $data); 
	}
}