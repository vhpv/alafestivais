<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Eventos_model extends CI_Model {
	
	var $tabela 			= 'eventos';
	var $tabela_detalhes	= 'eventos_cidades';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get($filho=false,$arquivo=false)
	{
		$this->db->select('cidades.nome as nome_cidade, '.$this->tabela.'.*');
		$this->db->from($this->tabela);
		$this->db->join('cidades', 'cidades.id = '.$this->tabela.'.id_cidade');
		$this->db->order_by($this->tabela.".id","desc");
		if($filho) $this->db->where($this->tabela.".evento_pai",0);
		if(!$arquivo) $this->db->where($this->tabela.".arquivo",0);
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_by_id($id)
	{
		$this->db->select('cidades.nome as nome_cidade, '.$this->tabela.'.*');
		$this->db->from($this->tabela);
		$this->db->join('cidades', 'cidades.id = '.$this->tabela.'.id_cidade');
		$this->db->where($this->tabela.".id",$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function get_eventos_pai()
	{
		$query = $this->db->get_where($this->tabela, array(
			'evento_pai'			=> 1
			)
		);
		return $query->result();
	}
	
	function get_main_eventos()
	{
		$this->db->order_by("id","desc");
		$query = $this->db->get_where($this->tabela, array(
			'evento_pai_id'			=> 0
			)
		);
		return $query->result();
	}
	
	function get_by_id_pai($id)
	{
		$query = $this->db->get_where($this->tabela, array(
			'evento_pai_id'			=> $id
			)
		);
		return $query->result();
	}
	
	function get_slides()
	{
		$query = $this->db->get_where($this->tabela, array(
			'slide'			=> 1
			)
		);
		return $query->result();
	}
	
	function get_activos()
	{
		$query = $this->db->get_where($this->tabela, array(
			'active'			=> 1
			)
		);
		return $query->result();
	}
	
	function get_main_eventos_activos()
	{
		$query = $this->db->get_where($this->tabela, array(
			'active'			=> 1,
			'evento_pai_id'		=> 0
			)
		);
		return $query->result();
	}
	
	function get_historico()
	{
		$query = $this->db->get_where($this->tabela, array(
			'portfolio'			=> 1
			)
		);
		return $query->result();
	}

	function adicionar($data)
	{		
		$this->db->insert($this->tabela, $data);
		return $this->db->insert_id();
	}

	function editar($id,$data)
	{	
		$this->db->where(array('id'=> $id));
		$this->db->update($this->tabela, $data);
	}
	
	function apagar($data) {
		$this->db->delete($this->tabela, $data); 
	}
	
	/* DETALHES */
	
	function get_detalhes_by_id($id)
	{
		$query = $this->db->get_where($this->tabela_detalhes, array(
			'id_ec'			=> $id
			)
		);
		return $query->row();
	}
	
	function get_detalhes_by_cidade_evento($evento,$cidade)
	{
		$query = $this->db->get_where($this->tabela_detalhes, array(
			'id_evento'			=> $evento,
			'id_cidade'			=> $cidade
			)
		);
		return $query->result();
	}
	
	function get_detalhes_by_cidade_evento_row($evento,$cidade)
	{
		$query = $this->db->get_where($this->tabela_detalhes, array(
			'id_evento'			=> $evento,
			'id_cidade'			=> $cidade
			)
		);
		return $query->row();
	}
	
	function adicionar_detalhe($data)
	{		
		$this->db->insert($this->tabela_detalhes, $data);
	}
	
	function get_cidades_by_evento($id)
	{
		$this->db->select('*');
		$this->db->from($this->tabela_detalhes);
		$this->db->join('cidades', 'cidades.id = '.$this->tabela_detalhes.'.id_cidade');
		
		$this->db->where($this->tabela_detalhes.'.id_evento',$id);
		$this->db->order_by($this->tabela_detalhes.".id_ec","asc");
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_cidades_disponiveis_by_evento($id)
	{
		$this->db->select('cidades.id, cidades.nome');
		$this->db->distinct();
		$this->db->from('autocarros');
		$this->db->join('autocarros_cidades', 'autocarros.id = autocarros_cidades.id_autocarro');
		$this->db->join('cidades', 'cidades.id = autocarros_cidades.id_cidade');
		
		$this->db->where('autocarros.id_evento',$id);
		$this->db->order_by("autocarros_cidades.id_ac","asc");
		$query = $this->db->get();
		return $query->result();
	}
	
	function apagar_detalhe($data) {
		$this->db->delete($this->tabela_detalhes, $data);
	}
	
		
	function apagar_cidades_by_evento($data) {
		$this->db->delete($this->tabela_detalhes, $data); 
	}
}