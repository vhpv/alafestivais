<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {
	
	var $users_table		= 'admin_users';
	
	/* CHECKS */
	function check_username($email){
		$this->db->where('email',$email);
		return $this->db->get($this->users_table)->num_rows();
	}

	function validar_login($email,$password) {
		$this->db->where('email',$email);
		$this->db->where('password',md5($this->encrypt->sha1($password).$this->encrypt->sha1($password)));
		$query = $this->db->get($this->users_table);
		if($query->num_rows() == 1){
			return true;
		} else {
			return false;
		}
	}
	
	function update_lastlogin() {
		$this->db->where('email',$this->input->post('email'));
		$this->db->where('password',md5($this->input->post('password')));
		$valores = array(
						 'last_login' => date("Y-m-d H:i:s") 
						 );
		$this->db->update($this->users_table, $valores);
	}
	
	function dados_utilizador($email) {
		$query = $this->db->get_where($this->users_table, array(
			'email' => $email
														   )
									 );
		return $query->row();
	}
	
	function get_paged_list($limit = 10, $offset = 0){
		$this->db->order_by('id','asc');
		return $this->db->get($this->users_table, $limit, $offset);
	}

	function count_all(){
		return $this->db->count_all($this->users_table);
	}

	/* UTILIZADORES CRUD */
	function add($dados) {		
		$this->db->insert($this->users_table, $dados);
	}

	function update($id, $dados)
	{
		$this->db->where('id' , $id);
		$this->db->update($this->users_table, $dados);
	}
	
	function delete($id){
		$this->db->delete($this->users_table, array('id' => $id));
		return $this->db->affected_rows();
	}

	function get_utilizadores_by_id($user_id)
	{
		$query = $this->db->get_where($this->users_table, array('id' => $user_id));
		return $query->row();
	}
	
	
}