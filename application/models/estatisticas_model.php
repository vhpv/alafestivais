<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Estatisticas_model extends CI_Model {
	
	function get_total_eventos() {
		$query = $this->db->get_where('eventos');
		return $query->num_rows();
	}
	
	function get_total_reservas() {
		$query = $this->db->get_where('reservations');
		return $query->num_rows();
	}
}