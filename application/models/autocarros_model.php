<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Autocarros_model extends CI_Model {
	
	var $tabela 			= 'autocarros';
	var $tabela_detalhes	= 'autocarros_cidades';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get()
	{
		$this->db->select($this->tabela.'.*,eventos.nome as nome_evento');
		$this->db->from($this->tabela);
		$this->db->join('eventos', 'eventos.id = '.$this->tabela.'.id_evento','LEFT');
		$this->db->order_by("nome","asc");
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_cidades_by_autocarro($id)
	{
		$this->db->select('*');
		$this->db->from($this->tabela_detalhes);
		$this->db->join('cidades', 'cidades.id = '.$this->tabela_detalhes.'.id_cidade');
		
		$this->db->where($this->tabela_detalhes.'.id_autocarro',$id);
		$this->db->order_by($this->tabela_detalhes.".id_ac","asc");
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_by_evento_cidade($evento,$cidade)
	{
		$this->db->select('*');
		$this->db->from($this->tabela_detalhes);
		$this->db->join($this->tabela, $this->tabela.'.id = '.$this->tabela_detalhes.'.id_autocarro','left');
		//$this->db->join('eventos_cidades', 'eventos_cidades.id_cidade = '.$this->tabela_detalhes.'.id_autocarro','left');
		
		$this->db->where($this->tabela.'.id_evento',$evento);
		$this->db->where($this->tabela_detalhes.'.id_cidade',$cidade);
		$this->db->order_by($this->tabela_detalhes.".valor","asc");
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_lugares_livres_by_evento($evento)
	{
		$this->db->select_sum('lugares');
		$this->db->where('id_evento',$evento);
		$this->db->from($this->tabela);
		$query = $this->db->get();
		return $query->row()->lugares;
	}
	

	function get_detalhes_by_id($id)
	{
		$query = $this->db->get_where($this->tabela_detalhes, array(
			'id_ac'			=> $id
			)
		);
		return $query->row();
	}
	
	function get_by_evento($id)
	{
		$query = $this->db->get_where($this->tabela, array(
			'id_evento'			=> $id
			)
		);
		return $query->result();
	}
	
	function get_by_evento_full($id)
	{
		$this->db->select($this->tabela.'.*,eventos.nome as nome_evento');
		$this->db->from($this->tabela);
		$this->db->join('eventos', 'eventos.id = '.$this->tabela.'.id_evento','LEFT');
		$this->db->order_by("nome","asc");
		$this->db->where('id_evento',$id);
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_by_id($id)
	{
		$query = $this->db->get_where($this->tabela, array(
			'id'			=> $id
			)
		);
		return $query->row();
	}

	function adicionar($data)
	{		
		$this->db->insert($this->tabela, $data);
		return $this->db->insert_id();
	}
	
	function adicionar_detalhe($data)
	{		
		$this->db->insert($this->tabela_detalhes, $data);
	}

	function editar($id,$data)
	{	
		$this->db->where(array('id'=> $id));
		$this->db->update($this->tabela, $data);
	}
	
	function apagar($data) {
		$this->db->delete($this->tabela, $data); 
	}
	
	function apagar_detalhe($data) {
		$this->db->delete($this->tabela_detalhes, $data);
	}
}