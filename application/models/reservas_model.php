<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Reservas_model extends CI_Model {
	
	var $tabela		= 'reservations';
	var $table_join = 'reservations_join';
	
	function __construct()
	{
		parent::__construct();
	}
	
	function get() {
		$this->db->order_by('id','desc');
		$query = $this->db->get($this->tabela);
		return $query->result();
	}
	
	function get_by_bus($id){
		$query = $this->db->get_where($this->tabela, array(
			'bus_id'			=> $id
			)
		);
		return $query->result();
	}
	
	function get_total_by_bus($id)
	{
		$this->db->select_sum('quantity');
		$this->db->where('bus_id', $id); 
		$query = $this->db->get($this->tabela);
		return $query->row();
	}
	
	function get_by_token($token){
		$this->db->where('code', $token);
		$this->db->limit(1);
		$query = $this->db->get($this->tabela);
		return $query->row();
	}
	
	function get_by_event($id){
		$query = $this->db->get_where($this->tabela, array(
			'event_id'			=> $id
			)
		);
		return $query->result();
	}
	
	function get_by_event_bus($event_id,$bus_id){
		$this->db->where('event_id', $event_id); 
		$this->db->where('bus_id', $bus_id); 
		$query = $this->db->get($this->tabela);
		return $query->result();
	}
	
	function get_total_by_event($id)
	{
		$this->db->select_sum('quantity');
		$this->db->where('event_id', $id); 
		$query = $this->db->get($this->tabela);
		return $query->row();
	}
	
	function get_by_event_place($event,$place,$trip=false,$sum=false)
	{
		if($sum) $this->db->select_sum('quantity');
		$this->db->where('event_id', $event);
		$this->db->where('place_id', $place);
		if($trip == 'Ida e volta'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Apenas ida'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Apenas volta'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Todos ida'){
			$this->db->where("(trip = 'Ida e volta' OR trip = 'Apenas ida')");
		} elseif($trip == 'Todos volta'){
			$this->db->where("(trip = 'Ida e volta' OR trip = 'Apenas volta')");
		}
		$query = $this->db->get($this->tabela);
		if($sum) return $query->row('quantity');
		return $query->result();
	}
	
	function get_by_bus_place($bus_id,$place,$trip=false,$sum=false,$order_by=false)
	{
		if($sum) $this->db->select_sum('quantity');
		$this->db->where('bus_id', $bus_id);
		$this->db->where('place_id', $place);
		if($trip == 'Ida e volta'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Apenas ida'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Apenas volta'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Todos ida'){
			$this->db->where("(trip = 'Ida e volta' OR trip = 'Apenas ida')");
		} elseif($trip == 'Todos volta'){
			$this->db->where("(trip = 'Ida e volta' OR trip = 'Apenas volta')");
		}
			
		if($order_by) $this->db->order_by($order_by,'ASC');
		$query = $this->db->get($this->tabela);
		if($sum) return $query->row();
		return $query->result();
	}
	
	function get_total_by_event_place($event,$place)
	{
		$this->db->select_sum('quantity');
		$this->db->where('event_id', $event);
		$this->db->where('place_id', $place);
		$query = $this->db->get($this->tabela);
		return $query->row();
	}
	
	function get_total_by_event_place_bus($event,$place,$bus_id=false,$trip=false)
	{
		$this->db->select_sum('quantity');
		$this->db->where('event_id', $event);
		$this->db->where('place_id', $place);
		if($bus_id) $this->db->where('bus_id', $bus_id);
		if($trip == 'Ida e volta'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Apenas ida'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Apenas volta'){
			$this->db->where('trip', $trip);
		} elseif($trip == 'Todos ida'){
			$this->db->where("(trip = 'Ida e volta' OR trip = 'Apenas ida')");
		} elseif($trip == 'Todos volta'){
			$this->db->where("(trip = 'Ida e volta' OR trip = 'Apenas volta')");
		}
		$query = $this->db->get($this->tabela);
		return $query->row();
	}
	
	function get_by_id($id)
	{
		$query = $this->db->get_where($this->tabela, array(
			'id'			=> $id
			)
		);
		return $query->row();
	}
	
	function find_by_mb_ref($referencia)
	{
		$query = $this->db->get_where($this->tabela, array(
			'mb_ref'			=> $referencia
			)
		);
		return $query->row();
	}
	
	function get_next_id()
	{
		$this->db->get($this->tabela);
		return $this->db->insert_id();
	}
	
	

	function adicionar($data)
	{		
		$this->db->insert($this->tabela, $data);
		return $this->db->insert_id();
	}

	function editar($id,$data)
	{	
		$this->db->where(array('id'=> $id));
		$this->db->update($this->tabela, $data);
	}
	
	function apagar($data) {
		$this->db->delete($this->tabela, $data); 
	}
	
	function add_join($data){		
		$this->db->insert($this->table_join, $data);
		return $this->db->insert_id();
	}
	
	function delete_join($data) {
		$this->db->delete($this->table_join, $data); 
	}
		
	function get_joins_by_reservation_id($reservation_id){
		$this->db->where('reservation_id', $reservation_id);
		$query = $this->db->get($this->table_join);
		return $query->result();
	}
}