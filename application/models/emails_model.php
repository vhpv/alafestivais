<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Emails_model extends  CI_Model {
	
	function get_emails() {
		$query = $this->db->get('emails');
		return $query->result();
	}
	
	function get_paged_list($limit = 10, $offset = 0){
		$this->db->order_by('id','desc');
		$query = $this->db->get('emails', $limit, $offset);
		return $query->result();
	}
	
	function count_all(){
		return $this->db->count_all('emails');
	}

	function adicionar($data) {
		$this->db->insert('emails', $data);
	}
	
	function apagar($data) {
		$this->db->delete('emails', $data); 
	}
}