<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Autocarros extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->admin_logged_in();
		$this->load->model('Autocarros_model','autocarros');
		$this->load->model('Cidades_model','cidades');
		$this->load->model('Eventos_model','eventos');
		$this->load->model('Reservas_model','reservas');
		$this->load->library('form_validation');
		$this->data['title'] = 'Autocarros';
		$this->output->enable_profiler(TRUE);
	}
	
	function index(){
		$this->listar();
	}

	function listar(){
		$this->data['autocarros'] = $this->autocarros->get();
		
		if($this->input->get('event_id')){
			$this->data['autocarros'] = $this->autocarros->get_by_evento_full($this->input->get('event_id'));
		}
		
		$this->data['events']				= $this->eventos->get_main_eventos();
		$this->worker->render_admin('autocarros/listar',$this->data);
	}
	
	function adicionar(){
		$this->form_validation->set_rules('nome', 'Nome', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
		} else {
			$data['nome']				= $this->input->post('nome');
			$data['lugares']			= $this->input->post('lugares');
			$data['bloqueado']			= $this->input->post('bloqueado');
			$data['id_evento']			= $this->input->post('evento');
			$data['data_criacao']		= date('Y-m-d H:i:s');
			
			$id_autocarro 	= $this->autocarros->adicionar($data);
			if($this->input->post('cidade')){
				/* DETALHE */
				$cidades		= $this->input->post('cidade');
				$valor			= $this->input->post('valor');
				$detalhes		= $this->input->post('detalhes');
				$hora_ida		= $this->input->post('hora_ida');
				$hora_volta		= $this->input->post('hora_volta');
				
				
				
				$total_paragens = count($cidades);
				
				for($i=0;$i<$total_paragens; $i++){
					$data_detalhe['id_autocarro']		= $id_autocarro;
					$data_detalhe['id_cidade']			= $cidades[$i];
					$data_detalhe['valor']				= $valor[$i];
					$data_detalhe['detalhes']			= $detalhes[$i];
					$data_detalhe['hora_ida']			= $hora_ida[$i];
					$data_detalhe['hora_volta']			= $hora_volta[$i];
	
					$this->autocarros->adicionar_detalhe($data_detalhe);
				}
			}
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Autocarro criado com sucesso');
			redirect('admin/autocarros/listar');
		}
		$this->data['cidades'] = $this->cidades->get();
		//$this->data['eventos'] = $this->eventos->get(true,false);
		$this->data['eventos'] = $this->eventos->get_main_eventos();
		$this->worker->render_admin('autocarros/adicionar',$this->data);
	}
	
	function editar($id) {
		$this->data['title']	= 'Editar autocarro';
		
		$this->form_validation->set_rules('nome', 'Nome', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
		} else {
			$data['nome']				= $this->input->post('nome');
			$data['lugares']			= $this->input->post('lugares');
			$data['bloqueado']			= $this->input->post('bloqueado');
			$data['id_evento']			= $this->input->post('evento');
			
			$this->autocarros->editar($id,$data);
			
			if($this->input->post('cidade')){
				/* DETALHE */
				$cidades		= $this->input->post('cidade');
				//$valor			= $this->input->post('valor');
				//$detalhes		= $this->input->post('detalhes');
				//$hora_ida		= $this->input->post('hora_ida');
				//$hora_volta		= $this->input->post('hora_volta');
				
				$total_paragens = count($this->input->post('cidade'));
				
				if($total_paragens > 0) {
					$data_apagar['id_autocarro'] = $id;
					$this->autocarros->apagar_detalhe($data_apagar);
				}
				
				for($i=0;$i<$total_paragens; $i++){
					$data_detalhe['id_autocarro']		= $id;
					$data_detalhe['id_cidade']			= $cidades[$i];
					//$data_detalhe['valor']				= $valor[$i];
					//$data_detalhe['detalhes']			= $detalhes[$i];
					//$data_detalhe['hora_ida']			= $hora_ida[$i];
					//$data_detalhe['hora_volta']			= $hora_volta[$i];
	
					$this->autocarros->adicionar_detalhe($data_detalhe);
				}
			}
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Autocarro modificado com sucesso');
			redirect('admin/autocarros/listar');
		}
		//$this->data['cidades'] 				= $this->cidades->get();
		$this->data['eventos']				= $this->eventos->get_main_eventos();
		$this->data['autocarro']			= $autocarro = $this->autocarros->get_by_id($id);
		$this->data['cidades']				= $this->eventos->get_cidades_by_evento($autocarro->id_evento);
		$this->data['autocarro_cidades'] 	= $this->autocarros->get_cidades_by_autocarro($id);
		
		
		$this->worker->render_admin('autocarros/editar',$this->data);
	}
	
	function ver($id) {
		$this->data['title']	= 'Ver autocarro';

		$this->data['cidades'] 				= $this->cidades->get();
		$this->data['eventos']				= $this->eventos->get(true,false);
		$this->data['autocarro']			= $this->autocarros->get_by_id($id);
		$this->data['autocarro_cidades'] 	= $this->autocarros->get_cidades_by_autocarro($id);
		
		
		$this->worker->render_admin('autocarros/ver',$this->data);
	}
	

	function apagar($id) {
		$data['id']			= $id;
		$data_cidades['id_autocarro'] = $id;
		$this->autocarros->apagar($data);
		$this->autocarros->apagar_detalhe($data_cidades);
		
		$this->session->set_flashdata('msg_tipo', 'success');
		$this->session->set_flashdata('msg_descricao', 'Autocarro apagado com sucesso');
		redirect('admin/autocarros/listar');
	}
	
	function distribuir($id_evento){
		$this->data['evento']				= $this->eventos->get_by_id($id_evento);
		$this->data['available_bus']		= $this->autocarros->get_by_evento($id_evento);
		$this->data['reservations']			= $this->reservas->get_by_event($id_evento);
		$this->data['total_reservations']	= $this->reservas->get_total_by_event($id_evento);
		$this->data['reservations_left']	= $this->reservas->get_by_event_bus($id_evento,0);
		
		
		$this->worker->render_admin('autocarros/distribuir',$this->data);
	}
	
	function gerar($id_autocarro){
		$autocarro					= $this->autocarros->get_by_id($id_autocarro);
		if($autocarro->bloqueado){
			$this->session->set_flashdata('msg_tipo', 'warning');
			$this->session->set_flashdata('msg_descricao', 'Autocarro já se encontra bloqueado');
			
			redirect('admin/autocarros/distribuir/'.$autocarro->id_evento);
			exit;
		}
		$this->data['evento']		= $this->eventos->get_by_id($autocarro->id_evento);
		$cidades					= $this->autocarros->get_cidades_by_autocarro($id_autocarro);
		$lugares_disponiveis_ida	= $autocarro->lugares;
		$lugares_disponiveis_volta	= $autocarro->lugares;
		
		$inventario 				= array();
		
		$bus_reservations			= $this->reservas->get_by_bus($id_autocarro);
		
		foreach($bus_reservations as $bus_reservation){
			$data_bus_reservation['bus_id']				= 0;
			$this->reservas->editar($bus_reservation->id,$data_bus_reservation);
		}
		
		foreach($cidades as $cidade){
			//$inventario[$cidade->id] = $this->reservas->get_by_event_place($this->data['evento']->id,$cidade->id,'Ida e volta',true);
			$inventario[$cidade->id] = $this->reservas->get_by_event_place($this->data['evento']->id,$cidade->id,false,true);
		}
		
		//ordenar pelo numero de reservas
		asort($inventario);
		//print_r($inventario);
		foreach($inventario as $cidade=>$reservas){
			$reservas_so_ida = $this->reservas->get_by_event_place($this->data['evento']->id,$cidade,'Apenas ida');
			foreach($reservas_so_ida as $reserva){
				$reservation_bus = $this->autocarros->get_by_id($reserva->bus_id);
				if(($lugares_disponiveis_ida-$reserva->quantity) >= 0){
					if(isset($reservation_bus->bloqueado)){
						if($reservation_bus->bloqueado==1){
							continue;
						}
					}

					$data['bus_id']				= $id_autocarro;
					$this->reservas->editar($reserva->id,$data);
					$lugares_disponiveis_ida = $lugares_disponiveis_ida-$reserva->quantity;
					
				}
			}
			
			$reservas_so_volta = $this->reservas->get_by_event_place($this->data['evento']->id,$cidade,'Apenas volta');
			foreach($reservas_so_volta as $reserva){
				$reservation_bus = $this->autocarros->get_by_id($reserva->bus_id);
				if(($lugares_disponiveis_volta-$reserva->quantity) >= 0){
					if(isset($reservation_bus->bloqueado)){
						if($reservation_bus->bloqueado==1){
							continue;
						}
					}

					$data['bus_id']				= $id_autocarro;
					$this->reservas->editar($reserva->id,$data);
					$lugares_disponiveis_volta = $lugares_disponiveis_volta-$reserva->quantity;
					
				}
			}
			
			$reservas_ida_e_volta = $this->reservas->get_by_event_place($this->data['evento']->id,$cidade,'Ida e volta');
			foreach($reservas_ida_e_volta as $reserva){
				$reservation_bus = $this->autocarros->get_by_id($reserva->bus_id);
				if(($lugares_disponiveis_ida-$reserva->quantity) >= 0 && ($lugares_disponiveis_volta-$reserva->quantity) >= 0){
					if(isset($reservation_bus->bloqueado)){
						if($reservation_bus->bloqueado==1){
							continue;
						}
					}

					$data['bus_id']				= $id_autocarro;
					$this->reservas->editar($reserva->id,$data);
					$lugares_disponiveis_ida = $lugares_disponiveis_ida-$reserva->quantity;
					$lugares_disponiveis_volta = $lugares_disponiveis_volta-$reserva->quantity;
					
				}
			}
			
		}
		
		/*$this->data['available_bus']		= $this->autocarros->get_by_evento($id_evento);
		$this->data['reservations']			= $this->reservas->get_by_event($id_evento);
		$this->data['total_reservations']	= $this->reservas->get_total_by_event($id_evento);
		
		$this->worker->render_admin('autocarros/distribuir',$this->data);*/
		redirect('admin/autocarros/distribuir/'.$autocarro->id_evento.'#bus'.$id_autocarro);
	}
	
	function bloquear($id_autocarro){
		$autocarro					= $this->autocarros->get_by_id($id_autocarro);
		$data['bloqueado']			= ($autocarro->bloqueado)?0:1;
		
		$this->autocarros->editar($id_autocarro,$data);
		
		redirect('admin/autocarros/distribuir/'.$autocarro->id_evento.'#bus'.$id_autocarro);
	}
	
	function imprimir($id_autocarro){
		$autocarro					= $this->autocarros->get_by_id($id_autocarro);
		$this->data['autocarro']	= $autocarro;
		
		$cidades					= $this->autocarros->get_cidades_by_autocarro($id_autocarro);
		foreach($cidades as $cidade){
			$reservas = $this->reservas->get_by_event_place($autocarro->id_evento,$cidade->id,'Apenas ida');
			$inventario[$cidade->id] = count($reservas).'<br>';
		}
		asort($inventario);
		$this->data['cidades'] = $inventario;
		
		
		$this->worker->render_admin('autocarros/imprimir',$this->data);
	}
	
}