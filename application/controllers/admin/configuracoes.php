<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracoes extends MY_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->admin_logged_in();
		$this->load->model('Configuracoes_model','configuracoes');
		$this->load->library('form_validation');
	}
	
	function textos() {
		$this->data['title']	= 'Editar configurações';
		$this->data['texto'] = $this->configuracoes->get_site('texto_footer');
		
		$this->form_validation->set_rules('site_texto_footer', 'Descrição do rodapé', 'required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
		} else {
			$data['site_texto_footer']				= $this->input->post('site_texto_footer');
			$this->configuracoes->update_site();
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Configurações actualizadas com sucesso');
			redirect('admin/configuracoes/textos');
		}
		$this->worker->render_admin('configuracoes/textos',$this->data);
	}
}
?>