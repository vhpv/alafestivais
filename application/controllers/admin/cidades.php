<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cidades extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->admin_logged_in();
		$this->load->model('Cidades_model','cidades');
		$this->load->library('form_validation');
		$this->data['title'] = 'Cidades';
	}
	
	function index()
	{
		$this->listar();
	}

	function listar(){
		$this->data['cidades'] = $this->cidades->get();
		$this->worker->render_admin('cidades/listar',$this->data);
	}
	
	function adicionar()
	{
		$this->form_validation->set_rules('nome', 'Nome', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
		} else {
			$data['nome']				= $this->input->post('nome');
			$data['activo']				= $this->input->post('activo');
			$data['data_criacao']		= date('Y-m-d H:i:s');
			
			$this->cidades->adicionar($data);
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Cidade criada com sucesso');
			redirect('admin/cidades/listar');
		}
		$this->worker->render_admin('cidades/adicionar',$this->data);
	}
	
	function editar($id) {
		$this->data['title']	= 'Editar cidade';
		
		$this->form_validation->set_rules('nome', 'Nome', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
		} else {
			$data['nome']				= $this->input->post('nome');
			
			$this->cidades->editar($id,$data);
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Cidade modificada com sucesso');
			redirect('admin/cidades/listar');
		}
		$this->data['cidade']	= $this->cidades->get_by_id($id);
		$this->worker->render_admin('cidades/editar',$this->data);
	}
	

	function apagar($id) {
		$data['id']			= $id;
		$categoria = $this->categorias->get_by_id($id);
		
		if(count($this->produtos->get_by_categoria($id)) > 0) {
			$this->session->set_flashdata('msg_tipo', 'danger');
			$this->session->set_flashdata('msg_descricao', 'Categoria ainda tem produtos');
		} else {
			$this->categorias->apagar($data);
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Categoria apagada com sucesso');
		}
		redirect('admin/categorias/listar');
	}		
}