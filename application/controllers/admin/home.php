<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
		$this->load->model('Users_model','users');
	}

	function index()
	{
		$this->admin_logged_in();
		$this->load->model('Estatisticas_model','estatisticas');
		$this->worker->render_admin('home',$this->data);
	}
	
	function login()
	{
		$data = array('title' => 'Login');
		$this->load->view('admin/login');
	}
	
	function logout()
	{
		$this->admin_logged_in();
		$this->session->sess_destroy();
		redirect('admin','refresh');
	}
	
	function validar_login(){
		$query = $this->users->validar_login($this->input->post('email'),$this->input->post('password'));
		if ($query) {
			$dados_utilizador = $this->users->dados_utilizador($this->input->post('email'));
			$data = array(
				'email' => $this->input->post('email'),
				'nome' => $dados_utilizador->nome,
				'last_login' => $dados_utilizador->last_login,
				'admin_logged_in' => true,
				'level' => $dados_utilizador->level
			);
			$this->users->update_lastlogin();
			$this->session->set_userdata($data);
			redirect('admin');
		} else {
			$this->index();
		}
	}
	
	function entrada()
	{
		$this->admin_logged_in();
		redirect('admin/albuns/listar','refresh');
	}
}
