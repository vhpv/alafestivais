<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Eventos extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->admin_logged_in();
		$this->load->model('Eventos_model','eventos');
		$this->load->model('Cidades_model','cidades');
		$this->load->model('Autocarros_model','autocarros');
		$this->load->library('form_validation');
		$this->data['title'] = 'Eventos';
	}
	
	function index()
	{
		$this->listar();
	}

	function listar(){
		$this->data['eventos'] = $this->eventos->get_main_eventos();
		$this->worker->render_admin('eventos/listar',$this->data);
	}
	
	function adicionar()
	{
		$config['upload_path'] 	= 'media/albuns/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size']		= '5000';
		$config['max_width']  	= '5000';
		$config['max_height']  	= '5000';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);

		$this->form_validation->set_rules('nome', 'Nome', 'required|xss_clean');
		$this->form_validation->set_rules('descricao', 'Descrição', 'required|xss_clean');
		$this->form_validation->set_rules('data_inicio', 'Data de Início', 'required|xss_clean');
		$this->form_validation->set_rules('data_fim', 'Data de Fim', 'required|xss_clean');
		$this->form_validation->set_rules('evento_pai', 'É evento pai?', 'required|xss_clean');
		$this->form_validation->set_rules('evento_pai_id', 'Evento pai', 'required|xss_clean');
		$this->form_validation->set_rules('active', 'Activo', 'required|xss_clean');
		$this->form_validation->set_rules('id_cidade', 'Cidade', 'required|xss_clean');
		$this->form_validation->set_rules('address_arrive', 'Local do evento', 'required|xss_clean');

		$this->form_validation->set_rules('payment_limit', 'Limite pagamento (horas)', 'required|xss_clean');
		$this->form_validation->set_rules('go', 'Ida', 'required|xss_clean');
		$this->form_validation->set_rules('come', 'Volta', 'required|xss_clean');
		//$this->form_validation->set_rules('go_come_dd', 'Dias diferentes', 'required|xss_clean');
		
		

		if ($this->form_validation->run() == FALSE) {
		} else {
			/*if($this->upload->do_upload('imagem')) {
				$fInfo = $this->upload->data();
				$this->imagem_resize($fInfo['file_name']);
				$this->miniatura($fInfo['file_name']);
				$data['imagem']				= $fInfo['file_name'];
			} else {
				$this->data['erro_upload'] = $error = array('error' => $this->upload->display_errors());
			}*/
			
			$data['nome'] 				= $this->input->post('nome');
			$data['descricao'] 			= $this->input->post('descricao');
			$data['data_inicio'] 		= $this->input->post('data_inicio');
			$data['data_fim'] 			= $this->input->post('data_fim');
			$data['evento_pai'] 		= $this->input->post('evento_pai');
			$data['evento_pai_id'] 		= $this->input->post('evento_pai_id');
			$data['active']				= $this->input->post('active');
			$data['id_cidade']			= $this->input->post('id_cidade');
			$data['address_arrive']		= $this->input->post('address_arrive');
			//$data['ref_local']		= $this->input->post('ref_local');
			$data['payment_limit']		= $this->input->post('payment_limit');
			$data['go']					= $this->input->post('go');
			$data['come']				= $this->input->post('come');
			//$data['go_come_dd']			= $this->input->post('go_come_dd');
			
			$data['created_at']			= date('Y-m-d H:i:s');
			
			$id_evento = 	$this->eventos->adicionar($data);
			
			/* DETALHE */
			if($this->input->post('cidade')){
				$cidades		= $this->input->post('cidade');
				$valor			= $this->input->post('valor');
				$detalhes		= $this->input->post('detalhes');
				$hora_ida		= $this->input->post('hora_ida');
				$hora_volta		= $this->input->post('hora_volta');
				
				$total_paragens = count($cidades);
				
				for($i=0;$i<$total_paragens; $i++){
					$data_detalhe['id_evento']			= $id_evento;
					$data_detalhe['id_cidade']			= $cidades[$i];
					$data_detalhe['valor']				= $valor[$i];
					$data_detalhe['detalhes']			= $detalhes[$i];
					$data_detalhe['hora_ida']			= $hora_ida[$i];
					$data_detalhe['hora_volta']			= $hora_volta[$i];
	
					$this->eventos->adicionar_detalhe($data_detalhe);
				}
			}
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Evento criado com sucesso');
			redirect('admin/eventos/listar');
		}
		$this->data['cidades'] = $this->cidades->get();
		$this->data['eventos'] = $this->eventos->get_eventos_pai();
		
		$this->worker->render_admin('eventos/adicionar',$this->data);
	}
	
	function editar($id) {
		$this->data['title']	= 'Editar evento';
		$this->data['evento']	= $this->eventos->get_by_id($id);
		
		$path = 'media/events/'.$id.'/ticket/';
		if(!is_dir($path)){
			mkdir($path,0755,TRUE);
		}

		$path_detail = 'media/events/'.$id.'/detail/';
		if(!is_dir($path_detail)){
			mkdir($path_detail,0755,TRUE);
		}
		$config['upload_path'] 	= $path;
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size']		= '5000';
		$config['max_width']  	= '5000';
		$config['max_height']  	= '5000';
		$config['encrypt_name'] = TRUE;

		$config_detail['upload_path'] 	= $path_detail;
		$config_detail['allowed_types'] = 'jpg|jpeg|png';
		$config_detail['max_size']		= '5000';
		$config_detail['max_width']  	= '5000';
		$config_detail['max_height']  	= '5000';
		$config_detail['encrypt_name']	= TRUE;
		
		$this->load->library('upload', $config);
		
		$this->form_validation->set_rules('nome', 'Nome', 'required|xss_clean');
		$this->form_validation->set_rules('descricao', 'Descrição', 'required|xss_clean');
		$this->form_validation->set_rules('data_inicio', 'Data de Início', 'required|xss_clean');
		$this->form_validation->set_rules('data_fim', 'Data de Fim', 'required|xss_clean');
		$this->form_validation->set_rules('id_cidade', 'Cidade', 'required|xss_clean');
		$this->form_validation->set_rules('address_arrive', 'Local do evento', 'required|xss_clean');

		if ($this->form_validation->run() == FALSE) {
		} else {
			
			if($this->upload->do_upload('imagem')) {
				$fInfo = $this->upload->data();
				$this->imagem_resize($fInfo['file_name'],$path,300,300);
				$data['image_file_name']				= $fInfo['file_name'];
			} else {
				$this->data['erro_upload'] = $error = array('error' => $this->upload->display_errors());
			}

			$this->upload->initialize($config_detail);
			if($this->upload->do_upload('detail_image')) {
				$fInfo_detail = $this->upload->data();
				$this->imagem_resize($fInfo_detail['file_name'],$path_detail,780,300);
				$data['detail_image']				= $fInfo_detail['file_name'];
			} else {
				$this->data['erro_upload'] = $error = array('error' => $this->upload->display_errors());
			}

			
			$data['nome'] 				= $this->input->post('nome');
			$data['descricao'] 			= $this->input->post('descricao');
			$data['data_inicio'] 		= $this->input->post('data_inicio');
			$data['data_fim'] 			= $this->input->post('data_fim');
			$data['evento_pai'] 		= $this->input->post('evento_pai');
			$data['evento_pai_id'] 		= $this->input->post('evento_pai_id');
			$data['active']				= $this->input->post('activo');
			$data['id_cidade']			= $this->input->post('id_cidade');
			$data['ref_local']			= $this->input->post('ref_local');
			$data['address_arrive']		= $this->input->post('address_arrive');
			$data['payment_limit']		= $this->input->post('payment_limit');
			$data['go']					= $this->input->post('go');
			$data['come']				= $this->input->post('come');
			//$data['go_come_dd']			= $this->input->post('go_come_dd');
			
			$data['created_at']			= date('Y-m-d H:i:s');
			
			
			$this->eventos->editar($id,$data);
			
			if($this->input->post('cidade')){
				/* DETALHE */
				$cidades		= $this->input->post('cidade');
				$valor			= $this->input->post('valor');
				$detalhes		= $this->input->post('detalhes');
				$hora_ida		= $this->input->post('hora_ida');
				$hora_volta		= $this->input->post('hora_volta');
				
				$total_paragens = count($this->input->post('cidade'));
				
				if($total_paragens > 0) {
					$data_apagar['id_evento'] = $id;
					$this->eventos->apagar_detalhe($data_apagar);
				}
				
				for($i=0;$i<$total_paragens; $i++){
					$data_detalhe['id_evento']			= $id;
					$data_detalhe['id_cidade']			= $cidades[$i];
					$data_detalhe['valor']				= $valor[$i];
					$data_detalhe['detalhes']			= $detalhes[$i];
					$data_detalhe['hora_ida']			= $hora_ida[$i];
					$data_detalhe['hora_volta']			= $hora_volta[$i];
	
					$this->eventos->adicionar_detalhe($data_detalhe);
				}
			}
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Evento modificado com sucesso');
			
			redirect('admin/eventos/listar');
		}
		$this->data['cidades'] = $this->cidades->get();
		$this->data['eventos_cidades'] 	= $this->eventos->get_cidades_by_evento($id);
		$this->data['eventos'] = $this->eventos->get_eventos_pai();
		
		$this->worker->render_admin('eventos/editar',$this->data);
	}
	

	function apagar($id) {
		$data['id']		= $id;
		$evento			= $this->eventos->get_by_id($id);
		$eventos_filhos = $this->eventos->get_by_id_pai($id);
		
		if(count($eventos_filhos)) {
			$this->session->set_flashdata('msg_tipo', 'danger');
			$this->session->set_flashdata('msg_descricao', 'Este evento ainda tem eventos filhos, não foi possível apagar');
		} else {
			/* apagar os autocarros */
			$autocarros = $this->autocarros->get_by_evento($id);
			
			foreach($autocarros as $autocarro){
				$data_ac['id_autocarro'] = $autocarro->id;
				$this->autocarros->apagar_detalhe($data_ac);
				$data_autocarros['id'] = $autocarro->id;
				$this->autocarros->apagar($data_autocarros);
			}
			
			/* apagar as cidades */
			$data_cidades['id_evento'] = $id;
			$this->eventos->apagar_cidades_by_evento($data_cidades);
			
			$this->eventos->apagar($data);
						
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Evento apagado com sucesso');
		}
		redirect('admin/eventos/listar');
	}
	
	function imagem_resize($fileName,$src,$width,$height) 
	{
	    $config['image_library'] = 'gd2';
		$config['source_image'] = $src . $fileName;
        $config['create_thumb'] = FALSE;	 
		$config['maintain_ratio'] = TRUE;
		$config['width'] = $width;
		$config['height'] = $height;
		$this->load->library('image_lib', $config);
        $this->image_lib->initialize($config);		
        $this->image_lib->resize();        
        $this->image_lib->clear();
  	}
}