<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vouchers extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->admin_logged_in();
		$this->load->model('Vouchers_model','vouchers');
		$this->load->model('Eventos_model','eventos');
		$this->load->library('form_validation');
	}
	
	function index()
	{
		$this->listar();
	}

	function listar(){
		$this->data['title']	= 'Listar Voucher';
		$this->data['vouchers'] = $this->vouchers->get();
		$this->worker->render_admin('vouchers/listar',$this->data);
	}
	
	function adicionar(){
		$this->data['events'] = $this->eventos->get_main_eventos();
		
		$this->data['title']	= 'Adicionar Voucher';
		
		$this->form_validation->set_rules('name', 'Nome', 'required|xss_clean');
		$this->form_validation->set_rules('code', 'Código', 'required|xss_clean');
		$this->form_validation->set_rules('value', 'Valor', 'required|xss_clean');
		$this->form_validation->set_rules('type', 'Tipo', 'required|xss_clean');
		
		if ($this->form_validation->run() != FALSE) {
			$data['name']				= $this->input->post('name');
			$data['code']				= strtoupper($this->input->post('code'));
			$data['value']				= $this->input->post('value');
			$data['type']				= $this->input->post('type');
			$data['start_date']			= $this->input->post('start_date');
			$data['end_date']			= $this->input->post('end_date');
			$data['event_id']			= $this->input->post('event_id');
			$data['times']				= $this->input->post('times');
			$data['active']				= $this->input->post('active');
			
			$data['date_created']		= date('Y-m-d H:i:s');
			
			$id_voucher = $this->vouchers->add($data);
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Voucher criado com sucesso');
			
			redirect('admin/vouchers/listar');
		}
		
		$this->worker->render_admin('vouchers/adicionar',$this->data);
	}
	
	function editar($id) {
		$this->data['title']	= 'Editar voucher';
		
		$this->form_validation->set_rules('name', 'Nome', 'required|xss_clean');
		$this->form_validation->set_rules('code', 'Código', 'xss_clean');
		$this->form_validation->set_rules('value', 'Valor', 'required|xss_clean');
		$this->form_validation->set_rules('type', 'Tipo', 'required|xss_clean');

		if ($this->form_validation->run() != FALSE) {
			$data['name']				= $this->input->post('name');
			$data['code']				= strtoupper($this->input->post('code'));
			$data['value']				= $this->input->post('value');
			$data['type']				= $this->input->post('type');
			$data['start_date']			= $this->input->post('start_date');
			$data['end_date']			= $this->input->post('end_date');
			$data['event_id']			= $this->input->post('event_id');
			$data['times']				= $this->input->post('times');
			$data['active']				= $this->input->post('active');
			$data['date_modified']		= date('Y-m-d H:i:s');
			
			$this->vouchers->update($id,$data);
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Vouchers modificado com sucesso');
			redirect('admin/vouchers/listar');
		}
		$this->data['events']		= $this->eventos->get_main_eventos();
		$this->data['voucher']		= $this->vouchers->get_by_id($id);
		$this->worker->render_admin('vouchers/editar',$this->data);
	}
	

	function apagar($id) {
		$data['id']			= $id;
		$this->vouchers->delete($data);
		$this->session->set_flashdata('msg_tipo', 'success');
		$this->session->set_flashdata('msg_descricao', 'Voucher apagado com sucesso');
		redirect('admin/vouchers/listar');
	}
}