<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reservas extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->admin_logged_in();
		$this->load->model('Reservas_model','reservas');
		$this->load->model('Eventos_model','eventos');
		$this->load->model('Cidades_model','cidades');
		$this->load->model('Autocarros_model','autocarros');
		$this->load->library('form_validation');
	}
	
	function index()
	{
		$this->listar();
	}

	function listar(){
		$data['reservas'] = $this->reservas->get();
		$this->worker->render_admin('reservas/listar',$data);
	}
	
	function editar($id) {
		$this->data['title']			= 'Editar reserva';
		$this->data['reservation']		= $this->reservas->get_by_id($id);
		$this->data['evento']			= $this->eventos->get_by_id($this->data['reservation']->event_id);
		$this->data['autocarro']		= $this->autocarros->get_by_id($this->data['reservation']->bus_id);
		//$this->data['available_bus']	= $this->autocarros->get_by_evento($this->data['reservation']->event_id);
		
		$this->data['available_bus']	= $this->autocarros->get_by_evento_cidade($this->data['reservation']->event_id,$this->data['reservation']->place_id);
		
		$this->data['cidade']			= $this->cidades->get_by_id($this->data['reservation']->place_id);
		
		$this->form_validation->set_rules('estado', 'Estado', 'xss_clean');

		if ($this->form_validation->run() == FALSE) {
		} else {
			$data['status']				= $this->input->post('status');
			$data['paid']				= $this->input->post('paid');
			$data['bus_id']				= $this->input->post('bus_id');
			$this->reservas->editar($id,$data);
			
			$this->session->set_flashdata('msg_tipo', 'success');
			$this->session->set_flashdata('msg_descricao', 'Reserva modificada com sucesso');
			redirect('admin/reservas/listar');
		}
		
		$this->worker->render_admin('reservas/editar',$this->data);
	}
	
	function ver($id)
	{	
		$this->data['reserva'] = $this->reservas->get_by_id($id);
		$this->data['reserva_detalhes'] = $this->reservas->get_detalhes_by_id_reserva($id);
		
		$this->worker->render_admin('reservas/ver',$this->data);
	}
	
	function contactos()
	{	
		$this->data['contactos'] = $this->reservas->get_contactos();
		$this->worker->render_admin('reservas/contactos',$this->data);
	}
	
	function enviar_email($id){		
		$this->data['reserva'] = $reserva = $this->reservas->get_by_id($id);
		$this->data['reserva_array'] = $reserva_array = $this->reservas->get_by_id_array($id);
		
		$this->data['reserva_detalhes'] = $reserva_detalhes = $this->reservas->get_detalhes_by_id_reserva($id);
		
		//enviar e-mail
		$this->load->library('email');
		$this->load->library('parser');		
		
		$data_email = array(
			'reserva_codigo'		=> $reserva->codigo,
			'reserva_data'		=> substr($reserva->data_criacao,0,10),
			'cliente_nome'			=> $reserva->nome,
			'cliente_morada'		=> nl2br($reserva->morada),
			'cliente_email'			=> $reserva->email,
			'cliente_telefone'		=> $reserva->telefone,
			
			'reserva_detalhes'	=> $reserva_detalhes,
			'reserva_array'		=> $reserva_array,
		);
		
		/* INICIO ENVIO CLIENTE */
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		$this->email->from('geral@alafestivais.com', 'ALA - Reservas');
		$this->email->to($reserva->email);
		//$this->email->to('vitorvilela@sapo.pt');
		$this->email->subject('Encomenda #' . $reserva->codigo);
		
		$html_email = $this->parser->parse('emails/cliente_reserva',$data_email,TRUE);
		$this->email->message($html_email);
		$this->email->send();
			
		/* INICIO ENVIO INTERNO */
		$this->email->initialize($config);
		$this->email->from('geral@alafestivais.com', 'ALA');
		$this->email->to('geral@alafestivais.com');
		$this->email->subject('Encomenda #' . $reserva->codigo);
		
		$html_email = $this->parser->parse('emails/cliente_reserva',$data_email,TRUE);
		$this->email->message($html_email);
		$this->email->send();
	}
	
	function enviar_email_pago($id){
		
		$this->data['reserva'] = $reserva = $this->reservas->get_by_id($id);
		$this->data['reserva_array'] = $reserva_array = $this->reservas->get_by_id_array($id);
		$this->data['reserva_detalhes'] = $reserva_detalhes = $this->reservas->get_detalhes_by_id_reserva($id);
		
		//enviar e-mail
		$this->load->library('email');
		$this->load->library('parser');		
		
		$data_email = array(
			'reserva_codigo'		=> $reserva->codigo,
			'reserva_data'		=> substr($reserva->data_criacao,0,10),
			'cliente_nome'			=> $reserva->nome,
			'cliente_morada'		=> nl2br($reserva->morada),
			'cliente_email'			=> $reserva->email,
			'cliente_telefone'		=> $reserva->telefone,
			
			'reserva_detalhes'	=> $reserva_detalhes,
			'reserva_array'		=> $reserva_array,
		);
		
		/* INICIO ENVIO CLIENTE */
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		$this->email->from('geral@alafestivais.com', 'ALA - Reservas');
		$this->email->to($reserva->email);
		//$this->email->to('vitorvilela@sapo.pt');
		$this->email->subject('Encomenda #' . $reserva->codigo . ' - Pago');
		
		$html_email = $this->parser->parse('emails/cliente_reserva_pago',$data_email,TRUE);
		$this->email->message($html_email);
		$this->email->send();
			
		/* INICIO ENVIO INTERNO */
		$this->email->initialize($config);
		$this->email->from('geral@alafestivais.com', 'ALA - Reservas');
		$this->email->to('geral@alafestivais.com');
		$this->email->subject('Encomenda #' . $reserva->codigo . ' - Pago');
		
		$html_email = $this->parser->parse('emails/cliente_reserva_pago',$data_email,TRUE);
		$this->email->message($html_email);
		$this->email->send();
	}
	
	function enviar_email_enviado($id){
		
		$this->data['reserva'] = $reserva = $this->reservas->get_by_id($id);
		$this->data['reserva_array'] = $reserva_array = $this->reservas->get_by_id_array($id);
		$this->data['reserva_detalhes'] = $reserva_detalhes = $this->reservas->get_detalhes_by_id_reserva($id);
		
		//enviar e-mail
		$this->load->library('email');
		$this->load->library('parser');		
		
		$data_email = array(
			'reserva_codigo'		=> $reserva->codigo,
			'reserva_data'		=> substr($reserva->data_criacao,0,10),
			'cliente_nome'			=> $reserva->nome,
			'cliente_morada'		=> nl2br($reserva->morada),
			'cliente_email'			=> $reserva->email,
			'cliente_telefone'		=> $reserva->telefone,
			
			'reserva_detalhes'	=> $reserva_detalhes,
			'reserva_array'		=> $reserva_array,
		);
		
		/* INICIO ENVIO CLIENTE */
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		$this->email->from('geral@alafestivais.com', 'ALA - Reservas');
		$this->email->to($reserva->email);
		//$this->email->to('vitorvilela@sapo.pt');
		$this->email->subject('Encomenda #' . $reserva->codigo . ' - Enviado');
		
		$html_email = $this->parser->parse('emails/cliente_reserva_enviado',$data_email,TRUE);
		$this->email->message($html_email);
		$this->email->send();
			
		/* INICIO ENVIO INTERNO */
		$this->email->initialize($config);
		$this->email->from('geral@alafestivais.com', 'ALA - Reservas');
		$this->email->to('geral@alafestivais.com');
		$this->email->subject('Encomenda #' . $reserva->codigo . ' - Enviado');
		
		$html_email = $this->parser->parse('emails/cliente_reserva_enviado',$data_email,TRUE);
		$this->email->message($html_email);
		$this->email->send();
	}
}