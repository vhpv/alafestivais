<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area_reservada extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Albuns_model','albuns');
		$this->load->model('Fotografias_model','fotografias');
		$this->load->model('Banners_laterais_model','banners');
		$this->data['banner_lateral'] = $this->banners->get_random();
		$this->load->model('Precos_model','precos');
	}
	
	public function index()
	{
		$this->listar();
	}
	
	public function listar($id_categoria = 0)
	{
		
		$this->data['albuns'] = $this->albuns->get(2);
		$this->worker->render('area_reservada/listar',$this->data);
	}
	
	public function detalhe($id_album = 0)
	{	
		$this->data['album'] = $this->albuns->get_by_id($id_album);
		$this->data['fotografias'] = $this->fotografias->get_by_album($id_album);
		
		if($this->session->userdata('visitante_logged_in')==false || ($this->session->userdata('id_album_visitar')!=$id_album)) {
			if($this->input->post('password')) {
				$array_items = array('visitante_logged_in' => '', 'id_album_visitar' => '');
				$this->session->unset_userdata($array_items);
				
				$query = $this->albuns->validar_login_visita($id_album,$this->input->post('password'));
				if($query){
					$data = array(
						'visitante_logged_in' 		=> true,
						'id_album_visitar'			=> $id_album
					);
					$this->session->set_userdata($data);
				} else {
					$this->data['password_errada'] = true;
				}
			}
		}
		
		$this->worker->render('area_reservada/detalhe',$this->data);
	}
	
	public function detalhe_fotografia($id_fotografia = 0)
	{
		
		$this->data['fotografia'] = $this->fotografias->get_by_id($id_fotografia);
		$this->data['album'] = $this->albuns->get_by_id($this->data['fotografia']->id_album);
		if($this->session->userdata('visitante_logged_in')==false || ($this->session->userdata('id_album_visitar')!=$this->data['fotografia']->id_album)) {
			redirect(site_url('area-reservada/detalhe/'.$this->data['fotografia']->id_album));
		}
		
		$this->data['fotografia_next']		= $this->fotografias->get_fotografia_next_by_id($id_fotografia,$this->data['fotografia']->id_album);
		$this->data['fotografia_previous']	= $this->fotografias->get_fotografia_previous_by_id($id_fotografia,$this->data['fotografia']->id_album);
		
		$this->data['precos'] = $this->precos->get_detalhe_by_preco_id($this->data['album']->id_preco);
		$this->data['produtos'] = $this->produtos->get_random(4);
		
		$this->worker->render('area_reservada/detalhe_fotografia',$this->data);
	}
}