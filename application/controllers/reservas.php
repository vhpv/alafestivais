<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservas extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Reservas_model','reservas');
		$this->load->model('Eventos_model','eventos');
	}
	
	public function index($token)
	{
		$this->actualizar($token);
	}
	
	function actualizar($token) {
		$this->data['reserva'] = $reserva = $this->reservas->get_by_token($token);
		$this->data['friends'] = $friends = $this->reservas->get_joins_by_reservation_id($reserva->id);
		//print_r($reserva);
		if(!$reserva){
			redirect('/');
			exit;
		}
		
		if(isset($_POST['save_button'])){
			$data_delete['reservation_id'] = $reserva->id;
			$this->reservas->delete_join($data_delete);
			
			if(isset($_POST['friend'])){
				foreach($_POST['friend'] as $friend){
					$data['reservation_id']	= $reserva->id;
					$data['friend_code']	= $friend;
					$data['date_created']	= date('Y-m-d H:i:s');
					$this->reservas->add_join($data);
				}
			}
			redirect(current_url());
		}
		
		//$this->data['options'] 	= $this->reservas->get_options_by_reservation($reserva->id);
		//print_r($reserva);

		/*$this->load->library('form_validation');
		
		$this->form_validation->set_rules('facturacao_nif', 'NIF', 'required|xss_clean');
		$this->form_validation->set_rules('facturacao_nome', 'Nome para facturar', 'required|xss_clean');
		$this->form_validation->set_rules('facturacao_morada', 'Morada', 'required|xss_clean');
		$this->form_validation->set_rules('facturacao_cp', 'Código-Postal', 'required|xss_clean');
		$this->form_validation->set_rules('facturacao_localidade', 'Localidade', 'required|xss_clean');
		
		if ($this->form_validation->run() == TRUE) {
			$data['facturacao_nif'] = $this->input->post('facturacao_nif');
			$data['facturacao_nome'] = $this->input->post('facturacao_nome');
			$data['facturacao_morada'] = $this->input->post('facturacao_morada');
			$data['facturacao_cp'] = $this->input->post('facturacao_cp');
			$data['facturacao_localidade'] = $this->input->post('facturacao_localidade');
			
			$this->reservas->editar($reserva->id,$data);
			
			$this->session->set_flashdata('msg_tipo_frente', 'success');
			$this->session->set_flashdata('msg_descricao_frente', 'Obrigado! Os teus dados foram actualizados com sucesso');
			
			redirect(current_url(),'refresh');
		}*/
		$this->worker->render('reservas/actualizar',$this->data);
	}
}