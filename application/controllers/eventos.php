<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eventos extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Eventos_model','eventos');
		$this->load->model('Cidades_model','cidades');
		$this->load->model('Autocarros_model','autocarros');
		$this->load->model('Reservas_model','reservas');
		$this->load->model('Vouchers_model','vouchers');
	}
	
	public function index()
	{
		
		$this->worker->render('home',$this->data);
	}
	
	public function detalhe($id)
	{
		$this->data['evento'] = $evento = $this->eventos->get_by_id($id);
		
		$eventos_filhos = $this->eventos->get_by_id_pai($evento->id);
		if(count($eventos_filhos)){
			// SE FOR UM MAIN EVENT
			foreach($eventos_filhos as $evento_filho) {
				$precos_por_cidade = $this->eventos->get_detalhes_by_cidade_evento($evento_filho->id,$evento_filho->ref_local);
				if(count($precos_por_cidade) > 1) {
					$evento_filho->valor_base = '0.00';
				} elseif(count($precos_por_cidade) == 1) {
					$evento_filho->valor_base = $precos_por_cidade[0]->valor;
				} else {
					$evento_filho->valor_base = '0.00';
				}
				$evento_filho->cidade_base = $this->cidades->get_by_id($evento_filho->ref_local)->nome;
			}
			
			$this->data['eventos'] = $eventos_filhos;
		
			$this->worker->render('eventos/listar',$this->data);
		} else {
			// SE FOR UM EVENTO NORMAL
			$this->data['cidades'] = $this->eventos->get_cidades_disponiveis_by_evento($id);
			$this->data['lugares_livres'] = $this->autocarros->get_lugares_livres_by_evento($id);
			$this->data['limite_pagamento'] = $evento->payment_limit-1;

			$precos_por_cidade = $this->eventos->get_detalhes_by_cidade_evento($evento->id,$evento->ref_local);
			if(count($precos_por_cidade) > 1) {
				$evento->valor_base = '0.00';
			} elseif(count($precos_por_cidade) == 1) {
				$evento->valor_base = $precos_por_cidade[0]->valor;
			} else {
				$evento->valor_base = '0.00';
			}
			
			$evento->cidade_base = $this->cidades->get_by_id($evento->ref_local)->nome;
			
			$this->load->library('form_validation');
			$this->form_validation->set_rules('first_name', 'Primeiro Nome', 'trim|required|xss_clean');
			$this->form_validation->set_rules('last_name', 'Último Nome', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|xss_clean');
			$this->form_validation->set_rules('phone', 'Telefone', 'required|xss_clean');
			$this->form_validation->set_rules('voucher', 'Voucher', 'callback_voucher_check['.$id.']|xss_clean');
			
			
			if ($this->form_validation->run() == FALSE) {
			} else {
				
				
				$voucher = $this->vouchers->get_by_code($this->input->post('voucher'));
				
				$data['first_name'] 				= $this->input->post('first_name');
				$data['last_name'] 					= $this->input->post('last_name');
				$data['email'] 						= $this->input->post('email');
				$data['phone'] 						= $this->input->post('phone');
				$data['quantity'] 					= $this->input->post('quantity');
				$data['trip'] 						= $this->input->post('trip');
				$data['place_id']					= $this->input->post('place_id');
				$data['event_id'] 					= $id;
				$data['price']						= $this->get_price($data['event_id'],$data['place_id'],$data['quantity'],$data['trip'])->preco_unitario;
				$data['status']						= 0;
				$data['paid']						= 0;
				$data['created_at'] 				= date('Y-m-d H:i:s');
				$data['payment_time_limit'] 		= date('Y-m-d H:i:s', strtotime(sprintf("+%d hours", $evento->payment_limit)));
				$data['facturacao_nome'] 			= $this->input->post('facturacao_nome');
				$data['facturacao_nif'] 			= $this->input->post('facturacao_nif');
				/* CODIGO PARA GERIR */
				$this->load->helper('string');
				$data['code']						= random_string('alnum', 32);
				
				if($voucher){
					$data['voucher_id'] = $voucher->id;
					if($voucher->type == 'flat'){
						$data['price'] = number_format($data['price']-$voucher->value,2);
					} elseif($voucher->type == 'percent'){
						$data['price'] = number_format($data['price']*($voucher->value/100),2);
					}
					$data_voucher_update['times'] = $voucher->times-$data['quantity'];
					$data_voucher_update['times_used'] = $voucher->times_used+$data['quantity'];
					$this->vouchers->update($voucher->id,$data_voucher_update);
				}
				if($data['price'] < 0){
					$data['price'] = 0;
				}
				
				$id_reserva = $this->reservas->adicionar($data);
				
				$dados_mb = GenerateMbRef('11604', '504', $id_reserva, $this->get_price($data['event_id'],$data['place_id'],$data['quantity'],$data['trip'])->preco_final);
				
				$data_update['mb_ent'] = $dados_mb['entidade'];
				$data_update['mb_ref'] = $dados_mb['referencia'];
				
				/* codigo para gerir */
				$data_update['reservation_code'] = substr($data_update['mb_ref'],0,7);
				
				$this->reservas->editar($id_reserva,$data_update);
				
				//envio de email de reserva
				$r = email_reserva($id_reserva);

				if(!$r) {
					$this->email->print_debugger();
				}

				$this->session->set_flashdata('msg_tipo_frente', 'success');
				$this->session->set_flashdata('msg_descricao_frente', 'Obrigado pela tua reserva! Receberás dentro de alguns minutos mais informações na tua caixa de e-mail');
				
				$autocarro_escolhido = false;
				$autocarros_disponiveis = $this->autocarros->get_by_evento_cidade($evento->id,$data['place_id']);
				while($autocarro_escolhido == false){
					foreach($autocarros_disponiveis as $autocarro_disponivel){
						//verifica se esta bloqueado
						if($autocarro_disponivel->bloqueado){
							break;
						}
						//verifica se tem lugares disponiveis
						
						//se tem lugares disponiveis atribui
						$data_bus['bus_id'] = $autocarro_disponivel->id_autocarro;
						$this->reservas->editar($id_reserva,$data_bus);
						$autocarro_escolhido = true;
						break;
					}
					$autocarro_escolhido = true;
				}

				redirect('event/'.$id.'?success');
			}
			$this->worker->render('eventos/detalhe',$this->data);
		}
	}
	
	function price_update() {
		if($this->input->post('event')) {
			$evento 	= $this->input->post('event');
			$cidade		= $this->input->post('place');
			$trip		= $this->input->post('trip');
			$quantidade	= $this->input->post('quantity');
			//$autocarros = $this->autocarros->get_by_evento_cidade($evento,$cidade);
			$autocarros = $this->eventos->get_detalhes_by_cidade_evento($evento,$cidade);
			$detalhes_evento = $this->eventos->get_by_id($evento);
			
			foreach($autocarros as $autocarro){
				if($trip == 'Ida e volta') {
					//$autocarro->preco_unitario = ($detalhes_evento->go_come_dd/100) * $autocarro->valor;
					$autocarro->preco_unitario = number_format($autocarro->valor, 2);
					$autocarro->preco_final = number_format($autocarro->preco_unitario * $quantidade, 2);
				} elseif($trip == 'Apenas ida') {
					$autocarro->preco_unitario = number_format(($detalhes_evento->go/100) * $autocarro->valor, 2);
					$autocarro->preco_final = number_format($autocarro->preco_unitario * $quantidade, 2);
				} elseif($trip == 'Apenas volta') {
					$autocarro->preco_unitario = number_format(($detalhes_evento->come/100) * $autocarro->valor, 2);
					$autocarro->preco_final = number_format($autocarro->preco_unitario * $quantidade, 2);
				}
			}
			
			$detalhes_proposta_array = array(
				'autocarros'	=> $autocarros
			);
			
			header('Content-Type: application/json');
			echo json_encode($detalhes_proposta_array);
		}
	}
	
	function get_bus() {
		if($this->input->post('busid')) {
			$id_autocarro = $this->input->post('busid');
			$autocarro = $this->autocarros->get_detalhes_by_id($id_autocarro);
			header('Content-Type: application/json');
			echo json_encode(get_object_vars($autocarro));
		}
	}
	
	function formulario_distrito(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nome', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'E-mail', 'required|xss_clean');
		$this->form_validation->set_rules('telefone', 'Telefone', 'xss_clean');
		$this->form_validation->set_rules('distrito', 'Distrito', 'xss_clean');
		$this->form_validation->set_rules('localidade', 'Localidade', 'xss_clean');
		$this->form_validation->set_rules('quantidade', 'Quantidade', 'xss_clean');
		$this->form_validation->set_rules('evento', 'Evento', 'xss_clean');
		
		if ($this->form_validation->run() == FALSE) {
		} else {
			
			$data['name'] 				= $this->input->post('name');
			$data['email'] 				= $this->input->post('email');
			$data['telefone'] 			= $this->input->post('telefone');
			$data['distrito'] 			= $this->input->post('distrito');
			$data['localidade'] 		= $this->input->post('localidade');
			$data['quantidade'] 		= $this->input->post('quantidade');
			$data['evento'] 			= $this->input->post('evento');
			
			$mensagem = "Recebeu um pedido para o evento XXX\r\n\r\n".
			"Nome: " . $data['name'] ."\r\n".
			"E-mail: " . $data['email'] ."\r\n".
			"Telefone: " . $data['telefone'] ."\r\n".
			"Distrito: " . $data['distrito'] ."\r\n".
			"Localidade: " . $data['localidade'] ."\r\n".
			"Quantidade: " . $data['quantidade'] ."\r\n";
			
			$this->load->library('email');
	
			$config['mailtype'] = 'text';
		
			$this->email->initialize($config);
			$this->email->from('alafestivais@alaviagens.com', 'ALA Viagens');
			$this->email->to('vitorvilela@sapo.pt');
			$this->email->subject('Pedido para evento');
			
			$this->email->message($mensagem);
			$this->email->send();		
		}
		
		$this->load->view('site/eventos/formulario_distrito');
	}
	
	function get_price($evento,$cidade,$quantidade,$trip) {
		$autocarro = $this->eventos->get_detalhes_by_cidade_evento_row($evento,$cidade);
		$detalhes_evento = $this->eventos->get_by_id($evento);
			
		if($trip == 'Ida e volta') {
			$autocarro->preco_unitario = number_format($autocarro->valor, 2);
			$autocarro->preco_final = number_format($autocarro->preco_unitario * $quantidade, 2);
		} elseif($trip == 'Apenas ida') {
			$autocarro->preco_unitario = number_format(($detalhes_evento->go/100) * $autocarro->valor, 2);
			$autocarro->preco_final = number_format($autocarro->preco_unitario * $quantidade, 2);
		} elseif($trip == 'Apenas volta') {
			$autocarro->preco_unitario = number_format(($detalhes_evento->come/100) * $autocarro->valor, 2);
			$autocarro->preco_final = number_format($autocarro->preco_unitario * $quantidade, 2);
		}
		return $autocarro;
	}
	
	function callback(){
	//	https://www.exemplo.com/callback.php?
	//	chave=[CHAVE_ANTI_PHISHING]&entidade=[ENTIDADE]&referencia=[REFERENCIA]&valor=[VALOR]
		
		$chave_anti_phishing = "2013trinity2010alaviagen10negaivala";
		if($this->input->get('chave') && $this->input->get('entidade') && $this->input->get('referencia') && $this->input->get('valor')){
	 
			$chave = $this->input->get('chave');
			$entidade = $this->input->get('entidade');
			$referencia = $this->input->get('referencia');
			$valor = $this->input->get('valor');
			//verifica se a chave anti-phishing devolvida pela ifthen corresponde à chave definida
			if($chave==$chave_anti_phishing){
				//procura a reserva
				$reserva = $this->reservas->find_by_mb_ref($this->input->get('referencia'));
				//muda o status
				$data['paid']				= 1;
				$this->reservas->editar($reserva->id,$data);
				//envia o email
				
				print 'RESERVA ' . $reserva->id . ' actualizada com sucesso';
			} else {
				die('elements not defined!');
			}
		} else {
			die('elements not defined!');
		}
	}
	
	public function voucher_check($str,$id){
		$str = strtoupper($str);
		$voucher = $this->vouchers->get_by_code($str);
		if(empty($str)){
			return true;
		}
		if(!$voucher){
			$this->form_validation->set_message('voucher_check', 'O voucher introduzido não é válido');
			return FALSE;
		} else {
			if($voucher->active==0){
				$this->form_validation->set_message('voucher_check', 'O voucher não está activo');
				return FALSE;
			}
			if($voucher->event_id>0 && $id!=$voucher->event_id){
				$this->form_validation->set_message('voucher_check', 'O voucher não é válido para este evento');
				return FALSE;
			}
			if($voucher->start_date>'0000-00-00' && date('Y-m-d')<$voucher->start_date){
				$this->form_validation->set_message('voucher_check', 'O voucher ainda não começou');
				return FALSE;
			}
			if($voucher->end_date>'0000-00-00' && date('Y-m-d')>$voucher->end_date){
				$this->form_validation->set_message('voucher_check', 'O voucher já expirou');
				return FALSE;
			}
			if($voucher->times==0){
				$this->form_validation->set_message('voucher_check', 'O voucher já esgotou o número de utilizações');
				return FALSE;
			}
		}
		return TRUE;
	}
}