<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->model('Eventos_model','eventos');
		$this->load->model('Cidades_model','cidades');
		$this->data['slides'] = $this->eventos->get_slides();
		
		$this->data['eventos'] = $this->eventos->get_main_eventos_activos();
		
		foreach($this->data['eventos'] as $evento) {
			//se for um main event
			if($evento->evento_pai) {
				//procura filhos
				$eventos_filhos = $this->eventos->get_by_id_pai($evento->id);
				//conta os filhos
				$evento->eventos_filhos = count($eventos_filhos);
				//se existirem filhos
				if(count($eventos_filhos) > 0) {
					$valor_mais_baixo = 999999;
					$cidade_base = '';
					foreach($eventos_filhos as $evento_filho) {
						$precos_por_cidade = $this->eventos->get_detalhes_by_cidade_evento($evento_filho->id,$evento_filho->ref_local);
						if(count($precos_por_cidade)){
							if($precos_por_cidade[0]->valor < $valor_mais_baixo){
								$valor_mais_baixo	= $precos_por_cidade[0]->valor;
								$cidade_base 		= $this->cidades->get_by_id($precos_por_cidade[0]->id_cidade)->nome;
							}
						} else {
							$valor_mais_baixo	= NULL;
							$cidade_base 		= NULL;
						}
					}
					$evento->valor_base		= $valor_mais_baixo;
					$evento->cidade_base	= $cidade_base;
				}
			} else {
				$precos_por_cidade = $this->eventos->get_detalhes_by_cidade_evento($evento->id,$evento->ref_local);
				if(count($precos_por_cidade) > 1) {
					$evento->valor_base = '0.00';
				} elseif(count($precos_por_cidade) == 1) {
					$evento->valor_base = $precos_por_cidade[0]->valor;
				} else {
					$evento->valor_base = '0.00';
				}
				$evento->cidade_base = $this->cidades->get_by_id($evento->ref_local)->nome;
			}
		}
		
		$this->worker->render('home',$this->data);
	}
	
	public function contactos()
	{
		$this->worker->render('contactos',$this->data);
	}
	
	public function porque()
	{
		$this->worker->render('info/porque',$this->data);
	}
	public function como()
	{
		$this->worker->render('info/como',$this->data);
	}
	public function horarios()
	{
		$this->worker->render('info/horarios',$this->data);
	}
	public function historico()
	{
		$this->load->model('Eventos_model','eventos');
		$this->data['eventos'] = $this->eventos->get_historico();
		$this->worker->render('info/historico',$this->data);
	}
	public function precos()
	{
		$this->worker->render('info/precos',$this->data);
	}
}