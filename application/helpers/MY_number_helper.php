<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists('moeda_format'))
{
    function moeda_format($numero = 0)
    {
        return number_format($numero,2,",",".");
    }
}