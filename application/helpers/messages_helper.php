<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists('alerta')) {
	function alerta() {
		$CI =& get_instance();
		$resultado = '';
		if($CI->session->flashdata('msg_tipo')) {
			$resultado .='<div class="box-body">';
			$resultado .='<div class="alert alert-'.$CI->session->flashdata('msg_tipo').'">';
			$resultado .='<i class="fa fa-check"></i>';
			$resultado .= $CI->session->flashdata('msg_descricao');
			$resultado .='</div>';
			$resultado .='</div>';
		}
		
		return $resultado;
	}
}

if(! function_exists('alerta_frente')) {
	function alerta_frente() {
		$CI =& get_instance();
		$resultado = '';
		if($CI->session->flashdata('msg_tipo_frente')) {
			$resultado .='<div class="alert alert-'.$CI->session->flashdata('msg_tipo_frente').'">';
			$resultado .= $CI->session->flashdata('msg_descricao_frente');
			$resultado .='</div>';
		}
		return $resultado;
	}
}