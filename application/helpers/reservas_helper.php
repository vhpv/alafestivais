<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists('email_reserva')) {
	function email_reserva($id_reserva)
	{
		$CI =& get_instance();
		
		$reserva = $CI->reservas->get_by_id($id_reserva);
		$evento = $CI->eventos->get_by_id($reserva->event_id);
		
		$data_email['nome'] 				= $reserva->first_name . ' ' . $reserva->last_name;
		$data_email['evento_nome'] 			= $evento->nome;
		$data_email['quantidade'] 			= $reserva->quantity;
		$data_email['trip'] 				= $reserva->trip;
		$data_email['preco'] 				= $reserva->price;
		$data_email['limite_pagamento'] 	= $evento->payment_limit-1;
		$data_email['entidade'] 			= $reserva->mb_ent;
		$data_email['referencia'] 			= $reserva->mb_ref;
		$data_email['montante'] 			= moeda_format($reserva->price*$reserva->quantity);
		$data_email['token'] 				= $reserva->code;
		
		$CI->load->library('email');
		$CI->load->library('parser');
		
		$CI->email->initialize();
		$CI->email->set_newline("\r\n");
		$CI->email->from('noreply@alaviagens.com', 'ALA Viagens');
		$CI->email->to($reserva->email);
		$CI->email->subject('Reserva #' . $id_reserva);
		
		$html_email = $CI->parser->parse('emails/cliente_reserva',$data_email,TRUE);
		
		$CI->email->message($html_email);
		$r = $CI->email->send();
		/*if(!$r){
			print $CI->email->print_debugger();
		}*/
	}
}