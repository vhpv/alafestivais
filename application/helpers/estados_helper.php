<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists('estado')) {
	function estado($estado) {
		$CI =& get_instance();
		$resultado = '';
		switch($estado) {
			case 1:
				$resultado .= '<span class="label label-success"><i class="fa fa-check"></i></span>';
				break;
			default:
			case 0:
				$resultado .= '<span class="label label-warning"><i class="fa fa-exclamation-circle"></i></span>';
				break;
		}

		return $resultado;
	}
}