<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Worker {
	var $C = false;
	function __construct(){
		$this->C =& get_instance();
	}
	
	function render($view,$data = array()) {
		$this->C->load->view('site/common/header',$data);
		$this->C->load->view('site/common/top',$data);
		$this->C->load->view('site/'.$view,$data);	
		$this->C->load->view('site/common/bottom',$data);
		$this->C->load->view('site/common/footer',$data);
	}
	
	function render_admin($view,$data = array()) {
		$this->C->load->view('admin/common/header',$data);
		$this->C->load->view('admin/common/top');
		$this->C->load->view('admin/common/sidebar');
		$this->C->load->view('admin/'.$view,$data);	
		$this->C->load->view('admin/common/bottom',$data);
		$this->C->load->view('admin/common/footer',$data);
	}
	
	function render_print($view,$data = array()) {
		$this->C->load->view('admin/common/header_print',$data);
		$this->C->load->view('admin/'.$view,$data);	
		$this->C->load->view('admin/common/footer_print',$data);
	}
}
?>