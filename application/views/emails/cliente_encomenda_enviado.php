<body style="margin:0; background:#9d4f8e; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#9d4f8e" style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#9d4f8e"><table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr bgcolor="#FFFFFF">
					<td height="150"><table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr>
								<td width="360"><a href="<?=base_url()?>"><img src="<?=base_url('img/email/logo.png')?>" style="display:block;" border="0" /></a></td>
								<td width="360" align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><h1>Encomenda #{encomenda_codigo}</h1></td>
							</tr>
						</table></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td><table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr>
								<td width="360" valign="top"><table width="360" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><strong>{cliente_nome}</strong></td>
										</tr>
										<tr>
											<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">{cliente_morada}</td>
										</tr>
										<tr>
											<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">{cliente_telefone}</td>
										</tr>
										<tr>
											<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">{cliente_email}</td>
										</tr>
									</table></td>
								<td width="360" valign="top"><table width="360" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td colspan="2" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><strong>Envio</strong></td>
										</tr>
										<?php if($encomenda_array['envio_metodo'] == 'll') { ?>
										<tr>
											<td colspan="2" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">A sua encomenda está disponível para levantamento em loja:<br>
												<br>
Avenida José da Silva Soares, 55<br>
4475-100 Maia</td>
										</tr>
										<?php } elseif($encomenda_array['envio_metodo'] == 'ctt') { ?>
										<tr>
											<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px;" colspan="2">A sua encomenda foi expedida.</td>
										</tr>
										<?php } ?>
									</table></td>
							</tr>
					</table></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td height="40">&nbsp;</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><table width="720" align="center" cellpadding="1" cellspacing="3">
							<thead>
								<tr>
									<th style="font-family:Arial, Helvetica, sans-serif; font-size:12px;" align="left">Imagem</th>
									<th style="font-family:Arial, Helvetica, sans-serif; font-size:12px;" align="left">Descrição</th>
									<th style="font-family:Arial, Helvetica, sans-serif; font-size:12px;" align="right">Quant</th>
									<th style="font-family:Arial, Helvetica, sans-serif; font-size:12px;" align="right">Valor U.</th>
									<th style="font-family:Arial, Helvetica, sans-serif; font-size:12px;" align="right">Subtotal</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$subtotal = 0;
								$total_iva = 0;
								$total = 0;
								
								foreach($encomenda_detalhes as $encomenda_detalhe) {
								$total += $encomenda_detalhe->subtotal;
								?>
								<tr>
									<td bgcolor="#f9f9f9">
									<?php if($encomenda_detalhe->tipo == 'produto'){
										$imagem = $this->produtos->get_by_id($encomenda_detalhe->id_produto)->imagem;?><img alt="<?=$encomenda_detalhe->nome?>" src="<?=($imagem)?base_url('media/produtos/thumbs/'.$imagem):base_url('img/no-image-produto.jpg')?>" height="50" width="50"><?php
									} elseif($encomenda_detalhe->tipo == 'fotografia'){$fotografia = $this->fotografias->get_by_id($encomenda_detalhe->id_produto);?><img alt="<?=$encomenda_detalhe->nome?>" src="<?=($fotografia)?base_url('media/fotografias_thumbs/'.$fotografia->directorio.'/'.$fotografia->foto):base_url('img/no-image-produto.jpg')?>" height="50" width="50"><?php
									} ?></td>
									<td bgcolor="#f9f9f9" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><?=$encomenda_detalhe->nome?><?php if($encomenda_detalhe->opcao){ echo ' (' . $encomenda_detalhe->opcao . ')';}?></td>
									<td align="right" bgcolor="#f9f9f9" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><?=$encomenda_detalhe->quantidade?></td>
									<td align="right" bgcolor="#f9f9f9" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">€ <?=moeda_format($encomenda_detalhe->preco)?></td>
									<td align="right" bgcolor="#f9f9f9" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">€ <?=moeda_format($encomenda_detalhe->subtotal)?></td>
								</tr>
								<?php }?>
							</tbody>
					</table></td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td height="40">&nbsp;</td>
				</tr>
				<tr>
					<td bgcolor="#FFFFFF"><table width="720" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td width="360">&nbsp;</td>
							<td width="360"><table width="360" border="0" align="right" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<th height="30" colspan="2" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><h4>Resumo de Valores</h4></th>
									</tr>
									<tr>
										<th align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">Subtotal</th>
										<td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">€ <?=moeda_format($total/1.23)?></td>
									</tr>
									<tr>
										<th align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">IVA (23%)</th>
										<td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">€ <?=moeda_format($total-$total/1.23)?></td>
									</tr>
									<tr>
										<th align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">Portes:</th>
										<td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">€ <?=moeda_format($encomenda_array['envio_valor'])?></td>
									</tr>
									<tr class="active">
										<th align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">Total</th>
										<td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">€ <?=moeda_format($encomenda_array['total'])?></td>
									</tr>
								</tbody>
							</table></td>
						</tr>
					</table></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td height="40">&nbsp;</td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">&nbsp;</td>
	</tr>
</table>
</body>
