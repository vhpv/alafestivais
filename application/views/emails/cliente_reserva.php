<table width="500" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td style="font-size: 20px; color:#FFA300; font-family: 'Source Sans Pro', sans-serif; padding-top:20px;padding-bottom:30px;">
			A tua reserva está confirmada!
		</td>
	</tr>
	<tr>
		<td>
		<?php /*<% @event_place = Place.find(@event.place_id) %>*/ ?>
		<div style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;padding-bottom:20px;">
			Olá <?=$nome?>!
		</div>
		<div style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;padding-bottom:10px;">
			Obrigado pelo teu interesse no nosso serviço de transporte para os melhores concertos do nosso País! Neste momento, podemos já confirmar a tua reserva nas seguintes condições:
		</div>
		<div>
			<ul style="list-style:none;font-family: 'Source Sans Pro', sans-serif; ">
				<li style="font-weight: 700; color:#FFA300">Dados da Viagem</li>
				<li><span style="font-weight: 700;">Evento: </span><?=$evento_nome?></li>
				<li><span style="font-weight: 700;">N.º de Lugares: </span><?=$quantidade?></li>
				<li><span style="font-weight: 700;">Trajecto: </span><?=$trip?></li>
				<?php /*?><% @price_por_pessoa = @reservation.price / @reservation.quantity %><?php */?>
				<li><span style="font-weight: 700;">Preço por pessoa: </span><?=moeda_format($preco)?> €</li>	
				<li><span style="font-weight: 700;">Origem/Destino: </span><?php /*?><%= @place_orgi.name%><?php */?></li>
					<?php /*?><% if @reservation.trip != "Apenas volta" %><?php */?>
				<li><span style="font-weight: 700;">Data do Evento: </span><?php /*?><%= @reservation.datein.strftime("%d-%m-%Y")%><?php */?></li>
               	<li><span style="font-weight: 700;">– Hora e Local de partida na ida: </span><?php /*?><%= @pp.hour %>, <%= @pp.address %><?php */?></li>                                                
				<?php /*?><% end %><?php */?>
				<?php /*?><% if @reservation.trip != "Apenas ida" %><?php */?>
				<li><span style="font-weight: 700;">– Hora e Local de partida na volta: </span><?php /*?><%= @pp.hour_back %>, <%= @event.adress_arrive %><?php */?></li>					<?php /*?><% end %><?php */?>
				<li><span style="font-weight: 700;">Preço total: </span><?=moeda_format($quantidade*$preco)?> €</li>	
			</ul>
		</div>
		<div>
			<p>Podes gerir a tua reserva seguindo a ligação:<br />
			<?=site_url('reservas/actualizar/'.$token);?>
			</p>
		</div>
	<div id="infotexto" style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px; padding-bottom:20px;padding-top:20px;">Podes pagar a reserva através de Multibanco ou num dos nossos balcões, dentro de <b><?=$limite_pagamento?></b> horas após o envio deste e-mail. 
	</div>
	<div id="textopagamento" style="">
		<span style="font-family: 'Source Sans Pro', sans-serif; font-size: 20px; color:#FFA300;">
		1. PAGAMENTO POR MULTIBANCO
		</span>
		<br>
		<span style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;">
		Podes pagar com toda a segurança através da opção “Pagamento de Serviços” em qualquer terminal Multibanco ou através do serviço homebanking do teu banco. Os dados de pagamento são os seguintes:
		</span>
		<br>
		<div id="tabelapagamento" style="padding-top:30px;font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;">
			<div style="float:left;">
			  <img src="<?=base_url('img/email/mb.png')?>" alt="Logo MB" width="60" height="auto">
			</div>
			<div style="float:left;">
				<ul style="list-style:none;">
					<li><span style="font-weight: 700;">Entidade: </span><?=$entidade?> (“IFMB”)</li>
					<li><span style="font-weight: 700;">Referência: </span><?=$referencia?></li>
					<li><span style="font-weight: 700;">Valor: </span><?=$montante?> €</li>
		</ul>
			</div>
		</div>
	</div>
	<br>
	<div style="padding-bottom:20px;padding-top:20px;clear:left;">
	<br>
		<span style="font-family: 'Source Sans Pro', sans-serif; font-size: 20px; color:#FFA300;padding-top:20px;">
		2. PAGAMENTO NA SOLNORTE – AGÊNCIA DE VIAGENS
		</span>
		<br>
		<span style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;">
		Podes também pagar nos balcões da Solnorte, na Maia e em Ermesinde. Consulta as nossas moradas e contactos, www.solnorte.pt .
		</span>
		
		<br>
		<span style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;padding-top:20px;">
		O processamento do teu pagamento pode demorar até 12 horas. Assim que esteja registado, receberás um e-mail de confirmação com o título de transporte.
		</span>
		<br><br>
                                    
		<span style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;padding-top:20px;">
		Se tiveres alguma dúvida ou dificuldade no pagamento, não hesites em contactar-nos através do e-mail alafestivais@alaviagens.com ou do número 910405582.
		</span>
		<span style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;padding-top:20px;padding-bottom:20px">
		<br><br>
                
		Até breve,
		</span>
		<br>
		<img src="<?=base_url('img/ala_viagens.png')?>" ALT="Ala Viagens" WIDTH="200" HEIGHT="auto">
		<br>
		<div style=style="font-family: 'Source Sans Pro', sans-serif; color:black;font-size: 12px;padding-top:10px;">
		<span style="font-weight:bold;">ALA VIAGENS</span>
		<br>Agência de Viagens e Turismo
		<br>Veloso Vieira & Batista Oliveira, Lda.
		<br>Rua Escultor Barata Feyo, n.º 140, 3.12
		<br>4250-076 Porto
		<br>RNAVT n.º 4196
		<br>NIPC 510827969                                
		<br><br>Visite o nosso site em:
		<br><a href="www.alaviagens.com" target="_blank">www.alaviagens.com</a>
                <br><br>Siga-nos no Facebook:                
		<br><a href="www.fb.com/alaviagens" target="_blank">fb.com/alaviagens</a>
		</div>                
		<div>
		    		<p style="font-family: 'Source Sans Pro', sans-serif; font-size: 10px; color:#000000">Termos e Condições</p>
		    		<p style="font-family: 'Source Sans Pro', sans-serif; font-size: 10px; color:#c0c0c0">Condições gerais<br>                                
1. O objectivo deste site é a promoção do serviço de transporte para concertos e festivais oferecido pela Veloso Vieira & Batista Oliveira, Lda., doravante designada por ALA Viagens, com sede na rua Escultor Barata Feyo, nº140, 3.12, registada no Registo Nacional de Agências de Viagens e Turismo (RNAVT) sob o número 4196.

2. A ALA Viagens é uma marca registada operada pela Veloso Vieira & Batista Oliveira, Lda.

3. O serviço de transporte é efectuado em autocarros de turismo.

4. Todos os preços apresentados incluem IVA à taxa em vigor.

5. Através deste site é proporcionada a possibilidade de se submeter um pedido de reserva de transporte.

6. A comunicação com o cliente é efectuada através de correio electrónico.

7. A cada pedido de reserva é atribuído um prazo de pagamento, devidamente indicado no e-mail de pedido de pagamento enviado após o pedido de reserva.

8. Os pagamentos efectuados após o término do prazo de pagamento poderão não ser aceites. Nesse caso, a devolução do valor poderá ser efectuada, abatendo-se ao valor pago uma taxa de pelo menos 2 euros pelo processamento da devolução.

9. Os horários apresentados aquando do pedido de reserva poderão ser alterados unilateralmente pela ALA Viagens, face a situações imprevistas ou para adequar a escala do serviço à procura efectiva.

10. Se a alteração for superior a 2 horas face ao horário inicial e motivada pela adequação da escala do serviço à procura efectiva, o cliente tem o direito ao ressarcimento dos valores pagos se pretender anular a reserva.

11. A ALA Viagens reserva-se o direito de não efectuar qualquer devolução dos valores pagos no caso de pedido de cancelamento unilateral por parte do cliente.

12. A realização das viagens a partir de cada uma das origens disponibilizadas para cada evento está sujeita à existência de um grupo mínimo de 25 pessoas com lugares pagos.

13. No caso de não se verificar a existência do grupo mínimo referido no ponto anterior, a ALA Viagens reserva-se o direito de cancelar o serviço, devendo então proceder à devolução dos valores pagos pelos clientes.

14. A informação sobre o cancelamento do serviço deve ser comunicada ao cliente atempadamente, de forma a permitir a reserva de meios de transporte alternativos.

15. O lugar utilizado na viagem de ida para o concerto deverá ser o mesmo a utilizar na viagem de regresso. Todos aqueles que reservem apenas “viagem de regresso” ficam sujeitos aos lugares disponíveis nesse trajecto.

16. A ALA Viagens reserva-se o direito de alterar a qualquer momento as presentes condições gerais.

17. A utilização deste site e deste serviço pressupõe a aceitação, na íntegra, das presentes condições gerais.

			    	</p>
		</div>
	</div>
</td></tr></table>