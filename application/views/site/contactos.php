<div class="wrapper">
	<!-- Como -->
	<div class="custom-content-title">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h2>Contacta-nos</h2>
				</div>
				<div class="col-md-4 col-right">
					<img src="<?=base_url('assets/images/event-title.svg')?>">
					...e esclarece as tuas dúvidas!
				</div>
			</div>
		</div>
	</div>
	<!-- Como -->
	
	<div class="custom-content-headline contactos-top" style="background-image:url(<?=base_url('assets/images/contactos_2.jpg')?>)">
		<div class="container">
			<div class="row">
				<h2>Fala connosco! Estamos disponíveis através de Facebook, Email, Telefone ou Chat. Em dias de eventos, estamos sempre contactáveis.</h2>
			</div>
		</div>
	</div>
	
	<div class="contactos">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
				
					<h3>Morada</h2>
					<p>​Rua Escultor Barata Feyo, n.º 140, 3.12</br>4250-076 Porto, Portugal</p>

					<h3>E-mail</h3>
					<p><a href="mailto:alafestivais@alaviagens.com">alafestivais@alaviagens.com</a></p>
					
					<h3>Telefone</h3>
					<p><a href="tel:223 222 476">223 222 476</a> Horário: 11h - 18h</p>
					
					<h3>Telemóvel</h3>
					<p><a href="tel:910 405 582">910 405 582</a> Horário: 11h - 18h</p>
				
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6005.919502429372!2d-8.62207849168041!3d41.17904506618017!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2465c9f9009079%3A0x25ad1119e4d6f27f!2sALA+Viagens!5e0!3m2!1spt-PT!2spt!4v1414146010791" class="map_iframe"  frameborder="0" style="border:0"></iframe>
				</div>
				<div class="col-md-6">
					<form accept-charset="UTF-8" method="post" action="<?=current_url()?>" id="contactus" class="form">
						<div><span class="error"></span></div>
						<div class="form-group">
							<label for="name">Nome Completo* </label>
							<input type="text" class="form-control" placeholder="" title="Nome Completo*: " maxlength="50" value="" id="name" name="name">
							<span class="error" id="contactus_name_errorloc"></span>
						</div>
						<div class="form-group">
							<label for="email">Email*</label>
							<input type="text" class="form-control" placeholder="" maxlength="50" value="" id="email" name="email">
							<span class="error" id="contactus_email_errorloc"></span>
						</div>
						<div class="form-group">
							<label for="telefone">Nº de telefone</label>
							<input type="text" class="form-control" placeholder="" maxlength="50" value="" id="telefone" name="telefone">
							<span class="error" id="contactus_email_errorloc"></span>
						</div>
						<div class="form-group">
							<label for="assunto">Assunto</label>
							<input type="text" class="form-control" placeholder="" maxlength="50" value="" id="empresa" name="assunto">
							<span class="error" id="contactus_email_errorloc"></span> </div>
						<div class="form-group">
							<label for="messagem">Mensagem</label>
							<span class="error" class="form-control" id="contactus_message_errorloc"></span>
							<textarea id="message" class="form-control" placeholder="" name="messagem" cols="50" rows="10"></textarea>
						</div>
												
						<div class="submit">
							
							<img class="event-submit" src="<?=base_url('assets/images/event-submit.svg')?>">
							<button class="btn btn-default" type="submit" name="Enviar">ENVIAR <i class="material-icons">chevron_right</i></button>
						</div>
						
						<div class="short_explanation">* Dados Obrigatórios</div>
						
					</form>
				
				</div>
			</div>
		</div>
	</div>
</div>