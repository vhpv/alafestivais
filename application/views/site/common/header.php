<!DOCTYPE html>
<html>
<head>

<!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimun-scale=1.0, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link rel="icon" type="image/x-icon" href="<?=base_url('assets/images/favicon.ico')?>">

<title>ALA Viagens</title>

<meta name="description" content="Especialistas em viagens para concertos">
<meta name="author" content="Ala Viagens">
<meta name="keywords" content="viagens, concertos, transporte, festivais, ala viagens, transporte festivais, transporte para concertos, viagens baratas" />
<meta property="og:title" content="ALA Viagens"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://www.alafestivais.com/"/>
<meta property="og:locale" content="pt_PT" />
<meta property="og:description" content="Especialistas em viagens para concertos ao melhor preço"/>
<meta property="og:image" content="http://www.alafestivais.com/assets/alaviagens_facebook.jpg" />

<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/css/main.css?'.date("YmdHis"))?>" rel="stylesheet" type="text/css">
<link href="<?=base_url('assets/css/responsive.css?'.date("YmdHis"))?>" rel="stylesheet" type="text/css">


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">

<script src="<?=base_url('assets/js/jquery-3.2.1.min.js')?>" type="text/javascript"></script>
<script>
	var baseUrl = <?= "'" . base_url('index.php') . "'";?>;
	localStorage.setItem("baseUrl",baseUrl);
</script>
</head>
<body>
<?php /*** GOOGLE ANALYTICS HERE ****/ ?>

	