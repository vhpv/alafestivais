<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle jq_toggle_search">
				<i class="material-icons">search</i>
			</button>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
				<i class="material-icons">menu</i>
			</button>
			<a class="navbar-brand" href="<?=base_url();?>">
				<img src="<?=base_url('assets/images/logo.svg')?>" alt="Ala Viagens" class="mt2"/>
			</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<form class="navbar-form navbar-right search-module" role="search">
			  <div class="form-group">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Pesquisar...">
					<span class="input-group-addon"><i class="material-icons">search</i></span>
				</div>
			  </div>
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li id="eventos"><a href="<?=site_url();?>">Eventos</a></li>
				<li id="porque"><a href="<?=site_url('porque')?>">Porquê connosco?</a></li>
				<li id="como"><a href="<?=site_url('como')?>">Como reservar</a></li>
				<li id="historico"><a href="<?=site_url('historico')?>">Arquivo</a></li>
				<li id="contactos"><a href="<?=site_url('contactos')?>">Contactos</a></li>
			</ul>
		</div>
	</div>
</nav>
<div class="wrapper">


<!--ul>
	<li><a href="mailto:info@alaviagens.com"><img src="<?=base_url('img/social_email.png')?>" alt="Ala Viagens email" class="fl mr1"/><p>info@alaviagens.com</p></a></li>
	<li><a href="#"><img src="<?=base_url('img/social_tlf.png')?>" alt="Ala Viagens telefone" class="fl mr1"/><p>910 405 582</p></a></li>
	<li><a href="http://www.facebook.com/alaviagens" target="_blank"><img src="<?=base_url('img/social_face.png')?>" alt="Ala Viagens facebook" class="fl mr1"/><p>facebook.com/alaviagens</p></a></li>
</ul-->