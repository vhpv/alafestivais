<?php

$uri = explode("/",$_SERVER['REQUEST_URI']);

if( $uri[count($uri)-1] != "porque"):

?>

<div class="promo-bottom-top">
	<img src="<?=base_url('assets/images/promo-bottom-top.svg')?>">
</div>
<div class="promo-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-4 illustration-bus">
				<h4>Vamos contigo!</h4>
				<i class="material-icons">directions_bus</i>
			</div>
			<div class="col-md-8 text-center advantages">
				<h3>Vantagens de viajar connosco</h3>
				<div class="row">
					<div class="col-sm-6 clearfix">
						<div class="illustration-pair">
							<i class="material-icons">local_offer</i>
							<i class="material-icons">local_offer</i>
						</div>
						<h4>Melhor preço do mercado</h4>
					</div>
					<div class="col-sm-6 clearfix">
						<div class="illustration-pair">
							<i class="material-icons">watch_later</i>
							<i class="material-icons">watch_later</i>
						</div>
						<h4>Regresso após o concerto</h4>
					</div>
					<div class="col-sm-6 clearfix">
						<div class="illustration-pair">
							<i class="material-icons">lock</i>
							<i class="material-icons">lock</i>
						</div>
						<h4>Segurança e credibilidade</h4>
					</div>
					<div class="col-sm-6 clearfix">
						<div class="illustration-pair">
							<i class="material-icons">beenhere</i>
							<i class="material-icons">beenhere</i>
						</div>
						<h4>Levamos-te até ao recinto</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php endif; ?>