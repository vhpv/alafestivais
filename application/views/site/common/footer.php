	</div>

	<footer>
		<div class="pre-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<h3><span>www.alafestivais.com</span> é uma marca:</h3>
						<img src="<?=base_url('assets/images/ala-viagens.svg')?>">
					</div>
					<div class="col-sm-4">
						<h3>ALA FESTIVAIS</h3>
						<p><a href="<?=site_url();?>">Eventos</a></p>
						<p><a href="<?=site_url('porque')?>">Porquê connosco?</a></p>
						<p><a href="<?=site_url('como')?>">Como reservar</a></p>
						<p><a href="<?=site_url('historico')?>">Arquivo</a></p>
						<p><a class="jq_termos_e_condicoes" href="#">Termos e condições</a></p>
					</div>
					<div class="col-sm-4">
						<h3>FALA CONNOSCO</h3>
						<p><a href="mailto:alafestivais@alaviagens.com">alafestivais@alaviagens.com</a></p>
						<p><a href="tel:910 405 582">910 405 582</a></p>
						<br>
						<h3>SEGUE-NOS NAS REDES SOCIAIS</h3>
						<a href="https://www.facebook.com/alaviagens" target="_blank" class="social-media"><img src="<?=base_url('assets/images/facebook.svg')?>"></a>
						<a href="https://twitter.com/alaviagens" target="_blank" class="social-media"><img src="<?=base_url('assets/images/twitter.svg')?>"></a>
						<a href="https://www.instagram.com/alaviagens/" target="_blank" class="social-media"><img src="<?=base_url('assets/images/instagram.svg')?>"></a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<a class="copyright" href="http://www.alaviagens.com/" target="_blank">
							<strong>ALA VIAGENS</strong> <?=date('Y');?>
						</a>
					</div>
					<div class="col-md-6">
						<script src="https://blk.pt/our_info/blek.js"></script>
						<script type="text/javascript">BLEKINFO.init('bimotor');</script>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<!--<script src="<?=base_url('js/ala.js?v='.date('YmdHis'))?>" type="text/javascript"></script>-->
	
	<script src="<?=base_url('js/jquery.slides.min.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('js/jquery.validate.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('assets/js/jquery.ajax_call.js')?>" type="text/javascript"></script>
	
	<script src="<?=base_url('assets/js/bootstrap.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('assets/js/jquery.waypoints.min.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('assets/js/inview.min.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('assets/js/main.js?'.date("YmdHis"))?>" type="text/javascript"></script>
	
	<div class="search-backdrop"></div>
	<div class="search-container">
		<h2>A pesquisar</h2>
		<i class="material-icons close-search">close</i>
		<div class="search-placeholder">
			<input type="text">
		</div>
		<div class="search-results"></div>
	</div>
	
	<div class="layer" id="termos">
		<i class="material-icons layer-close">close</i>
		<div class="layer-inner">
			<h2>Termos e Condições</h2>
			<p><strong>1.</strong>O objectivo deste site é a promoção do serviço de transporte para concertos e festivais oferecido pela Veloso Vieira & Batista Oliveira, Lda., doravante designada por ALA Viagens, com sede na rua Escultor Barata Feyo, nº140, 3.12, registada no Registo Nacional de Agências de Viagens e Turismo (RNAVT) sob o número 4196.</p>
			<p><strong>2.</strong>A ALA Viagens é uma marca registada operada pela Veloso Vieira & Batista Oliveira, Lda.</p>
			<p><strong>3.</strong>O serviço de transporte é efectuado em autocarros de turismo.</p>
			<p><strong>4.</strong>Todos os preços apresentados incluem IVA à taxa em vigor.</p>
			<p><strong>5.</strong>Através deste site é proporcionada a possibilidade de se submeter um pedido de reserva de transporte.</p>
			<p><strong>6.</strong>A comunicação com o cliente é efectuada através de correio electrónico.</p>
			<p><strong>7.</strong>A cada pedido de reserva é atribuído um prazo de pagamento, devidamente indicado no e-mail de pedido de pagamento enviado após o pedido de reserva.</p>
			<p><strong>8.</strong>Os pagamentos efectuados após o término do prazo de pagamento poderão não ser aceites. Nesse caso, a devolução do valor poderá ser efectuada, abatendo-se ao valor pago uma taxa de pelo menos 2 euros pelo processamento da devolução.</p>
			<p><strong>9.</strong>Os horários apresentados aquando do pedido de reserva poderão ser alterados unilateralmente pela ALA Viagens, face a situações imprevistas ou para adequar a escala do serviço à procura efectiva.</p>
			<p><strong>10.</strong>Se a alteração for superior a 2 horas face ao horário inicial e motivada pela adequação da escala do serviço à procura efectiva, o cliente tem o direito ao ressarcimento dos valores pagos se pretender anular a reserva.</p>
			<p><strong>11.</strong>A ALA Viagens reserva-se o direito de não efectuar qualquer devolução dos valores pagos no caso de pedido de cancelamento unilateral por parte do cliente.</p>
			<p><strong>12.</strong>A realização das viagens a partir de cada uma das origens disponibilizadas para cada evento está sujeita à existência de um grupo mínimo de 25 pessoas com lugares pagos.</p>
			<p><strong>13.</strong>No caso de não se verificar a existência do grupo mínimo referido no ponto anterior, a ALA Viagens reserva-se o direito de cancelar o serviço, devendo então proceder à devolução dos valores pagos pelos clientes.</p>
			<p><strong>14.</strong>A informação sobre o cancelamento do serviço deve ser comunicada ao cliente atempadamente, de forma a permitir a reserva de meios de transporte alternativos.</p>
			<p><strong>15.</strong>O lugar utilizado na viagem de ida para o concerto deverá ser o mesmo a utilizar na viagem de regresso. Todos aqueles que reservem apenas “viagem de regresso” ficam sujeitos aos lugares disponíveis nesse trajecto.</p>
			<p><strong>16.</strong>A ALA Viagens reserva-se o direito de alterar a qualquer momento as presentes condições gerais.</p>
			<p><strong>17.</strong>A utilização deste site e deste serviço pressupõe a aceitação, na íntegra, das presentes condições gerais.</p>
		</div>
	</div>
	
</body>

</html>