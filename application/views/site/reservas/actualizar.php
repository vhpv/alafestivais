<div class="grid centeredblock content_t mt2">
	<div class="update_reservation">
		<h1>Olá <?=$reserva->first_name?> <?=$reserva->last_name?></h1>
		<p>Aqui podes gerir a tua reserva.</p>
		<p>O código da tua reserva é: <strong><?=$reserva->reservation_code?></strong></p>
		<hr />
		<p>Aqui podes juntar os teus amigos, faremos todos os esforços para viajarem juntos.</p>
		<form action="<?=current_url()?>" method="post">
			<p>Introduz o código da reserva dos teus amigos.</p><br />
			<div class="listFriends">
				<?php if(count($friends)>0) { ?>
					<?php foreach($friends as $friend){ ?>
					<div class="row">
						<div class="field">
							<input type="text" name="friend[]" placeholder="Introduz o código da reserva" value="<?=$friend->friend_code?>" />
						</div>
						<div class="field_button"><button type="button" class="removeFriend">remover</button></div>
					</div>
					<?php } ?>
				<?php } else {?>
					<div class="row">
						<div class="field">
							<input type="text" name="friend[]" placeholder="Introduz o código da reserva" />
						</div>
						<div class="field_button"><button type="button" class="removeFriend">remover</button></div>
					</div>
				<?php } ?>
				<?php if(count($friends)>0 && count($friends)<9){?>
				<div class="row">
						<div class="field">
							<input type="text" name="friend[]" placeholder="Introduz o código da reserva" />
						</div>
						<div class="field_button"><button type="button" class="removeFriend">remover</button></div>
					</div>
				<?php } ?>
				
			</div>
			<div><button type="button" class="addFriend">Adicionar amigo</button> <button name="save_button" value="save" type="submit">Guardar</button></div>
		</form>
		<div style="display:none;" class="hidden_row">
			<div class="row">
				<div class="field">
					<input type="text" name="friend[]" placeholder="Introduz o código da reserva" />
				</div>
				<div class="field_button"><button type="button" class="removeFriend">remover</button></div>
			</div>
		</div>
	</div>
</div>