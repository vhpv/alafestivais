<div class="wrapper">
	<!-- Porque -->
	<div class="custom-content-title">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h2>Porquê connosco?</h2>
				</div>
				<div class="col-sm-4 col-right">
					<img src="<?=base_url('assets/images/event-title.svg')?>">
					Descobre as vantagens!
				</div>
			</div>
		</div>
	</div>
	<!-- Porque -->
	
	<div class="custom-content-headline" style="background-image:url(<?=base_url('assets/images/live-concert.jpeg')?>)"></div>
</div>

<div class="vantagens">
	<div class="container">
		<h3>Vantagens de viajar connosco</h3>
		<div class="row">
			<div class="col-sm-5 clearfix">
				<div class="illustration-pair">
					<i class="material-icons">local_offer</i>
					<i class="material-icons">local_offer</i>
				</div>
				<h4>Melhor preço do mercado</h4>
				<p>Temos o melhor preço do mercado na viagem de ida-e-volta comparativamente com outros serviços de autocarro, de comboio e de carro.</p>
			</div>
			<div class="col-sm-5 col-sm-offset-1 clearfix">
				<div class="illustration-pair">
					<i class="material-icons">watch_later</i>
					<i class="material-icons">watch_later</i>
				</div>
				<h4>Regresso após o concerto</h4>
				<p>Os nossos horários são adequados a cada evento e o transporte é direto até ao recinto — se o concerto se atrasar, nós aguardamos. Não deixamos ninguém para trás!</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-5 clearfix">
				<div class="illustration-pair">
					<i class="material-icons">lock</i>
					<i class="material-icons">lock</i>
				</div>
				<h4>Segurança e credibilidade</h4>
				<p>Temos mais de 20 mil clientes satisfeitos: 99% voltariam a viajar connosco e aconselhariam o nosso serviço.</p>
			</div>
			<div class="col-sm-5 col-sm-offset-1 clearfix">
				<div class="illustration-pair">
					<i class="material-icons">beenhere</i>
					<i class="material-icons">beenhere</i>
				</div>
				<h4>Levamos-te até ao recinto</h4>
				<p>A viagem connosco é uma experiência social única — viajar em conjunto comt um grupo que vai ver a mesma banda é meio caminho andado para fazer novos amigos. </p>
			</div>
			
		</div>
		<div class="row">
			<div class="col-sm-5 clearfix">
				<div class="illustration-pair">
					<i class="material-icons">airline_seat_recline_extra</i>
					<i class="material-icons">airline_seat_recline_extra</i>
				</div>
				<h4>Atenção total ao cliente</h4>
				<p>Cada autocarro conta com um monitor especializado. Caso o cliente não esteja presente pouco antes da partida entramos em contacto. Estamos permanentemente disponíveis por <a href="tel:910 405 582">telefone</a>, <a href="mailto:alafestivais@alaviagens.com">e-mail</a>, <a href="https://www.facebook.com/alaviagens" target="_blank">Facebook</a>, chat e na nossa sede.</p>
			</div>
			<div class="col-sm-5 col-sm-offset-1 clearfix">
				<div class="illustration-pair">
					<i class="material-icons">local_florist</i>
					<i class="material-icons">local_florist</i>
				</div>
				<h4>Consciência Ecológica</h4>
				<p>Porque queremos contribuir para um mundo mais sustentável, o nosso serviço é o mais ecológico: a redução de emissões face ao transporte particular é de 94%.</p>
			</div>
			
		</div>
	</div>
</div>
<div class="vantagens-bottom">
	<img src="<?=base_url('assets/images/promo-bottom-top.svg')?>">
</div>



<!-- Press Release -->
<div class="press-release clearfix">
	<div class="container">
		<a href="http://www.optimusalive.com/parceiros" target="_blank">
			<div class="in-wrapper">
				<img src="<?=base_url('assets/images/press_optimus.jpg')?>" alt="ALA no Optimus Alive"/>
				<h6>ALA no Optimus Alive</h6>
				<p>Somos parceiros oficiais do Optimus Alive!</p>
			</div>
		</a>

		<a href="http://www.everythingisnew.pt/noticias/ala-viagens-cria-autocarro-especial-do-norte-e-centro-para-o-night-day-dos-the-xx" target="_blank">
			<div class="in-wrapper">
				<img src="<?=base_url('assets/images/press_everything.jpg')?>" alt="ALA e Everything is New"/>
				<h6>ALA e Everything is New</h6>
				<p>Fomos transporte oficial dos The XX, em Belém.</p>
			</div>
		</a>

		<a href="http://p3.publico.pt/cultura/mp3/2686/ala-viagens-garante-transporte-custos-mais-baixos-para-os-festivais-de-verao" target="_blank">
			<div class="in-wrapper">
				<img src="<?=base_url('assets/images/press_p3.jpg')?>" alt="ALA na P3"/>
				<h6>ALA na P3</h6>
				<p>Saímos numa reportagem sobre a Ala nos festivais</p>
			</div>
		</a>

		<a href="../assets/presstime2.jpg" rel="lightbox">
			<div class="in-wrapper">
				<img src="<?=base_url('assets/images/press_time.jpg')?>" alt="ALA no Time Out"/>
				<h6>ALA no Time Out</h6>
				<p>A nossa viagem a Barcelona foi notícia na TimeOut!</p>
			</div>
		</a>

		<a href="https://www.facebook.com/photo.php?v=4321916847618&set=o.250237895004023&type=3&theater" target="_blank">
			<div class="in-wrapper">
				<img src="<?=base_url('assets/images/press_radio.jpg')?>" alt="ALA na Rádio Nova"/>
				<h6>ALA na Rádio Nova</h6>
				<p>Falámos com a Rádio Nova sobre as nossas viagens.</p>
			</div>
		</a>
	</div>
</div>

<!-- Press Release -->


<!-- infografia -->
<!--<div class="grid vantagem centeredblock mb4 oh">
	<h1>VANTAGENS DE VIAJAR CONNOSCO</h1>

	<ul id="why" class="grid centeredblock mb2 mt2">
		<li class="fl">
			<img src="<?=base_url('img/porque_preco.png')?>" alt="Melhor preço"/>
			<p>MELHOR PREÇO<br>DO MERCADO</p>
		</li>
		<li class="fl">
			<img src="<?=base_url('img/porque_tempo.png')?>" alt="Melhor preço"/>
			<p>REGRESSO APÓS<br>O CONCERTO</p>
		</li>
		<li class="fl">
			<img src="<?=base_url('img/porque_seguro.png')?>" alt="Melhor preço"/>
			<p>SEGURANÇA<br>E CREDIBILIDADE</p>
		</li>
		<li class="fl negvantagem">
			<img src="<?=base_url('img/porque_levamos.png')?>" alt="Melhor preço"/>
			<p>LEVAMOS-TE ATÉ<br>AO RECINTO</p>
		</li>
	</ul>

</div>-->
<!-- infografia -->