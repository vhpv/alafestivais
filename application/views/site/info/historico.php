	<!-- Arquivo -->
	<div class="custom-content-title">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h2>Arquivo</h2>
				</div>
				<div class="col-sm-4 col-right">
					<img src="<?=base_url('assets/images/event-title.svg')?>">
					Viaja até ao passado!
				</div>
			</div>
		</div>
	</div>
	<div class="custom-content-headline como" style="background-image:url(<?=base_url('assets/images/arquivo.jpg')?>)">
		<div class="container">
			<div class="row">
				<h2>Consulta aqui todos os eventos em que a ALA Festivais já marcou presença. Até hoje, já contamos com mais de 20 mil clientes!</h2>
			</div>
		</div>
	</div>
	<!-- Arquivo -->
	<div class="container">
		<div class="row events-mosaic">
			<?php $counter_eventos=0;
			
			foreach($eventos as $i=>$undefined){
				$eventos_php[]=json_decode(json_encode($eventos[$i]),true);
			}
			
			$eventos = msort($eventos_php, array('data_inicio'));

			

			?>
			<?php foreach($eventos as $evento){
				$counter_eventos++;
				if($counter_eventos<7){
				?>
				<div class="col-xs-12 col-md-6 col-lg-4">
					<div class="event">
						<!--taken-->
						<div class="image" style="background-image:url('<?= base_url('media/events/'.$evento["id"].'/medium/'.$evento["image_file_name"]) ?>')"></div>
						<!--<div class="traveled-with-us">
							<i class="material-icons">transfer_within_a_station</i>
							<p>Viajaram<br>connosco</p>
							<h4><?=$evento["taken"]?></h4>
							<p>pessoas</p>
						</div>-->
						<div class="title">
							<?=$evento["nome"]?>
						</div>
						<div class="info text-uppercase">
							<div class="row">
								<div class="col-xs-12">
									<i class="material-icons">today</i>
									<?=strftime("%d %h %Y",strtotime($evento["data_inicio"]));?>
									<?php
				if(date('Y-m-d',strtotime($evento["data_inicio"])) != date('Y-m-d',strtotime($evento["data_fim"]))) {
										print '&nbsp-&nbsp';
										print date('d',strtotime($evento["data_fim"])) . ' ' . strftime('%h',strtotime($evento["data_fim"]));
									}?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php }
			} ?> </div>
			<div class="row events-archive"> <?
			for($i=6;$i<count($eventos);$i++){ ?>
				<div class="event">
				<? echo '<span class="badge badge-default">'.strftime("%d %h %Y",strtotime($eventos[$i]["data_inicio"]))."</span>".$eventos[$i]["nome"]."<br>"; ?>
				</div>
			<?php } ?>
		</div>
	</div>
	
	
	<?

function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            arsort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

?>