<div class="wrapper">
	<!-- Como -->
	<div class="custom-content-title">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h2>Como reservar</h2>
				</div>
				<div class="col-sm-4 col-right">
					<img src="<?=base_url('assets/images/event-title.svg')?>">
					...ao melhor preço do mercado!
				</div>
			</div>
		</div>
	</div>
	<!-- Como -->
	
	<div class="custom-content-headline como" style="background-image:url(<?=base_url('assets/images/reserva-online.jpg')?>)">
		<div class="container">
			<div class="row">
				<h2>Garante o transporte para o teu concerto ou festival, no nosso website a partir do teu computador ou smartphone.</h2>
			</div>
		</div>
	</div>
</div>

<!-- infografia -->
<div class="como-reservar clearfix">
	<div class="container">
	<h2>COMO RESERVAR</h2>
	<ul>
		<li>
			<img src="<?=base_url('assets/images/como-01.svg')?>"/>
			<h3>1</h3>
			<p>Escolhe o teu concerto ou festival</p>
		</li>
		<li>
			<img src="<?=base_url('assets/images/como-02.svg')?>"/>
			<h3>2</h3>
			<p>Faz a tua reserva no nosso site</p>
		</li>
		<li>
			<img src="<?=base_url('assets/images/como-03.svg')?>"/>
			<h3>3</h3>
			<p>Efectua o pagamento por multibanco ou nos nossos balcões</p>
		</li>
		<li>
			<img src="<?=base_url('assets/images/como-04.svg')?>"/>
			<h3>4</h3>
			<p>Recebe de imediato o bilhete de transporte por e-mail</p>
		</li>
		<li>
			<img src="<?=base_url('assets/images/como-05.svg')?>"/>
			<h3>5</h3>
			<p>Encontramo-nos lá</p>
		</li>
	</ul>
	</div>
</div>

<!-- infografia -->