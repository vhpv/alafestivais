

<div id="schedule" class="grid funciona centeredblock mb2 mt2 oh">

	<h1>PREÇO E HORÁRIOS DISPONÍVEIS</h1>
	
	<div class="centeredblock mb6 horario">
		<p>1. ESCOLHE O TEU CONCERTO OU FESTIVAL</p>
		<p>2. ESCOLHE O TEU LOCAL DE ORIGEM</p>
	</div>
	<div class="centeredblock horario">
		<div class="fl mr4 mb4">
			<%= select_tag "Eventos", options_from_collection_for_select(@events, "id", "name"), :prompt => "Seleccione um evento", :class => 'select_evento' %>
		</div>
		<div id="places" class="horarios mr4 fl mb4">
			<select>
				<option>Seleccione um local</option>
			</select>
		</div>
		
		<div id="horario" class="fl mr4">
			<!-- horario de partida -->
		</div>
		
		<div id="preco" class="fl">
			<!- preco de ida e volta -->
		</div>
	</div>

</div>

<script>
	$(function($) {
        $("#Eventos").change(function() {
            $.ajax({url: '/public/places_hours_update',
            type: "GET",
            data: {'eventos' : $('#Eventos option:selected').val()},
            dataType: "html",
            success: function(response){
                var result = $(response).find('#places').html();
                $('#places').html(result);
                
                }
            });  
        });
      });
      
      $(function($) {
        $("#places").change(function() {
            $.ajax({url: '/public/places_hours_update',
            type: "GET",
            data: {'eventos' : $('#Eventos option:selected').val(),'city' : $('#cidades option:selected').val()},
            dataType: "html",
            success: function(response){
                var result = $(response).find('#horario').html();
                var result2 = $(response).find('#preco').html();
                $('#horario').html(result);
                $('#preco').html(result2);
                
                }
            });  
        });
      });
</script>