<!DOCTYPE html>
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
	<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimun-scale=1.0, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>ALA Viagens</title>

	<link href="<?=base_url('assets/css/bootstrap.css')?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/css/main.css?'.date("YmdHis"))?>" rel="stylesheet" type="text/css">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">

</head>
<body class="outro-distrito">
	<?php if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['telefone']) && !empty($_POST['distrito']) && !empty($_POST['quantidade'])){?>
		<div class="alert alert-success">
			<h4>Obrigado pelo contacto!</h4>
			<p>Serás contactado em breve!</p>
		</div>
	<?php } ?>
	<form id="outro-distrito" action="<?=current_url()?>" class="form" method="post" accept-charset="UTF-8">
		<div><span class="error"></span></div>
		<div class="form-group"> 
			<label class="control-label">Nome Completo *</label>
			<input type="text" class="form-control" name="name" id="name" value="" maxlength="50" placeholder="" />
		</div>
		<div class="form-group"> 
			<label class="control-label">Email *</label>
			<input type="text" class="form-control" name="email" id="email" value="" maxlength="50" placeholder="" />
		</div>
		<div class="form-group"> 
			<label class="control-label">Nº de telefone *</label>
			<input type="text" class="form-control" name="telefone" id="telefone" value="" maxlength="50" placeholder="" />
		</div>
		<div class="form-group"> 
			<label class="control-label">Qual é o teu distrito? *</label>
			<select class="form-control" name="distrito" id="distrito">
				<option value="aveiro">Aveiro</option>
				<option value="beja">Beja</option>
				<option value="braga">Braga</option>
				<option value="braganca">Bragança</option>
				<option value="castelo_branco">Castelo Branco</option>
				<option value="coimbra">Coimbra</option>
				<option value="evora">Évora</option>
				<option value="faro">Faro</option>
				<option value="guarda">Guarda</option>
				<option value="leiria">Leiria</option>
				<option value="lisboa">Lisboa</option>
				<option value="portalegre">Portalegre</option>
				<option value="porto">Porto</option>
				<option value="santarem">Santarém</option>
				<option value="setubal">Setúbal</option>
				<option value="viana_do_castelo">Viana do Castelo</option>
				<option value="vila_real">Vila Real</option>
				<option value="viseu">Viseu</option>
			</select>
		</div>
		<div class="form-group"> 
			<label class="control-label">Localidade</label>
			<input type="text" class="form-control" name="localidade" id="localidade" value="" maxlength="50" placeholder="" />
		</div>
		<div class="form-group">
			<label class="control-label">Quantidade *</label>
			<input type="text" class="form-control" name="quantidade" id="quantidade" value="" maxlength="50" placeholder="" />
		</div>
		<div class="form-group"> 
			<input type="hidden" name="evento" id="evento" value="<?= !empty($_GET["name"])? $_GET["name"] : "ala" ?>" />
		</div>
		<div class="submit">
			<span class="short_explanation">* Dados Obrigatórios</span>
			<button class="btn btn-default" type="submit" name="Enviar">ENVIAR <i class="material-icons">chevron_right</i></button>
		</div>
	</form>
	<script src="<?=base_url('js/jquery.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('js/jquery.validate.js')?>" type="text/javascript"></script>
	<script src="<?=base_url('assets/js/main.js?'.date("YmdHis"))?>" type="text/javascript"></script>
</body>