<div <?php if(!$evento->active) { ?>style="display:none"<?php }?>>
<div class="wrapper">
	<div class="event-title">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h2><?=$evento->nome?></h2>
				</div>
				<div class="col-sm-4 col-right">
					<img src="<?=base_url('assets/images/event-title.svg')?>">
					... e tu vais connosco ao melhor preço!
				</div>
			</div>
		</div>
	</div>
	<div class="event-content">
		<div class="container">
			<div class="row events-mosaic">
			<?php foreach($eventos as $subevento): ?>
			<div class="col-xs-12 col-md-6 col-lg-4">
				<div class="event" data-event="<?= $subevento->id;?>">

					<a class="image" style="background-image:url('<?= base_url('media/events/'.$subevento->id.'/ticket/'.$subevento->image_file_name) ?>')" href="<?=site_url('event/'.$subevento->id);?>"></a>
					
					<?//=img('media/events/'.$subevento->id.'/ticket/'.$subevento->image_file_name,array("alt" => $subevento->nome))?>
					
					<a class="price" href="<?=site_url('event/'.$subevento->id);?>">
						<i class="material-icons">swap_horiz</i>
						<h4><?=$subevento->valor_base?> €</h4>
						<p>Ida e volta</p>
					</a>
					<div class="title">
						<?=$subevento->nome?>
					</div>
					<div class="info text-uppercase">
						<div class="row">
							<div class="col-xs-12">
								<i class="material-icons">today</i>
									<?=strftime("%d %h",strtotime($subevento->data_inicio));?>
									<?php
									if(date('Y-m-d',strtotime($subevento->data_inicio)) != date('Y-m-d',strtotime($subevento->data_fim))) {
										print '&nbsp-&nbsp';
										print date('d',strtotime($subevento->data_fim)) . ' ' . strftime('%h',strtotime($subevento->data_fim));
									}?>

								<a href="<?=site_url('event/'.$subevento->id);?>" class="btn btn-default pull-right">VER <i class="material-icons">chevron_right</i></a>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<i class="material-icons">directions_bus</i>
								<div class="places">
								<?php
									$followDepartures = json_decode(file_get_contents(site_url('event/'.$subevento->id) .'?blek_cidades'),true);
									
									//cidade_base
							
									foreach($followDepartures as $key=>$departure){
										if($departure["nome"]==$subevento->cidade_base){
												$class='class="active"';
										}else{
											$class="";
										}
										
										echo "<span ".$class." value='".$departure["id"]."'>".$departure["nome"]."</span>";
										if(count($followDepartures)>$key+1){
												echo ', ';
										}
										
									}
									
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
</div>


