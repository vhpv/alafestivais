<? if(isset($_GET["blek_cidades"])){
	//print_r($cidades);
	echo json_encode($cidades);
	exit();
} ?>
<!-- Info evento -->
<div <?php if(!$evento->active) { ?>style="display:none"<?php }?>>
<div class="wrapper">
	<div class="event-title">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h2><?=$evento->nome?></h2>
				</div>
				<div class="col-sm-4 col-right">
					<img src="<?=base_url('assets/images/event-title.svg')?>">
					... e tu vais connosco ao melhor preço!
				</div>
			</div>
		</div>
	</div>
	<div class="event-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-sm-5">
					<div class="data">
						<i class="material-icons">today</i>
				
						<?=strftime('%h',strtotime($evento->data_inicio))?>
						<?=strftime("%d",strtotime($evento->data_inicio));
						if(date('Y-m-d',strtotime($evento->data_inicio)) != date('Y-m-d',strtotime($evento->data_fim))) {
							print ' a ';
							print date('d',strtotime($evento->data_fim));
						}?>
					</div>
					
					<div class="departures-list open">
						<div class="title">Local de partida <i class="material-icons">keyboard_arrow_down</i></div>
						<ul class="group">
							<?php foreach($cidades as $cidade) {
								?>
								<li value="<?=$cidade->id?>">
									<span class="hora_ida"></span>
									<span class="nome"><?=$cidade->nome?></span>
									<span class="detalhes"></span>
								</li>
							<?php } ?>
						</ul>
					</div>
					<br>
					<div class="local">
						<i class="material-icons">location_on</i>
						<?=$evento->address_arrive;?>
					</div>
					<br>
					<a href="#" class="btn btn-default reservar">Garante já o
					teu lugar <i class="material-icons">check</i></a>
					<br>
					<p class="district-not-found jq_district_not_found">
						<a href="#">Não encontras o teu distrito?</a>
					</p>
					
				</div>
				<div class="col-lg-8 col-sm-7">
					<div class="event-image" style="background-image:url(<?=base_url('media/events/'.$evento->id.'/detail/'.$evento->detail_image);?>)"></div>
					<img class="event-form-top" src="<?=base_url('assets/images/event-form.svg')?>">
					<div class="event-form">
						<form id="detalhe" action="<?=current_url()."?success";?>" method="post" enctype="multipart/form-data">
						<div id="com-autocarros">

							<?php if(isset($_GET["success"])){?>
								<div class="alert alert-success">
									<h4>Obrigado pela tua reserva!</h4>
									<p>Acabámos de te enviar um e-mail com os dados de pagamento.</p>
									<p>Se não receberes, por favor verifica na pasta de "spam" ou contacta-nos directamente:</p>
									<p><a href="tel:910 405 582">910 405 582</a> ou <a href="mailto:alafestivais@alaviagens.com">alafestivais@alaviagens.com</a></p>
								</div>
							<?php } ?>


							<h3>Os teus dados</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Primeiro nome</label>
										<input type="text" size="30" class="form-control" name="first_name" id="reservation_first_name" value="<?=set_value('first_name')?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Último nome</label>
										<input type="text" size="30" class="form-control" name="last_name" id="reservation_last_name" value="<?=set_value('last_name')?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">E-mail</label>
										<input type="text" size="30" class="form-control" name="email" id="reservation_email" value="<?=set_value('email')?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Telefone</label>
										<input type="tel" size="30" class="form-control" name="phone" id="reservation_phone" value="<?=set_value('phone')?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Data de nascimento</label>
										<input type="date" size="30" class="form-control" name="birth_date" id="birth_date" placeholder="dd-mm-aaaa">
									</div>
								</div>
							</div>
							
							<hr>
							
							<h3>Dados para facturação</h3>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Nome</label>
										<input type="text" size="30" class="form-control" id="facturacao_nome" name="facturacao_nome" value="<?=set_value('facturacao_nome')?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">NIF</label>
										<input type="tel" size="30" class="form-control" id="facturacao_nif" name="facturacao_nif" maxlength="9" value="<?=set_value('facturacao_nif')?>">
									</div>
								</div>
							</div>
							
							<hr>
							
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Nº de pessoas</label>
										<?php if($lugares_livres == 0) { ?>
										(aqui fica a sondagem).
										<?php } else { ?>
										<select name="quantity" id="reservation_quantity" class="form-control valid">
											<?php for($i=1;$i<=($lugares_livres>15?15:$lugares_livres);$i++){ ?>
												<option value="<?=$i?>"><?=$i?></option>
											<?php } ?>
										</select>
										<?php } ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Local de partida</label>
										<select id="reservation_place_id" class="form-control valid" name="place_id">
											<?php foreach($cidades as $cidade) {?>
												<option value="<?=$cidade->id?>"><?=$cidade->nome?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label">Tipo de viagem</label>
											<?php
											if($lugares_livres > 0) { ?>
												<select name="trip" id="reservation_trip" class="form-control valid">
													<option value="Ida e volta">Ida e volta</option>
													<option value="Apenas ida">Apenas ida</option>
													<option value="Apenas volta">Apenas volta</option>
												</select>
											<?php } ?>
									</div>
								</div>
							</div>
							<br>
							<div class="row" id="partidas-precos">
								<div class="col-xs-6">
									<div class="row" id="partida">
										<div class="col-sm-6"><label>Hora de partida</label></div>
										<div class="col-sm-6" id="target_partida"></div>
									</div>
									<div class="row" id="regresso">
										<div class="col-sm-6"><label>Hora de regresso</label></div>
										<div class="col-sm-6" id="target_volta"></div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<div class="col-sm-6"><label>Preço por pessoa</label></div>
										<div class="col-sm-6" id="final_price_pp"></div>
									</div>
									<div class="row">
										<div class="col-sm-6"><label>Preço total</label></div>
										<div class="col-sm-6" id="final_price_total"></div>
									</div>
								</div>
							</div>
						
							<div class="submit">
								
								<div class="field"><input id="reservation_event_id" name="event_id" type="hidden" value="<?=$evento->id?>" /></div>
								<label>Li e aceito os <a class="jq_termos_e_condicoes" href="#">Termos e Condições</a>
									<input type="checkbox" name="checkbox_termos" value="termoscond" id="termos" class="check" name="termoscond" required="required" checked="checked">
								</label>
								<img class="event-submit" src="<?=base_url('assets/images/event-submit.svg')?>">
								<button type="submit" class="btn btn-default">Reservar <i class="material-icons">chevron_right</i></button>
								<?php echo validation_errors('<div class="callout callout-danger">','</div>'); ?>
							</div>
						</div>
						
						<div id="sem-autocarros" class="text-center">
							<br>
							<p>Não há autocarros disponíveis para este destino</p>
							<br>
						</div>
						
					</form>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php // print $lugares_livres; ?>					
	<!--<div id="backright_1" class="fr oh"  <?php if($evento->onsale == true){?> style="display:none"<?php }?>>
	<div id="ticket_descricao_1">
	<div class="ticket_h4">Confirmação da tua reserva</div>
	<div class="ticket_h5">Prazo de pagamento após<br>confirmação: <b><?=$limite_pagamento?></b> horas. </div>
	<div id="widthh2"class="ticket_h2"><?=$evento->nome?></div>
	<div class="ticket_h4"><?=date('d - m - Y',strtotime($evento->data_inicio))?></div>
	<div class="ticket_h4"><?=$this->cidades->get_by_id($evento->id_cidade)->nome?></div>
	<div class="field"><input name="voucher" id="voucher" placeholder="Código voucher" value="<?=set_value('voucher')?>" /></div>
	<div class=" ml4 reserv"> <input type="submit" value="" /> </div>
	<div class="field" id="termoscond">-->

							<!-- Form de pre reserva -->
							<div id="backleft_1" class="oh" <?php if($evento->onsale == false){?> style="display:none"<?php }?>>
								<div id="backleft_2">
									<div class="fl ml3">
										<div class="field">
											<label>Primeiro Nome:</label>
											<br />
											<?php /*?><%= f.text_field :first_name %> <?php */?></div>
										<div class="field">
											<label>Último nome:</label>
											<br />
											<?php /*?><%= f.text_field :last_name %> <?php */?></div>
										<div class="field">
											<label>E-mail:</label>
											<br />
											<?php /*?><%= f.text_field :email %><?php */?> </div>
										<div class="field">
											<label>Telefone:</label>
											<br />
											<?php /*?><%= f.text_field :phone %><?php */?> </div>
									</div>
									<div class="ml3 backleft_right">
										<div class="field select_fixed">
											<label>Quantidade:</label>
											<br />
											<?php /*?><%= f.select :quantity, options_for_select((1..9).step(1).to_a.map{|s| ["#{s}", s]})%> <?php */?></div>
										<div class="field select_fixed">
											<label>Cidade de Partida:</label>
											<br />
											<?php /*?><%= f.select :place_id, @destinations.collect { |l| [l.name, l.id] } %> <?php */?></div>
										<div class="field">
											<label>Observações:</label>
											<br />
											<p>Esta pré-reserva é efectuada sem compromisso.</p>
											<p>Assim que o evento for confirmado,<br>
												trataremos de te contactar.</p>
										</div>
									</div>
								</div>
							</div>
							<div id="backright_1" class="fr oh" <?php if($evento->onsale == false){ ?> style="display:none"<?php } ?>>
								<div id="ticket_descricao_1">
									<div class="ticket_h4">Confirmação do teu interesse</div>
									<div class="ticket_h5">Preenche os dados corretamente se realizarmos a viagem serás contactado.</div>
									<div class="ticket_h2"><?php $evento->nome?></div>
									<div class="ticket_h4"><?php $evento->data_inicio?></div>
									<div class="ticket_h4"><?=$this->cidades->get_by_id($evento->id_cidade)?></div>
									<div class="ticket_h3">&nbsp </div>
									<div class="field"><p class="price_field price">Preço total:</p></div>
								</div>
							</div>

					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="layer" id="district-not-found">
	<i class="material-icons layer-close">close</i>
	<div class="layer-inner">
		<h2>Sugestão de local de partida para evento</h2>
		<iframe src="<?php echo site_url('eventos/formulario_distrito') . "?name=" . $evento->nome ?>" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
	</div>
</div>