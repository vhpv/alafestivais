<? if(isset($_GET["blek_eventos"])){

	//print_r($cidades);

	echo json_encode($eventos);
	
	exit();
	
} ?>

<?php 
	$counter_eventos=0;
	
	//array_multisort($eventos, $datas);
	
	foreach($eventos as $i=>$undefined){
		$eventos_php[]=json_decode(json_encode($eventos[$i]),true);
	}
	
	$eventos = msort($eventos_php, array('data_inicio'));
?>

<div class="events-list">
	<div class="title">Eventos <i class="material-icons">keyboard_arrow_down</i></div>
	<div class="group">
	<?php foreach($eventos as $evento){ ?>
		<a href="<?=site_url('event/'.$evento["id"]);?>"><?=$evento["nome"]?></a>
	<?php } ?>
	</div>
</div>

<div id="slidecont">
	<div id="slides">
		<?php foreach($slides as $slide): ?>
			<div style="height:100%">
				<?=img('media/events/' . $slide->id.'/slide/'.$slide->slidefoto_file_name);?>
				<div class="filter"></div>
				<div class="inner-wrapper">
					<div class="titulo_t oh">
						<h1><a href="<?=site_url('event/'.$slide->id);?>"><?=$slide->nome?></a></h1>
					</div>
					<div class="data">
						<i class="material-icons">today</i>
						<?php
						print date('d',strtotime($slide->data_inicio)) . ' ' . strftime('%h',strtotime($slide->data_inicio));
						if(date('Y-m-d',strtotime($slide->data_inicio)) != date('Y-m-d',strtotime($slide->data_fim))) {
							echo '&nbsp-&nbsp';
							print date('d',strtotime($slide->data_fim)) . ' ' . strftime('%h',strtotime($slide->data_fim));
							
						} ?>
					</div>
					<div class="call">
						<a href="<?=site_url('event/'.$slide->id);?>" class="btn btn-default">Reservar <i class="material-icons">check</i></a>
					</div>
				</div>
			</div>
		
		<?php endforeach; ?>
	</div>
</div>

<div class="container">

	<h1 class="site-title text-center"><strong>ALA FESTIVAIS :</strong> transporte ao melhor preço, direto ao recinto</h1>
	
	
	<div class="row events-mosaic">
		<?php foreach($eventos as $evento): $counter_eventos++; ?>
		<?php //echo "<pre>"; print_r($evento); echo "</pre>"; ?>
			<?php
				if($counter_eventos <= 7){
					$batchClass = "first-batch";
				}else{
					$batchClass="last-batch";
				}
			?>
			<?php if($evento["evento_pai"] && $evento["eventos_filhos"] == 0){} else { ?>
				<div class="col-xs-12 col-sm-6 col-lg-4 <?=$batchClass; ?>">
					<div class="event" data-event="<?= $evento["id"];?>">
					<?php if($evento["evento_pai"] == 1) :?>
						<a href="<?=site_url('event/'.$evento["id"]);?>" class="image" style="background-image:url(<?php echo base_url('media/events/'.$evento["id"].'/ticket/'.$evento["image_file_name"]); ?>)"></a>
					<?php else : ?>
						<a href="<?=site_url('event/'.$evento["id"]);?>" class="image" style="background-image:url(<?php echo base_url('media/events/'.$evento["id"].'/ticket/'.$evento["image_file_name"]); ?>)"></a>
					<?php endif; ?>
						<a class="price" href="<?=site_url('event/'.$evento["id"]);?>">
							<i class="material-icons">swap_horiz</i>
							<h4><?=$evento["valor_base"]?> €</h4>
							<p>Ida e volta</p>
						</a>
						<div class="title">
							<?=$evento["nome"]?>
						</div>
						<div class="info text-uppercase">
							<div class="row">
								<div class="col-xs-12">
									<i class="material-icons">today</i>
									<?=strftime("%d %h",strtotime($evento["data_inicio"]));?>
									<?php
									if(date('Y-m-d',strtotime($evento["data_inicio"])) != date('Y-m-d',strtotime($evento["data_fim"]))) {
										print '&nbsp-&nbsp';
										print date('d',strtotime($evento["data_fim"])) . ' ' . strftime('%h',strtotime($evento["data_fim"]));
									}?>

									<i class="material-icons">location_on</i>
									<?=$evento["address_arrive"]?>
									<a href="<?=site_url('event/'.$evento["id"]);?>" class="btn btn-default pull-right">VER <i class="material-icons">chevron_right</i></a>
								</div>
							</div>
							<?php if($evento["evento_pai"] == 0) :?>
							<div class="row">
								<div class="col-xs-12">
									<i class="material-icons">directions_bus</i>
									<div class="places">
									<?php
									
									$followDepartures = json_decode(file_get_contents(site_url('event/'.$evento["id"]) .'?blek_cidades'),true);
									
									//cidade_base
							
									foreach($followDepartures as $key=>$departure){
										if($departure["nome"]==$evento["cidade_base"]){
												$class='class="active"';
										}else{
											$class="";
										}
										
										echo "<span ".$class." value='".$departure["id"]."'>".$departure["nome"]."</span>";
										if(count($followDepartures)>$key+1){
												echo ', ';
										}
										
									}
									
									?>
									</div>
								</div>
							</div>
							<?php else: ?>
							<div class="parent-spacer"></div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php } 
			//if($counter_eventos==7) break;
			?>
		<?php endforeach; ?>
	</div>
	<div class="read-more">
		<button class="jq_events_read_more btn btn-default">Ver <span class="more">mais</span><span class="less">menos</span> <i class="material-icons">keyboard_arrow_down</i></button>
	</div>
	
</div>
<script>
$(function() {

	$('#slides').slidesjs({
		width: 940,
		height: 455,
		play: {
			active: true,
			auto: true,
			interval: 4000,
			swap: true
		}
	});
});

localStorage.setItem("eventos",JSON.stringify(<?php echo json_encode($eventos); ?>));

	
</script>


<?

function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    // @TODO This should be fixed, now it will be sorted as string
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_STRING;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

?>