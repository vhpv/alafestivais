<!DOCTYPE html>
<html class="bg-black">
<head>
<meta charset="UTF-8">
<title>Painel de Administração | Ala</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link href="<?=base_url('css/admin/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<?php $this->session->set_userdata('previous_page', uri_string());?>
</head>
<body class="bg-black">
	<div class="form-box" id="login-box">
		<div class="header">Painel de Administração</div>
		<form action="<?=site_url('admin/home/validar_login')?>" method="post" id="loginForm" >
			<div class="body bg-gray">
				<div class="form-group">
					<input type="text" name="email" class="form-control" placeholder="E-mail"/>
				</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Password"/>
				</div>
			</div>
			<div class="footer">
				<button type="submit" class="btn bg-olive btn-block">Entrar</button>
			</div>
		</form>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="<?=base_url('js/admin/bootstrap.min.js')?>" type="text/javascript"></script>
</body>
</html>