<aside class="right-side"> 
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/autocarros')?>">Autocarros</a></li>
			<li class="active">Distribuir</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-4">
				<div class="box box-widget">
					<div class="box-header with-border">
						<h3 class="box-title">Total de reservas (<?=$total_reservations->quantity?>)</h3>
					</div>
					<div class="box-body">
						<table class="table table-responsive table-condensed">
							<tr>
								<td>Cidade</td>
								<td>Ida e volta</td>
								<td>Ida</td>
								<td>Volta</td>
							</tr>
							<?php foreach($this->cidades->get_by_evento($evento->id) as $cidade){
								print '<tr>';
								print '<td>'.$cidade->nome.'</td>';
								print '<td>'.$this->reservas->get_by_event_place($evento->id,$cidade->id,'Ida e volta',true).'</td>';
								print '<td>'.$this->reservas->get_by_event_place($evento->id,$cidade->id,'Apenas ida',true).'</td>';
								print '<td>'.$this->reservas->get_by_event_place($evento->id,$cidade->id,'Apenas volta',true).'</td>';
								print '</tr>';
							} ?>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="box box-widget">
					<div class="box-header with-border">
						<h3 class="box-title">Total para distribuir</h3>
					</div>
					<div class="box-body">
						<table class="table table-responsive table-condensed">
							<tr>
								<td>Cidade</td>
								<td>Ida</td>
								<td>Volta</td>
							</tr>
							<?php foreach($this->cidades->get_by_evento($evento->id) as $cidade){
								$pessoas_ir = (int)$this->reservas->get_by_event_place($evento->id,$cidade->id,'Todos ida',true);
								$pessoas_voltar = (int)$this->reservas->get_by_event_place($evento->id,$cidade->id,'Todos volta',true);
								print '<tr>';
									print '<td>'.$cidade->nome.'</td>';
									print '<td>'. $pessoas_ir . '</td>';
									print '<td>'. $pessoas_voltar .'</td>';
								print '</tr>';
							} ?>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="box box-widget">
					<div class="box-header with-border">
						<h3 class="box-title">Reservas por atribuir</h3>
					</div>
					<div class="box-body">
						<table class="table table-responsive table-condensed">
							<tr>
								<td>Nome</td>
								<td>Quantidade</td>
							</tr>
							<?php foreach($reservations_left as $reservation_left){
								print '<tr>';
									print '<td>'.$reservation_left->first_name.' ' . $reservation_left->last_name.'</td>';
									print '<td>'. $reservation_left->quantity . '</td>';
								print '</tr>';
							} ?>
						</table>
					</div>
				</div>
			</div>
			<?php foreach($available_bus as $autocarro){ ?>
				<div class="col-xs-12" id="bus<?=$autocarro->id?>">
					<div class="box box-primary">
						<div class="box-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="box-header">
										<h3 class="box-title"><i class="fa fa-bus"></i> <?=$autocarro->nome?></h3>
									</div>
										<table class="table table-bordered table-condensed">
										<thead>
											<th>Cidade</th>
											<th>Ida</th>
											<th>Volta</th>
										</thead>
										<?php
											$i=0;
											$autocarro_cidades = $this->autocarros->get_cidades_by_autocarro($autocarro->id);
											$total_ida = 0;
											$total_volta = 0;
											foreach($autocarro_cidades as $autocarro_cidade):
												$todos_ida = $this->reservas->get_total_by_event_place_bus($evento->id,$autocarro_cidade->id_cidade,$autocarro->id,'Todos ida')->quantity;
												$total_ida +=$todos_ida;
												
												$todos_volta = $this->reservas->get_total_by_event_place_bus($evento->id,$autocarro_cidade->id_cidade,$autocarro->id,'Todos volta')->quantity;
												$total_volta += $todos_volta;
											?>
											<tr>
												<td><?=$this->cidades->get_by_id($autocarro_cidade->id_cidade)->nome?></td>
												<?php /*?><td><?=$this->reservas->get_by_event_place($evento->id,$autocarro_cidade->id_cidade,false,true)->quantity?></td><?php *//*?>
												<td><?=$this->reservas->get_total_by_event_place_bus($evento->id,$autocarro_cidade->id_cidade,$autocarro->id,'Ida e volta')->quantity?>
												<td><?=$this->reservas->get_total_by_event_place_bus($evento->id,$autocarro_cidade->id_cidade,$autocarro->id,'Apenas ida')->quantity?>
												<td><?=$this->reservas->get_total_by_event_place_bus($evento->id,$autocarro_cidade->id_cidade,$autocarro->id,'Apenas volta')->quantity*/?>
												<td><?=$todos_ida?></td>
												<td><?=$todos_volta?></td>
											</tr>
										<?php endforeach;?>
										
											<tr>
												<th>Total</th>
												<th><?=$total_ida?></th>
												<th><?=$total_volta?></th>
											</tr>
										
										<tfoot>
											<tr>
												<th>Lugares livres</th>
												<th><?=$autocarro->lugares-$total_ida?></th>
												<?php /*$this->reservas->get_total_by_bus($autocarro->id)->quantity */?>
												<th><?=$autocarro->lugares-$total_volta?></th>
											</tr>
										</tfoot>
										</table><br /><br />
										<strong>Lugares:</strong> <?=$autocarro->lugares?><br />
										<strong>Lugares livres:</strong> 
									
								</div>
								<div class="col-lg-8">
									<div class="box-header"> <i class="fa fa-shopping-cart"></i>
										<h3 class="box-title">Reservas</h3>
									</div>
									<div class="box-body">
										<table class="table table-hover table-condensed">
											<thead>
												<tr>
													<th>#</th>
													<th>Nome</th>
													<th>Cidade</th>
													<th>Tipo</th>
													<th>Quantidade</th>
													<th>Estado</th>
													<th>Pago</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($this->reservas->get_by_bus($autocarro->id) as $reserva): ?>
												<tr>
													<td><?=$reserva->id?></td>
													<td><?=$reserva->first_name?> <?=$reserva->last_name?></td>
													<td><?=$this->cidades->get_by_id($reserva->place_id)->nome?></td>
													<td><?php if(strtolower($reserva->trip)=='ida e volta') {
														echo 'IV';
													} elseif(strtolower($reserva->trip)=='apenas ida') {
														echo 'I';
													} else {
														echo 'V';
													}?></td>
													<td><?=$reserva->quantity?></td>
													<td><?=estado($reserva->status)?></td>
													<td><?=estado($reserva->paid)?></td>
												</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<a href="<?=site_url('admin/autocarros/gerar/'.$autocarro->id)?>" class="btn btn-success<?=($autocarro->bloqueado)?' disabled':''?>">Gerar autocarro</a> <a href="<?=site_url('admin/autocarros/bloquear/'.$autocarro->id)?>" class="btn<?=($autocarro->bloqueado)?' btn-warning':' btn-info'?>"><?=($autocarro->bloqueado)?'<i class="fa fa-lock"></i> Desbloquear':'<i class="fa fa-unlock"></i> Bloquear'?></a><?php if($autocarro->bloqueado){?> <a href="<?=site_url('admin/autocarros/imprimir/'.$autocarro->id)?>" class="btn btn-default"><i class="fa fa-file-text-o"></i> Imprimir</a><?php } ?>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</section>
</aside>