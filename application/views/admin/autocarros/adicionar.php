<aside class="right-side">
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/cidades')?>">Autocarros</a></li>
			<li class="active">Adicionar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<form role="form" method="post" action="<?=current_url()?>">
				<div class="col-xs-12 col-lg-4">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Detalhes autocarro</h3>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-xs-12"><?php echo validation_errors('<div class="callout callout-danger">','</div>'); ?></div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<div class="form-group">
										<label for="nome">Nome</label>
										<input type="text" placeholder="Introduza o nome do autocarro" name="nome" id="nome" class="form-control" value="<?=set_value('nome');?>">
									</div>
								</div>
								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label for="lugares">Lugares</label>
										<input type="text" placeholder="0" name="lugares" id="lugares" class="form-control" value="<?=set_value('lugares');?>">
									</div>
								</div>
								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label for="bloqueado">Bloqueado</label>
										<select name="bloqueado" id="bloqueado" class="form-control">
											<option value="1" <?php echo set_select('bloqueado', '1'); ?>>Sim</option>
											<option value="0" <?php echo set_select('bloqueado', '0', TRUE); ?>>Não</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label for="evento">Evento</label>
										<select name="evento" id="evento" class="form-control">
											<?php foreach($eventos as $evento):
												$eventos_filhos = $this->eventos->get_by_id_pai($evento->id);
												if(count($eventos_filhos)){?>
													<optgroup label="<?=$evento->nome?>">
														<?php foreach($eventos_filhos as $evento_filho) { ?>
															<option value="<?=$evento_filho->id?>" <?=set_select('evento',$evento_filho->id)?>><?=$evento_filho->nome?></option>
														<?php } ?>
													</optgroup>
												<?php } else { ?>
													<option value="<?=$evento->id?>" <?php echo set_select('evento', $evento->id); ?>><?=$evento->nome?></option>
												<?php } ?>
											<?php endforeach;?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary" type="submit">Guardar</button>
						</div>
					</div>
				</div>
			</form>
			<div class="cidade-escondida hidden">
				<li>
					<span class="handle">
						<i class="fa fa-ellipsis-v"></i>
						<i class="fa fa-ellipsis-v"></i>
					</span>
					<div class="col-xs-8">
						<select name="cidade[]" class="cidades form-control" data-identifier="1">
							<option value="">-- escolher--</option>
							<?php foreach($cidades as $cidade) { ?>
								<option value="<?=$cidade->id?>"><?=$cidade->nome?></option>
							<?php }?>
						</select>
					</div>
					<div class="tools">
						<i class="fa fa-trash-o removepontotrajecto"></i>
					</div>
				</li>
			</div>
		</div>
	</section>
</aside>