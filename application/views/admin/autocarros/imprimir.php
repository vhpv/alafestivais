<aside class="right-side"> 
	
	<section class="invoice">
		<h1>IDA</h1>
		<div class="row">
			<div class="col-xs-12 table-responsive">
				<?php foreach($cidades as $cidade=>$lugares){ ?>
					<h3><?=$this->cidades->get_by_id($cidade)->nome;?> (<?=$this->reservas->get_total_by_event_place_bus($autocarro->id_evento,$cidade,$autocarro->id,'Todos ida')->quantity?>)</h3>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>ID RESERVA</th>
								<th>Nome</th>
								<th>Telefone</th>
								<th>Lugares</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($this->reservas->get_by_bus_place($autocarro->id,$cidade,'Todas ida',false,'first_name') as $reserva): ?>
							<tr>
								<td><?=$reserva->id?></td>
								<td><?=$reserva->first_name?> <?=$reserva->last_name?></td>
								<td><?=$reserva->phone?></td>
								<td><?=$reserva->quantity?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php } ?>
			</div>
		</div>
	</section>
	<section class="invoice">
		<h1>VOLTA</h1>
		<div class="row">
			<div class="col-xs-12 table-responsive">
				<?php foreach($cidades as $cidade=>$lugares){ ?>
					<h3><?=$this->cidades->get_by_id($cidade)->nome;?> (<?=$this->reservas->get_total_by_event_place_bus($autocarro->id_evento,$cidade,$autocarro->id,'Todos volta')->quantity?>)</h3>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>ID RESERVA</th>
								<th>Nome</th>
								<th>Telefone</th>
								<th>Lugares</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($this->reservas->get_by_bus_place($autocarro->id,$cidade,'Todos volta',false,'first_name') as $reserva): ?>
							<tr>
								<td><?=$reserva->id?></td>
								<td><?=$reserva->first_name?> <?=$reserva->last_name?></td>
								<td><?=$reserva->phone?></td>
								<td><?=$reserva->quantity?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				<?php } ?>
			</div>
		</div>
	</section>
</aside>