<aside class="right-side"> 
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/autocarros')?>">Autocarros</a></li>
			<li class="active">Ver</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<form role="form" method="post" action="<?=current_url()?>">
				<div class="col-xs-12 col-lg-4">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Detalhes autocarro</h3>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-xs-12"><?php echo validation_errors('<div class="callout callout-danger">','</div>'); ?></div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<div class="form-group">
										<label for="nome">Nome</label><br />
										<?=$autocarro->nome?>
									</div>
								</div>
								<div class="col-xs-12 col-lg-6">
									<div class="form-group">
										<label for="lugares">Reservas / Lugares</label><br />
										<?=$this->reservas->get_total_by_bus($autocarro->id)->quantity?> / <?=$autocarro->lugares?>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label for="evento">Evento</label><br />
										<?=$this->eventos->get_by_id($autocarro->id_evento)->nome?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-8">
					<div class="box box-primary">
						<div class="box-header"> <i class="fa fa-location-arrow"></i>
							<h3 class="box-title">Trajecto</h3>
						</div>
						<div class="box-body">
							<table class="table table-bordered">
								<?php $i=0;foreach($autocarro_cidades as $autocarro_cidade):?>
									<tr>
										<td><?=++$i;?></td>
										<td><?=$this->cidades->get_by_id($autocarro_cidade->id_cidade)->nome?></td>
										<td><?php
										$precos_por_cidade = $this->eventos->get_detalhes_by_cidade_evento($autocarro->id_evento,$autocarro_cidade->id_cidade);
										print $precos_por_cidade[0]->valor;
										?></td>
									</tr>
								<?php endforeach;?>
								</table>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="box box-primary">
						<div class="box-header"> <i class="fa fa-shopping-cart"></i>
							<h3 class="box-title">Reservas</h3>
						</div>
						<div class="box-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Nome</th>
										<th>Data</th>
										<th>Preço</th>
										<th>Cidade</th>
										<th>Tipo</th>
										<th>Quantidade</th>
										<th>Estado</th>
										<th>Pago</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($this->reservas->get_by_bus($autocarro->id) as $reserva): ?>
									<tr>
										<td><?=$reserva->id?></td>
										<td><?=$reserva->first_name?> <?=$reserva->last_name?></td>
										<td><?=$reserva->created_at?></td>
										<td>€ <?=moeda_format($reserva->price*$reserva->quantity)?></td>
										<td><?=$this->cidades->get_by_id($reserva->place_id)->nome?></td>
										<td><i class="fa fa-<?php if(strtolower($reserva->trip)=='ida e volta') {
											echo 'arrows-h';
										} elseif(strtolower($reserva->trip)=='apenas ida') {
											echo 'long-arrow-right';
										} else {
											echo 'long-arrow-left';
										}?>"></td>
										<td><?=$reserva->quantity?></td>
										<td><?=estado($reserva->status)?></td>
										<td><?=estado($reserva->paid)?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</aside>