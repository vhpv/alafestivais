<aside class="right-side"> 
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/autocarros')?>"><?=$title?></a></li>
			<li class="active">Listar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<form class="form-inline" method="get" action="<?=current_url()?>">
							<div class="form-group">
								<select onchange="this.form.submit()" class="input-sm form-control" name="event_id">
									<option value="">-- Filtrar --</option>
									<?php foreach($events as $event){?><option value="<?=$event->id?>" <?=set_select('event_id',$event->id,($this->input->get('event_id')==$event->id)?TRUE:FALSE)?>><?=$event->nome?></option><?php } ?>
								</select>
							</div>
							<a href="<?=site_url('admin/autocarros/adicionar')?>" class="btn btn-success btn-sm pull-right">Novo</a>
						</form>
					</div>
					
					<?=alerta()?>
					<?php if(count($autocarros)): ?>
						<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Id</th>
										<th>Evento</th>
										<th>Nome</th>
										<th>Lugares</th>
										<th>Reservas</th>
										<th class="text-center">Acções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($autocarros as $autocarro):?>
									<tr>
										<td><?=$autocarro->id?></td>
										<td><?=$autocarro->nome_evento?></td>
										<td><?=$autocarro->nome?></td>
										<td><?=$autocarro->lugares?></td>
										<td><?=$reservations_efective = $this->reservas->get_total_by_bus($autocarro->id)->quantity?> <?=($reservations_efective>$autocarro->lugares)?'<span class="label label-danger"><i class="fa fa-exclamation"></i>':''?></td>
										
										<td class="text-center">
											<a href="<?=site_url('admin/autocarros/ver/'.$autocarro->id);?>" class="btn btn-success"><i class="fa fa-eye"></i></a>
											<a href="<?=site_url('admin/autocarros/editar/'.$autocarro->id);?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
											<a href="<?=site_url('admin/autocarros/apagar/'.$autocarro->id);?>" class="btn btn-danger apagar"><i class="fa fa-trash-o"></i></a>
											</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					<?php else: ?>
						<div class="box-body table-responsive">
							<div class="alert alert-info">
								<i class="fa fa-info"></i>
								<b>Atenção!</b> Não tem qualquer autocarro registado.
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</aside>