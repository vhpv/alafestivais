<aside class="right-side"> 
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/cidades')?>">Cidades</a></li>
			<li class="active">Editar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Editar</h3>
					</div>
					<form role="form" method="post" action="<?=current_url()?>">
						<div class="box-body">
							<div class="row">
								<div class="col-xs-12"><?php echo validation_errors('<div class="callout callout-danger">','</div>'); ?></div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="nome">Nome</label>
										<input type="text" placeholder="Introduza o nome da cidade" name="nome" id="nome" class="form-control" value="<?=set_value('nome',$cidade->nome);?>">
									</div>
								</div>
							</div>
							<?php /*?><div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="activo">Activo</label>
										<select name="activo" id="activo" class="form-control">
											<option value="1" <?php echo set_select('activo', 1, ($categoria->activo)?true:false); ?>>Sim</option>
											<option value="0" <?php echo set_select('activo', 0, (!$categoria->activo)?true:false); ?>>Não</option>
										</select>
									</div>
								</div>
							</div><?php */?>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary" type="submit">Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</aside>