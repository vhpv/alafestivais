<aside class="right-side"> 
	<section class="content-header">
		<h1>Reservas</h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/reservas')?>">Reservas</a></li>
			<li class="active">Editar</li>
		</ol>
	</section>
	<?php if($autocarro) {?>
		<?php if($autocarro->bloqueado) {?>
		<section class="content">
			<div class="alert alert-danger">
				<i class="fa fa-exclamation"></i> Não pode alterar esta reserva, o autocarro encontra-se bloqueado
			</div>
		</section>
		<?php } ?>
	<?php } ?>
	<section class="content invoice">
		<form role="form" method="post" action="<?=current_url()?>">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page-header"> <small class="text-right">Data: <?=$reservation->created_at?></small></h2>
			</div>
		</div>
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<address>
					<strong><?=$reservation->first_name?> <?=$reservation->last_name?></strong><br>
					Telefone: <?=$reservation->phone?><br/>
					E-mail: <?=$reservation->email?>
					<?php if($reservation->facturacao_nome){
						print '<br><br><strong>Dados facturação</strong>' ?>
					<?=($reservation->facturacao_nome)?'<br>Nome: '.$reservation->facturacao_nome:'';?>
					<?=($reservation->facturacao_nif)?'<br>NIF: '.$reservation->facturacao_nif:'';?>
					<?php } ?>
				</address>
			</div>
			<div class="col-sm-4 invoice-col">
				<table class="table">
					<tr><td><b>Evento:</b></td><td><?=$evento->nome?></td></tr>
					<tr><td><b>Viagem:</b></td><td><?=$reservation->trip?></td></tr>
					<tr><td><b>Origem:</b></td><td><?=$cidade->nome?></td></tr>
					<tr>
						<td><b>Autocarro:</b></td>
						<td><?php
							if($autocarro && $autocarro->bloqueado) {
								print $autocarro->nome;
							} else { ?>
							<select name="bus_id" class="form-control">
								<option value="0">-- seleccione --</option>
								<?php foreach($available_bus as $bus){ ?>
									<option value="<?=$bus->id?>" <?=(isset($autocarro->id) && $autocarro->id==$bus->id)?'selected="selected"':''?> <?=($bus->bloqueado)?'disabled':''?>><?=$bus->nome?></option>
								<?php } ?>
								
							</select><?php } ?>
						</td>
					</tr>
					<tr><td><b>Quantidade:</b></td><td><?=$reservation->quantity?></td></tr>
				</table>
			</div>
		</div>
		<hr />
		<div class="row"> 
			<div class="col-xs-12 col-sm-6">
				<p class="lead">Multibanco</p>
				<div class="table-responsive">
					<table class="">
						<tr>
							<th style="width:50%">Entidade:</th>
							<td><?=$reservation->mb_ent?></td>
						</tr>
						<tr>
							<th>Referência</th>
							<td><?=$reservation->mb_ref?></td>
						</tr>
						<tr>
							<th>Montante:</th>
							<td>€ <?=moeda_format($reservation->price*$reservation->quantity)?></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<p class="lead">Totais</p>
				<div class="table-responsive">
					<table class="table">
						<tr>
							<th style="width:50%">Sub-total:</th>
							<td>€<?=moeda_format($reservation->price*$reservation->quantity/1.23)?></td>
						</tr>
						<tr>
							<th>IVA (23%)</th>
							<td>€<?=moeda_format($reservation->price*$reservation->quantity-$reservation->price*$reservation->quantity/1.23)?></td>
						</tr>
						<tr>
							<th>Total:</th>
							<td>€<?=moeda_format($reservation->price*$reservation->quantity)?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row no-print">
			<div class="col-sm-3">
				<div class="input-group">
					<div class="input-group-addon">Pagamento</div>
					<select id="paid" name="paid" class="form-control">
						<option value="0" <?=$reservation->status=='0'?' selected="selected"':''?>>Não pago</option>
						<option value="1" <?=$reservation->status=='1'?' selected="selected"':''?>>Pago</option>
					</select>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="input-group">
					<div class="input-group-addon">Estado</div>
					<select id="status" name="status" class="form-control">
						<option value="0" <?=$reservation->status=='0'?' selected="selected"':''?>>Não confirmado</option>
						<option value="1" <?=$reservation->status=='1'?' selected="selected"':''?>>Confirmado</option>
					</select>
				</div>
			</div>
			<button class="btn btn-lg btn-success col-sm-offset-2 col-sm-2" type="submit"><i class="fa fa-edit"></i> Gravar</button>
			</div>
		</form>
	</section>
</aside>
