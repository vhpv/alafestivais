<aside class="right-side"> 
	<section class="content-header">
		<h1> Reservas <small>listar reservas</small> </h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/reservas')?>">Reservas</a></li>
			<li class="active">Listar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Reservas</h3>
					</div>
					<?=alerta()?>
					<?php if(count($reservas)): ?>
						<div class="box-body table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Nome</th>
										<th>Local</th>
										<th>Evento</th>
										<th>Autocarro</th>
										<th>Data</th>
										<th>Preço</th>
										<th>Tipo</th>
										<th>Estado</th>
										<th>Pago</th>
										<th class="text-center">Acções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($reservas as $reserva): ?>
									<tr>
										<td><?=$reserva->id?></td>
										<td><?=$reserva->first_name?> <?=$reserva->last_name?></td>
										<td><?=$this->cidades->get_by_id($reserva->place_id)->nome?></td>
										<td><?=isset($this->eventos->get_by_id($reserva->event_id)->nome)?$this->eventos->get_by_id($reserva->event_id)->nome:''?></td>
										<td><?=isset($this->autocarros->get_by_id($reserva->bus_id)->nome)?$this->autocarros->get_by_id($reserva->bus_id)->nome:'<span class="label label-danger">Sem autocarro</span>'?></td>
										<td><?=$reserva->created_at?></td>
										<td>€ <?=moeda_format($reserva->price*$reserva->quantity)?></td>
										<td><i class="fa fa-<?php if(strtolower($reserva->trip)=='ida e volta') {
											echo 'exchange';
										} elseif(strtolower($reserva->trip)=='apenas ida') {
											echo 'long-arrow-right';
										} else {
											echo 'long-arrow-left';
										}?>"></td>
										<td><?=estado($reserva->status)?></td>
										<td><?=estado($reserva->paid)?></td>
										<td class="text-center"><a href="<?=site_url('admin/reservas/editar/'.$reserva->id);?>" class="btn btn-warning btn-sm"><small><i class="fa fa-pencil"></i></small></a></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					<?php else: ?>
						<div class="box-body table-responsive">
							<div class="alert alert-info">
								<i class="fa fa-info"></i>
								<b>Atenção!</b> Não tem qualquer reserva registada.
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</aside>