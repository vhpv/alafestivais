<aside class="right-side"> 
	<section class="content-header">
		<h1>Encomenda <small>#<?=$encomenda->id?></small> </h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Blank page</li>
		</ol>
	</section>
	<section class="content invoice"> 
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page-header"> <i class="fa fa-envelope-o"></i> <?=$encomenda->codigo?> <small class="pull-right">Data: <?=$encomenda->data_criacao?></small></h2>
			</div>
		</div>
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col"><address>
				<strong><?=$encomenda->nome?></strong><br>
				<?=nl2br($encomenda->morada)?><br>
				<?=($encomenda->facturacao_nif)?'NIF: '.$encomenda->facturacao_nif.'<br>':'';?>
				Telefone: <?=$encomenda->telefone?><br/>
				E-mail: <?=$encomenda->email?>
				</address>
			</div>
			<div class="col-sm-4 invoice-col"><b>Pagamento</b><br/>
				<br/><?php if($encomenda->pagamento_metodo == 'pp') { ?>
				<b>Pagamento por Paypal</b><br>
				<?php } elseif($encomenda->pagamento_metodo == 'mb') { ?>
				<b>Entidade:</b> <?=$encomenda->mb_entidade?><br/>
				<b>Referência:</b> <?=$encomenda->mb_referencia?><br/>
				<b>Montante:</b> €<?=moeda_format($encomenda->total)?><br/>
				<?php } ?>
				<b>Estado:</b> <?php if($encomenda->estado == 'pendente') { ?>
					<span class="label label-warning">Aguarda pagamento</span>
					<?php } elseif($encomenda->estado == 'finalizado') { ?>
					<span class="label label-success">Finalizado</span>
					<?php } elseif($encomenda->estado == 'cancelado') { ?>
					<span class="label label-danger">Cancelado</span>
				<?php }?>
				
				</div>
		</div>
		<div class="row">
			<div class="col-xs-12 table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Id</th>
							<th>Produto</th>
							<th>Qtd</th>
							<th>Subtotal</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($encomenda_detalhes as $detalhe): ?>
						<tr>
							<td><?=$detalhe->id_produto?></td>
							<td><?=$detalhe->nome?></td>
							<td><?=$detalhe->quantidade?></td>
							<td>€<?=moeda_format($detalhe->subtotal)?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row"> 
			<div class="col-xs-12 col-sm-6 col-sm-offset-6">
				<p class="lead">Totais</p>
				<div class="table-responsive">
					<table class="table">
						<tr>
							<th style="width:50%">Sub-total:</th>
							<td>€<?=moeda_format($encomenda->pagamento_valor/1.23)?></td>
						</tr>
						<tr>
							<th>IVA (23%)</th>
							<td>€<?=moeda_format($encomenda->pagamento_valor-$encomenda->pagamento_valor/1.23)?></td>
						</tr>
						<tr>
							<th>Portes:</th>
							<td>€<?=moeda_format($encomenda->envio_valor)?></td>
						</tr>
						<tr>
							<th>Total:</th>
							<td>€<?=moeda_format($encomenda->total)?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row no-print">
			<div class="col-xs-12">
				<button class="btn btn-default" onClick="window.print();"><i class="fa fa-print"></i> Imprimir</button>
				<button class="btn btn-success pull-right"><i class="fa fa-edit"></i> Editar</button>
			</div>
		</div>
		<?php
		/*for($i = 1425680000; $i < 1429680000; $i++) {
			if(md5($i) == 'e9807045d50127af3331a8468bffff09'){
				die($i);
				
			}
		}*/
		?>
		<?=date('Y-m-d H:i:s',1425684770)?>
		
		<?=md5(1425683898)?>
		<?=mktime(0,0,0,3,10,2015)?>
	</section>
</aside>
