<aside class="right-side"> 
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> Clientes <small>adicionar clientes</small> </h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('clientes')?>">Clientes</a></li>
			<li class="active">Adicionar</li>
		</ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Ficha de cliente</h3>
					</div>
					<form role="form" method="post" action="<?=current_url()?>">
						<div class="box-body">
							<div class="row">
								<div class="col-xs-12"><?php echo validation_errors('<div class="callout callout-danger">','</div>'); ?></div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="nome">Nome</label>
										<input type="text" placeholder="Introduza o nome do cliente" name="nome" id="nome" class="form-control">
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="nome_contacto">Nome de Contacto</label>
										<input type="text" name="nome_contacto" id="nome_contacto" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="email">E-mail</label>
										<input type="email" placeholder="Endereço de e-mail" name="email" id="email" class="form-control">
										<p class="help-block">Apenas se souber.</p>
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="telefone">Telefone</label>
										<input type="text" name="telefone" id="telefone" class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary" type="submit">Guardar</button>
						</div>
					</form>
				</div>
				<!-- /.box --> 
			</div>
		</div>
	</section>
	<!-- /.content --> 
</aside>
<!-- /.right-side --> 