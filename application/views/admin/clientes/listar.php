<aside class="right-side"> 
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> Clientes <small>listar clientes</small> </h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('clientes')?>">Clientes</a></li>
			<li class="active">Listar</li>
		</ol>
	</section>
	
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Clientes</h3>
						<div class="box-tools">
							<a href="<?=site_url('clientes/adicionar')?>" class="btn btn-success btn-sm pull-right">Novo</a>
						</div>
					</div>
					<?php if(count($clientes)): ?>
						<div class="box-body table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Nome</th>
										<th>Contacto</th>
										<th>E-mail</th>
										<th>Telefone</th>
										<th class="text-center">Acções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($clientes as $cliente): ?>
									<tr>
										<td><?=$cliente->nome?></td>
										<td><?=$cliente->nome_contacto?></td>
										<td><?=$cliente->email?></td>
										<td><?=$cliente->telefone?></td>
										<td class="text-center"><a href="<?=site_url('clientes/editar/'.$cliente->id);?>" class="btn btn-warning btn-sm"><small><i class="fa fa-pencil"></i></small></a></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					<?php else: ?>
						<div class="box-body table-responsive">
							<div class="alert alert-info">
								<i class="fa fa-info"></i>
								<b>Atenção!</b> Não tem qualquer cliente registado.
							</div>
						</div>
					<?php endif; ?>
					<!-- /.box-body --> 
				</div>
				<!-- /.box -->
			</div>
		</div>
	</section>
	<!-- /.content --> 
</aside>
<!-- /.right-side --> 