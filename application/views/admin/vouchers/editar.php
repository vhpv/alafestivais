<aside class="right-side"> 
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/vouchers')?>">Vouchers</a></li>
			<li class="active">Editar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Editar</h3>
					</div>
					<form role="form" method="post" action="<?=current_url()?>">
						<div class="box-body">
							<div class="row">
								<div class="col-xs-12"><?php echo validation_errors('<div class="callout callout-danger">','</div>'); ?></div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="name">Nome</label>
										<input type="text" placeholder="Introduza o nome do voucher" name="name" id="name" class="form-control" value="<?=set_value('name',$voucher->name);?>">
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="code">Código</label>
										<input type="text" placeholder="Introduza o código voucher" name="code" id="code" class="form-control" value="<?=set_value('code',$voucher->code);?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="value">Valor</label>
										<div class="input-group">
											<input type="text" placeholder="0.00" name="value" id="value" class="form-control" value="<?=set_value('value',$voucher->value);?>">
											<span class="input-group-addon">
												<select name="type">
													<option value="percent" <?php echo set_select('type', 'percent',(bool)($voucher->type=='percent')); ?>>%</option>
													<option value="flat" <?php echo set_select('type', 'flat',(bool)($voucher->type=='flat')); ?>>€</option>
												</select>
											</span>
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label>Data de início:</label>
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="start_date" class="form-control pull-right datepicker" value="<?=set_value('data_inicio',$voucher->start_date);?>" />
										</div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<label>Data de fim:</label>
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="end_date" class="form-control pull-right datepicker" value="<?=set_value('data_fim',$voucher->end_date);?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-3 col-xs-12">
									<div class="form-group">
										<label for="active">Activo</label>
										<select name="active" id="active" class="form-control">
											<option value="1" <?php echo set_select('active', '1',(bool)($voucher->active)); ?>>Sim</option>
											<option value="0" <?php echo set_select('active', '0',(bool)($voucher->active==0)); ?>>Não</option>
										</select>
									</div>
								</div>
								<div class="col-lg-3 col-xs-12">
									<div class="form-group">
										<label for="times">Nº Utilizações disponíveis</label>
										<input class="form-control" type="text" name="times" value="<?=set_value('times',$voucher->times)?>" />
									</div>
								</div>
								<div class="col-lg-6 col-xs-12">
									<div class="form-group">
										<label for="event_id">Evento</label>
										<select name="event_id" id="event_id" class="form-control">
											<option value="0" <?php echo set_select('event_id', 0, (bool)($voucher->event_id==0)); ?>>-- Todos os eventos --</option>
											<?php foreach($events as $event){ ?>
												<option value="<?=$event->id?>" <?=set_select('event_id',$event->id,(bool)($voucher->event_id==$event->id));?>><?=$event->nome?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary" type="submit">Guardar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</aside>
