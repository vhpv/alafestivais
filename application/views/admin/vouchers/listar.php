<aside class="right-side"> 
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/vouchers')?>">Vouchers</a></li>
			<li class="active">Listar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Lista</h3>
						<div class="box-tools">
							<a href="<?=site_url('admin/vouchers/adicionar')?>" class="btn btn-success btn-sm pull-right">Novo</a>
						</div>
					</div>
					<?=alerta()?>
					<?php if(count($vouchers)): ?>
						<div class="box-body table-responsive">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Id</th>
										<th>Nome</th>
										<th>Código</th>
										<th>Valor</th>
										<th class="text-center">Acções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($vouchers as $voucher): ?>
									<tr>
										<td><?=$voucher->id?></td>
										<td><?=$voucher->name?></td>
										<td><?=$voucher->code?></td>
										<td><?=$voucher->value?><?=($voucher->type=='percent')?'%':'€'?></td>
										<td class="text-center"><a href="<?=site_url('admin/vouchers/editar/'.$voucher->id);?>" class="btn btn-warning"><small><i class="fa fa-pencil"></i></small></a> <a href="<?=site_url('admin/vouchers/apagar/'.$voucher->id);?>" class="btn btn-danger"><small><i class="fa fa-trash-o"></i></small></a></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					<?php else: ?>
						<div class="box-body table-responsive">
							<div class="alert alert-info">
								<i class="fa fa-info"></i>
								<b>Atenção!</b> Não tem qualquer cidade registada.
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</aside>