	<aside class="right-side"> 
		<section class="content-header">
			<h1> Entrada <small>Resumo</small> </h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i> Entrada</a></li>
				<li class="active">Resumo</li>
			</ol>
		</section>
		<section class="content"> 
			<div class="row">
				<div class="col-xs-6"> 
					<div class="small-box bg-aqua">
						<div class="inner">
							<h3><?=$this->estatisticas->get_total_eventos();?></h3>
							<p>Eventos</p>
						</div>
						<div class="icon"> <i class="ion ion-ios7-compose"></i> </div>
						<a href="<?=site_url('admin/eventos/listar/')?>" class="small-box-footer"> Ver todos <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<div class="col-xs-6"> 
					<div class="small-box bg-yellow">
						<div class="inner">
							<h3> <?=$this->estatisticas->get_total_reservas();?> </h3>
							<p> Reservas Registadas </p>
						</div>
						<div class="icon"> <i class="ion ion-person"></i> </div>
						<a href="<?=site_url('admin/reservas/listar/')?>" class="small-box-footer"> Ver todos <i class="fa fa-arrow-circle-right"></i> </a>
					</div>
				</div>
			</div>
		</section>
	</aside>