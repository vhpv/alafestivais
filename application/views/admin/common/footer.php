<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="<?=base_url('js/admin/jquery-ui-1.10.3.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/bootstrap.min.js')?>" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?=base_url('js/admin/plugins/morris/morris.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/sparkline/jquery.sparkline.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.j')?>s" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/fullcalendar/fullcalendar.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/jqueryKnob/jquery.knob.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/daterangepicker/daterangepicker.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/datepicker/bootstrap-datepicker.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/datetimepicker/bootstrap-datetimepicker.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/iCheck/icheck.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/uploadify/jquery.uploadify.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/input-mask/jquery.inputmask.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/input-mask/jquery.inputmask.date.extensions.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/plugins/input-mask/jquery.inputmask.extensions.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/AdminLTE/app.js')?>" type="text/javascript"></script>
<script src="<?=base_url('js/admin/AdminLTE/ala.js')?>" type="text/javascript"></script>
<?php
if(isset($aditional_jquery)) {
	print $aditional_jquery;
};

if(isset($aditional_jquery_page)) {
	print $aditional_jquery_page;
};
?>
</body>
</html>