<header class="header">
	<a href="<?=site_url('admin')?>" class="logo"><?=$this->config->item('admin_name');?></a>
	<nav class="navbar navbar-static-top" role="navigation"> 
		<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Menu</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
		<div class="navbar-right">
			<?php if($this->session->userdata('admin_logged_in') == true):?>
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i> <span><?=$this->session->userdata('nome')?> <i class="caret"></i></span> </a>
					<ul class="dropdown-menu">
						<li class="user-header bg-light-blue"> <img src="<?=base_url('img/admin/avatar5.png')?>" class="img-circle" alt="User Image" />
							<p><?=$this->session->userdata('nome')?></p>
						</li>
						<li class="user-footer">
							<?php /*<div class="pull-left"> <a href="#" class="btn btn-default btn-flat">Perfil</a> </div>*/?>
							<div class="pull-right"> <a href="<?=site_url('admin/home/logout')?>" class="btn btn-default btn-flat">Sair</a> </div>
						</li>
					</ul>
				</li>
			</ul>
			<?php endif; ?>
		</div>
	</nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left"> 