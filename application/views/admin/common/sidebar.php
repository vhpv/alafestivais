<aside class="left-side sidebar-offcanvas"> 
	<section class="sidebar"> 
		<ul class="sidebar-menu">
			<li><a href="<?=site_url('admin');?>"> <i class="fa fa-home"></i> <span>Entrada</span></a></li>
			<li><a href="<?=site_url('admin/autocarros')?>"><i class="fa fa-map-marker"></i> Autocarros</a></li>
			<li><a href="<?=site_url('admin/eventos')?>"><i class="fa fa-thumbs-o-up"></i> Eventos</a></li>
			<li><a href="<?=site_url('admin/reservas')?>"><i class="fa fa-shopping-cart"></i> <span>Reservas</span></a></li>
			<li><a href="<?=site_url('admin/cidades')?>"> <i class="fa fa-map-marker"></i> <span>Cidades</span></a></li>
			<li><a href="<?=site_url('admin/vouchers')?>"><i class="fa fa-tag"></i> Vouchers</a></li>
			<li><a href="<?=site_url('admin/juncoes')?>"><i class="fa fa-link"></i> Junções</a></li>
		</ul>
	</section>
</aside>