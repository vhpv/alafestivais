<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Painel de Administração | <?=$this->config->item('admin_title');?></title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link href="<?=base_url('css/admin/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/ionicons.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/morris/morris.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/jvectormap/jquery-jvectormap-1.2.2.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/fullcalendar/fullcalendar.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/daterangepicker/daterangepicker-bs3.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/datepicker/datepicker.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/datetimepicker/bootstrap-datetimepicker.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/uploadify.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('css/admin/AdminLTE.css')?>" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<?php $this->session->set_userdata('previous_page', uri_string());?>
</head>
<body class="skin-black">