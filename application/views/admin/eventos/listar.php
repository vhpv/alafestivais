<aside class="right-side"> 
	<section class="content-header">
		<h1><?=$title?></h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url('admin')?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li class="active"><?=$title?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Lista</h3>
						<div class="box-tools">
							<a href="<?=site_url('admin/eventos/adicionar')?>" class="btn btn-success btn-sm pull-right">Novo</a>
						</div>
					</div>
					<?=alerta()?>
					<?php if(count($eventos)): ?>
						<div class="box-body table-responsive">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>ID</th>
										<th>Titulo</th>
										<th class="text-center">Cidade</th>
										<th class="text-center">Referência</th>
										<th class="text-center">Acções</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($eventos as $evento):
									$eventos_filhos = $this->eventos->get_by_id_pai($evento->id);?>
									<tr>
										<td><?=$evento->id;?></td>
										<td><?=$evento->nome;?></td>
										<td><?=$this->cidades->get_by_id($evento->id_cidade)->nome;?></td>
										<td><?php if($evento->ref_local){
											print $this->cidades->get_by_id($evento->ref_local)->nome;
										} else {
											if(count($eventos_filhos)){
												print '----';
											} else {
												print '<span class="label label-danger">em falta</span>';
											}
										} ?></td>
										<td class="text-center"><a href="<?=site_url('admin/eventos/editar/'.$evento->id);?>" class="btn btn-warning"><small><i class="fa fa-pencil"></i></small></a> <a href="<?=site_url('admin/eventos/apagar/'.$evento->id);?>" class="btn btn-danger apagar"><small><i class="fa fa-trash-o"></i></small></a> <?php if($this->session->userdata('level') == 1):?><a class="btn btn-info<?php if(count($eventos_filhos)){print ' disabled';} ?>" href="<?=site_url('admin/autocarros/distribuir/'.$evento->id)?>"><i class="fa fa-bus"></i> Distribuir</a><?php endif;?></td>
									</tr>
									<?php
									
									if(count($eventos_filhos)){
										foreach($eventos_filhos as $evento_filho) {?>
										<tr>
											<td><?=$evento_filho->id;?></td>
											<td>|--- <?=$evento_filho->nome;?></td>
											<td><?=$this->cidades->get_by_id($evento_filho->id_cidade)->nome;?></td>
											<td><?php if($evento_filho->ref_local){
												print $this->cidades->get_by_id($evento_filho->ref_local)->nome;
											} else {
												print '<span class="label label-danger">em falta</span>';
											} ?></td>
											<td class="text-center"><a href="<?=site_url('admin/eventos/editar/'.$evento_filho->id);?>" class="btn btn-warning"><small><i class="fa fa-pencil"></i></small></a> <a href="<?=site_url('admin/eventos/apagar/'.$evento_filho->id);?>" class="btn btn-danger apagar"><small><i class="fa fa-trash-o"></i></small></a><?php if($this->session->userdata('level') == 1):?> <a class="btn btn-info" href="<?=site_url('admin/autocarros/distribuir')?>"><i class="fa fa-bus"></i> Distribuir</a><?php endif;?></td>
										</tr>
									<?php }
									} ?>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					<?php else: ?>
						<div class="box-body table-responsive">
							<div class="alert alert-info">
								<i class="fa fa-info"></i>
								<b>Atenção!</b> Não tem qualquer álbum criado.
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</aside>