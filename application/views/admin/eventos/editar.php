<aside class="right-side">
	<section class="content-header">
		<h1>Eventos</h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url()?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li><a href="<?=site_url('admin/eventos')?>">Eventos</a></li>
			<li class="active">Editar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<form role="form" method="post" action="<?=current_url()?>" enctype="multipart/form-data">
				<div class="col-xs-12 col-md-7">
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title">Editar</h3>
						</div>
						<div class="box-body">
							<?php if(isset($erro_upload)){
								foreach($erro_upload as $erro){
									print '<div class="callout callout-danger">' . $erro . '</div>';
								}
							} ?>
							<div class="row">
								<div class="col-xs-12"><?php echo validation_errors('<div class="callout callout-danger">','</div>'); ?></div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group">
										<label for="nome">Nome</label>
										<input type="text" placeholder="Nome do evento" name="nome" id="nome" class="form-control" value="<?=set_value('nome',$evento->nome);?>">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label for="descricao">Descrição</label>
										<textarea class="wysiwyg" placeholder="Descrição do produto" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="descricao" id="descricao"><?=set_value('descricao',$evento->descricao);?></textarea>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label for="imagem">Imagem</label>
										<input type="file" name="imagem" id="imagem">
										<p class="help-block">A imagem deve ter 300px x 300px.</p>
										<?php if($evento->image_file_name) { ?>
											<img src="<?=base_url('media/events/'.$evento->id.'/ticket/'.$evento->image_file_name)?>" width="100" />
										<?php }?>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label for="detail_image">Imagem interior</label>
										<input type="file" name="detail_image" id="detail_image">
										<p class="help-block">A imagem deve ter 750px x 300px.</p>
										<?php if($evento->detail_image) { ?>
											<img src="<?=base_url('media/events/'.$evento->id.'/detail/'.$evento->detail_image)?>" width="200" />
										<?php }?>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-primary" type="submit">Guardar</button>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="box box-success">
						<div class="box-header">
							<h3 class="box-title">Outros</h3>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label for="evento_pai">Evento Pai?</label>
										<select name="evento_pai" id="evento_pai" class="form-control">
											<option value="1" <?php echo set_select('evento_pai', 1, ($evento->evento_pai)?true:false); ?>>Sim</option>
											<option value="0" <?php echo set_select('evento_pai', 0, (!$evento->evento_pai)?true:false); ?>>Não</option>
										</select>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label for="activo">Activo</label>
										<select name="activo" id="activo" class="form-control">
											<option value="1" <?php echo set_select('activo', '1', ($evento->active)?true:false); ?>>Sim</option>
											<option value="0" <?php echo set_select('activo', '0', (!$evento->active)?true:false); ?>>Não</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 esconder-pai">
									<div class="form-group">
										<label for="evento_pai_id">Qual é o evento Pai</label>
										<select name="evento_pai_id" id="evento_pai_id" class="form-control">
											<option value="0" <?php echo set_select('evento_pai_id', 0); ?>>-- nenhum --</option>
											<?php foreach($eventos as $evento_filho):?>
											<option value="<?=$evento_filho->id?>" <?=set_select('evento_pai_id', $evento_filho->id, ($evento->evento_pai_id==$evento_filho->id)?true:false); ?>><?=$evento_filho->nome?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label for="id_cidade">Cidade</label>
										<select name="id_cidade" id="id_cidade" class="form-control">
											<?php foreach($cidades as $cidade):?>
											<option value="<?=$cidade->id?>" <?php echo set_select('id_cidade', $cidade->id, ($evento->id_cidade==$cidade->id)?true:false); ?>><?=$cidade->nome?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6">
									<div class="form-group">
										<label for="ref_local">Referência</label>
										<select name="ref_local" id="ref_local" class="form-control">
											<?php if(count($eventos_cidades)){ ?>
											<?php foreach($eventos_cidades as $cidade):?>
											<option value="<?=$cidade->id?>" <?php echo set_select('ref_local', $cidade->id, ($evento->ref_local==$cidade->id)?true:false); ?>><?=$cidade->nome?></option>
											<?php endforeach; ?>
											<?php } else { ?>
											<option value="0">-- SEM CIDADES --</option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label for="address_arrive">Local do evento</label>
										<input type="text" name="address_arrive" class="form-control" value="<?=set_value('address_arrive',$evento->address_arrive)?>" />
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Data de início:</label>
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="data_inicio" class="form-control pull-right datepicker" value="<?=set_value('data_inicio',$evento->data_inicio);?>" />
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Data de fim:</label>
										<div class="input-group">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" name="data_fim" class="form-control pull-right datepicker" value="<?=set_value('data_fim',$evento->data_fim);?>" />
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group">
										<label for="slide">Slide?</label>
										<div class="input-group">
											<div class="col-lg-2">
												<input type="checkbox" class="minimal" name="slide" value="1" />
											</div>
											<div class="col-lg-10">
												<input type="file" name="slide_foto_filename" id="slide_foto_filename">
											</div>
										</div>
										<p class="help-block">A imagem deve ter 960px x 465px.</p>
									</div>
								</div>
								<div class="col-xs-12 esconder-pai">
									<div class="form-group">
										<label for="payment_limit">Limite pagamento (horas)</label>
										<input type="text" placeholder="Limite pagamento em horas" name="payment_limit" id="payment_limit" class="form-control" value="<?=set_value('payment_limit',$evento->payment_limit);?>">
									</div>
								</div>
								<div class="col-xs-6 esconder-pai">
									<div class="form-group">
										<label for="go">Ida (%)</label>
										<input type="text" placeholder="%" name="go" id="go" class="form-control" value="<?=set_value('go',$evento->go);?>">
									</div>
								</div>
								<div class="col-xs-6 esconder-pai">
									<div class="form-group">
										<label for="come">Volta (%)</label>
										<input type="text" placeholder="%" name="come" id="come" class="form-control" value="<?=set_value('come',$evento->come);?>">
									</div>
								</div>
								<?php /*?><div class="col-xs-4 esconder-pai">
									<div class="form-group">
										<label for="go_come_dd">Dias diferentes</label>
										<input type="text" placeholder="%" name="go_come_dd" id="go_come_dd" class="form-control" value="<?=set_value('go_come_dd',$evento->go_come_dd);?>">
									</div>
								</div><?php */?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 esconder-pai">
					<div class="box box-primary">
						<div class="box-header"> <i class="fa fa-location-arrow"></i>
							<h3 class="box-title">Cidades disponíveis</h3>
						</div>
						<div class="box-body">
							<ul class="trajectoria lista">
								<?php foreach($eventos_cidades as $evento_cidade):?>
									<li>
										<span class="handle">
											<i class="fa fa-ellipsis-v"></i>
											<i class="fa fa-ellipsis-v"></i>
										</span>
										<select name="cidade[]" class="cidades form-control" data-identifier="1">
											<option value="">-- escolher--</option>
											<?php foreach($cidades as $cidade) { ?>
												<option value="<?=$cidade->id?>" <?=set_select('cidade', $cidade->id, ($cidade->id==$evento_cidade->id_cidade)?TRUE:''); ?>><?=$cidade->nome?></option>
											<?php }?>
										</select>
										<input name="valor[]" placeholder="Preço" class="form-control" size="4" value="<?=$evento_cidade->valor?>" />
										<input name="detalhes[]" placeholder="Detalhes" class="form-control" value="<?=$evento_cidade->detalhes?>" />
										<input name="hora_ida[]" placeholder="Horário ida" class="form-control" size="11" value="<?=$evento_cidade->hora_ida?>" />
										<input name="hora_volta[]" placeholder="Horário volta" class="form-control" size="12"  value="<?=$evento_cidade->hora_volta?>" />
										<div class="tools">
											<i class="fa fa-trash-o removepontotrajecto"></i>
										</div>
									</li>
								<?php endforeach;?>
							</ul>
						</div>
						<div class="box-footer clearfix no-border">
							<a class="btn btn-default pull-right addpontotrajecto" href="#"><i class="fa fa-plus"></i> Adicionar</a>
						</div>
					</div>
				</div>
			</form>
			<div class="cidade-escondida hidden">
				<li>
					<span class="handle">
						<i class="fa fa-ellipsis-v"></i>
						<i class="fa fa-ellipsis-v"></i>
					</span>
					<select name="cidade[]" class="cidades form-control" data-identifier="1">
						<option value="">-- escolher--</option>
						<?php foreach($cidades as $cidade) { ?>
							<option value="<?=$cidade->id?>"><?=$cidade->nome?></option>
						<?php }?>
					</select>
					<input name="valor[]" placeholder="Preço" class="form-control" size="4" required="required" />
					<input name="detalhes[]" placeholder="Detalhes" class="form-control" />
					<input name="hora_ida[]" placeholder="Horário ida" class="form-control" size="11" />
					<input name="hora_volta[]" placeholder="Horário volta" class="form-control" size="12" />
					<div class="tools">
						<i class="fa fa-trash-o removepontotrajecto"></i>
					</div>
				</li>
			</div>
		</div>
	</section>
</aside>