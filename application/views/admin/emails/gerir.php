<div id="page-content" class="clearfix">
<h1>E-mails</h1>
<h2>Listagem dos e-mails</h2>
  <div class="inner-box clearfix">
    <div id="table-block">
      
        <table cellspacing="0" cellpadding="0">
            <tbody>
                <thead>
					<th>Nome</th>
                    <th>E-mail</th>
                    <th>Data</th>
                </thead>
                
                <?php
                $i=0;
				if(isset($emails)): foreach($emails as $email): 
                $i++;?>
                <tr <?=($i%2==0)?"":"class=\"alternate\""?>>
                    <td><?=$email->nome?></td>
					<td><?=$email->email?></td>
					<td><?=$email->data?></td>
                </tr>
				<?php
                endforeach;
                else: ?>
                <strong>Nao existem conteúdos.</strong>
                <?php endif; ?>
            
            </tbody>
        </table>
		<?php echo $this->pagination->create_links(); ?> 

      </div>
  </div>
</div>