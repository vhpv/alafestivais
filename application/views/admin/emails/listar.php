<aside class="right-side"> 
	<section class="content-header">
		<h1>E-mails</h1>
		<ol class="breadcrumb">
			<li><a href="<?=site_url('admin')?>"><i class="fa fa-home"></i> Entrada</a></li>
			<li class="active">E-mails</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Listagem</h3>
					</div>
					<?php if(count($emails)): ?>
						<div class="box-body table-responsive">
							<table id="example2" class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Nome</th>
										<th>E-mail</th>
										<th>Data</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($emails as $email): ?>
									<tr>
										<td><?=$email->nome?></td>
										<td><?=$email->email?></td>
										<td><?=$email->data?></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
							<?php echo $this->pagination->create_links(); ?> 
						</div>
					<?php else: ?>
						<div class="box-body table-responsive">
							<div class="alert alert-info">
								<i class="fa fa-info"></i>
								<b>Atenção!</b> Não tem qualquer e-mail registado.
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
</aside>