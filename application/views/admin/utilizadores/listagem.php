<div id="page-content" class="clearfix">
	<h1>Administradores</h1>
	<h2>Listagem dos administradores <span>()</span></h2>
	<div class="inner-box clearfix">
		<div id="sidebar">
			<ul>
				<li class="head">Operações</li>
				<li class="current"><a href="<?php echo site_url('admin/utilizadores/listar');?>">Listar</a></li>
				<li><a href="<?php echo site_url('admin/utilizadores/criar');?>">Novo</a></li>
			</ul>
		</div>
		<div id="table-block">
        	<?php $total_users = $this->users_model->count_all();?>
			<table cellspacing="0" cellpadding="0">
					<tbody>
						<thead>
							<th>Administrador</th>
							<th>Data de criação</th>
							<th width="100">Acções</td>
						</thead>
						
						<?php
						$i=0;
						foreach($registos as $registo):
						$i++;?>
						<tr <?=($i%2==0)?"":"class=\"alternate\""?>>
							<td><?=$registo->pnome?>&nbsp;<?=$registo->unome?></td>
							<td><?=$registo->timestamp?></td>
							<td class="actions">
								<?php if($registo->user_id == 1){?><a class="edit" href="#">editar</a><?php }else{ ?><a class="edit" href="<?php echo site_url('admin/utilizadores/editar/'.$registo->user_id);?>">editar</a><?php } ?> | <?php if ($registo->user_id == 1){?><a href="#" class="delete">apagar</a><?php } else { ?><a class="delete" href="<?php echo site_url('admin/utilizadores/apagar/'.$registo->user_id);?>" onclick="return confirm('Esta operação irá remover <?=$registo->pnome?>&nbsp;<?=$registo->unome?> do Painel de Administração.\n\nPretende continuar?')">apagar</a><?php }?></td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php echo $this->pagination->create_links(); ?> 
		</div>
	</div>
</div>
