<div id="page-content" class="clearfix">
	<h1>Administradores</h1>
	<h2>Novo Administrador <span>( criação de conta de acesso)</span></h2>
	<div class="inner-box clearfix">
		<div id="sidebar">
			<ul>
				<li class="head">Operações</li>
				<li><a href="<?php echo site_url('admin/utilizadores/listar');?>">Listar</a></li>
				<li class="current"><a href="<?php echo site_url('admin/utilizadores/criar');?>">Novo</a></li>
			</ul>
		</div>
		<div id="form-block">

        <form enctype="multipart/form-data" method="post" action="<?=site_url('admin/utilizadores/criar/')?>" >
        <?php
        $pnome = array (
            'name' 	=> 'reg_pnome',
            'id'	=> 'reg_pnome',
            'value'	=> set_value('reg_pnome')
        );
    
        $unome = array (
            'name' 	=> 'reg_unome',
            'id'	=> 'reg_unome',
            'value'	=> set_value('reg_unome')
        );
    
        $email = array (
            'name' 	=> 'reg_email',
            'id'	=> 'reg_email',
            'value'	=> set_value('reg_email')
        );
        
        $username = array (
            'name' 	=> 'reg_username',
            'id'	=> 'reg_username',
            'value'	=> set_value('reg_username')
        );
        
        $password = array (
            'name' 	=> 'reg_password',
            'id'	=> 'reg_password',
            'value'	=> set_value('reg_password')
        );
        
        $password_conf = array (
            'name' 	=> 'reg_password_conf',
            'id'	=> 'reg_password_conf',
            'value'	=> set_value('reg_password_conf')
        );

		$button_enviar = array(
			'name' => 'inserir',
			'id' => 'inserir',
			'class' => '',
			'value' => 'true',
			'type' => 'submit',
			'content' => 'Registar'
		);
        
        ?>
            <?php
            echo validation_errors('<div class="box_msg error">','</div>');
        ?>
                <h1>Dados pessoais</h1>
                <div class="field">
                    <label>Primeiro nome</label>
                    <?php echo form_input($pnome); ?> </div>
                <div class="field">
                    <label>Último nome</label>
                    <?php echo form_input($unome); ?> </div>
                <div class="field">
                    <label>E-mail</label>
                    <?php echo form_input($email); ?></div>
                <h1>Dados de acesso</h1>
                <div class="field">
                    <label>Nome de utilizador</label>
                    <?php echo form_input($username); ?></div>
                <div class="field">
                    <label>Password</label>
                    <?php echo form_password($password); ?></div>
                <div class="field">
                    <label>Confirmar password</label>
                    <?php echo form_password($password_conf); ?></div>
            
            	<?php echo form_button($button_enviar); ?>
        </form>

		</div>
	</div>
</div>


