function actualiza_preco() {
	var posting = $.post('../eventos/price_update',{
			event:$('#reservation_event_id').val(),
			place:$('#reservation_place_id option:selected').val(),
			trip:$('#reservation_trip option:selected').val(),
			quantity:$('#reservation_quantity option:selected').val()
		}
	);
	posting.done(function(data) {
		$('#target_partida').html('');
		$('#target_volta').html('');
		var total_autocarros = data.autocarros;
		if(total_autocarros.length > 0){	
			$.each(data.autocarros, function(key, value) {
				$('#target_partida').append('<div>Local: '+value.detalhes+' | Hora: ' + value.hora_ida + '</div>');
				$('#target_volta').append('<div>Hora: ' + value.hora_volta + '</div>');
				$('#final_price_pp').text(value.preco_unitario);
				$('#final_price_total').text(value.preco_final);
				//se o valor final for maior que 100 obriga a facturar
				if(value.preco_final >= 100) {
					$('#dados_facturacao').show();
					$("#facturacao_nome").prop('required',true);
					$("#facturacao_nif").prop('required',true);
				} else {
					$('#dados_facturacao').hide();
					$("#facturacao_nome").prop('required',false);
					$("#facturacao_nif").prop('required',false);
				}
			});
		} else {
			$('#target_partida').html('Não há autocarros disponíveis para este destino');
		}
	});
	
	/*
	$("#distrito").change(function(){
		$.get("revendedores/get_concelhos",{distrito:$(this).val()}).done(function(data){
			$("#concelho").find('option').remove();  
			$.each(data.concelhos, function() {
				$("#concelho").append($("<option />").val(this.id).text(this.concelho));
			});
		});
		
	});
	*/
}
$("#reservation_quantity").change(actualiza_preco);
$("#reservation_place_id").change(actualiza_preco);
$('#reservation_trip').change(function() {
	var value = $("#reservation_trip").val();
	if (value == "Ida e volta"){
		$("#partida").show();
		$("#regresso").show();
	} else if (value == "Apenas ida"){
		$("#partida").show();
		$("#regresso").hide();
	} else if (value == "Apenas volta"){
		$("#partida").hide();
		$("#regresso").show();
	} else {
		$("#partida").hide();
		$("#regresso").hide();
	};
	actualiza_preco();
});


/*$(".removeFriend").bind( "click", function() {
	$(this).parent().parent().remove();
	return false;
});
*/

$('.listFriends').on('click','.removeFriend', function(){
	$(this).parent().parent().remove();
	return false;
});

$(".addFriend").click(function(e) {
	addFriend();
	return false;
});
function addFriend(){
	var numItems = $('.listFriends .row').length;
	if(numItems<9){
		var nova_linha= $('.hidden_row .row').clone();
		nova_linha.appendTo(".listFriends");
		var i=0;
		$('.cidades').each(function(){
			$(this).data('identifier',i++);
		});
	} else {
		alert('Só pode adicionar até 9 amigos');
	}
}