/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function() {
	"use strict";
	$(".apagar").click(function(e){
		if (confirm("Pretende confirmar esta acção?")) {
			return true;
		}
		return false;
	});
	$(".trajectoria").sortable({
		placeholder: "sort-highlight",
		handle: ".handle",
		forcePlaceholderSize: true,
		zIndex: 999999
	});
    //bootstrap WYSIHTML5 - text editor
    $(".wysiwyg").wysihtml5();
	$('.datepicker').datepicker({
		format: "yyyy-mm-dd",
		language: "pt",
		orientation: "bottom left",
		autoclose: true
	});
	$(".addpontotrajecto").click(function(e) {
		e.preventDefault();
		addPontoTrajecto();
	});
	
	function addPontoTrajecto(){
		var nova_linha= $('.cidade-escondida li').clone();
		nova_linha.appendTo(".trajectoria");
		var i=0;
		$('.cidades').each(function(){
			$(this).data('identifier',i++);
		});
	}
	
	$('.trajectoria').on('change','.cidades', function(){
		var seleccionados = $('.cidades');
		var value = $(this).val();
		for(var i=0; i<seleccionados.length; i++){
			if($(this).val()>0 && $(this).data('identifier') != $(seleccionados[i]).data('identifier')) {
				if(value == $(seleccionados[i]).val()) {
					$(this).val('');
					alert("Destino já seleccionado, por favor escolha outro.");
				}
			}
			
		}
	});
	
	$('.trajectoria').on('click','.removepontotrajecto', function(){
		var $location = $(this).parent().parent().parent().parent();
		$location.remove()
	});
	
	$('#evento_pai').on('change',this,function(){
		if($(this).val()==1){
			console.log('esconde');
			$('.esconder-pai').hide();
		} else {
			console.log('mostra');
			$('.esconder-pai').show();
		}
	});
	
	if($('#evento_pai').length){
		$('#evento_pai').change();
	}
	
});