$(document).ready(function(){
	
	
	//Show more events
	$(".jq_events_read_more").click(function(){
		$(this).toggleClass("opened");
		if($(this).is(".opened")){
			$('html,body').animate({ scrollTop: $(".jq_events_read_more").offset().top-120 }, 300);
			$(".last-batch").each(function(i,obj){
				setTimeout(function(){
					$(obj).fadeIn();
				},i*300);
			});
		}else{
			$(".last-batch").slideUp();
			//$('html,body').animate({ scrollTop: $(".last-batch").eq(0).offset().top-120 }, 300);
		}
	});
	
	
	//Mecanismo de search
	$(".search-module input").on("keyup",function(){
		var searchTerms = $(this).val();
		toggleSearch("show");
		$(".search-placeholder input").val(searchTerms).focus();
	});
	
	$(".search-backdrop,.search-container .close-search").on("click",function(){
		toggleSearch("hide");
	});
	
	$(".jq_toggle_search").click(function(){
		toggleSearch("show");
		$(".search-placeholder input").focus();
	});
	
	function toggleSearch(mode){
		if(mode=="show"){
			$(".search-backdrop").fadeIn();
			$(".search-container").fadeIn();
			$("body").addClass("blur");
		}else{
			$(".search-backdrop").fadeOut();
			$(".search-container").fadeOut();
			$("body").removeClass("blur");
		}
	}
	
	$(".search-placeholder input").on("keyup",function(){
		var searchTerms = $(this).val().toLowerCase();
		$(".search-module input").val(searchTerms);
		
		var eventsList = JSON.parse(localStorage.getItem("eventos"));
		
		//var eventsList = eval('(' + myObject + ')');
		
		if(searchTerms.length>2){
			$(".search-results").html("");
			
			for (var i = 0; i < eventsList.length; i++){
				var auxNome = eventsList[i].nome.toLowerCase();
				if (auxNome.indexOf(searchTerms)>-1){

					var data_inicio = eventsList[i].data_inicio;
					var aux_data_inicio = data_inicio.split("-");
					var data_inicio = aux_data_inicio[2]+"/"+aux_data_inicio[1]+"/"+aux_data_inicio[0];

					var template = '<a href="/~alafestivais/index.php/event/'+eventsList[i].id+'">';
					template += '<span class="title">'+eventsList[i].nome+'</span>';
					template += '<span class="date">'+data_inicio+'</span>';
					template += '<i class="material-icons">chevron_right</i>';
					template += '</a>';

					$(".search-results").append(template);

				}
			}
		}else{
			$(".search-results").html("");
		}
		
	});
	
	//Verificação de storage de lista completa de eventos para persistência e search
	if(localStorage.getItem("eventos")==null){
		$.get( "/~alafestivais/?blek_eventos", function( data ) {
			localStorage.setItem("eventos", JSON.stringify(data));
		});
	}
	
	//Toggle de lista de eventos em mobile
	$(".events-list").click(function(){
		$(this).toggleClass("open");
	});
	
	//Popular lista de partidas
	$(".departures-list li").each(function(){
		var elem = $(this);
		
		$.post(localStorage.getItem("baseUrl")+'/eventos/price_update',{
			event:$('#reservation_event_id').val(),
			place:elem.attr("value"),
			trip:"Ida e volta",
			quantity:"1"
		}).done(function(data){
			$.each(data.autocarros, function(key, value) {
				var detalhes = value.detalhes;
				var hora_ida = value.hora_ida.replace(":","h");
				$(".detalhes",elem).text(detalhes);
				$(".hora_ida",elem).text(hora_ida);
			});
		});
	})
	
	//Toggle de lista de partidas
	$(".departures-list .title").click(function(){
		$(this).parent().toggleClass("open");
	});
	$(".departures-list li").click(function(){
		$(".departures-list li").removeClass("active");
		$(this).addClass("active");
		$(this).parents(".departures-list").removeClass("open");
		$("#reservation_place_id").val($(this).attr("value"));
		
		localStorage.setItem("prefered_location",$(this).attr("value"));
		
		actualiza_preco();
	});
	
	
	//Open layer termos e condicoes
	$(".jq_termos_e_condicoes").click(function(e){
		e.preventDefault();
		$('html,body').animate({ scrollTop: 0 }, 200,function(){
			$("html").addClass("layer-open");
			$(".layer#termos").fadeIn(function(){
				$("body").addClass("blur");
			});
		});
	});
	//Open layer outro distrito
	$(".jq_district_not_found").click(function(e){
		e.preventDefault();
		$('html,body').animate({ scrollTop: 0 }, 200,function(){
			$("html").addClass("layer-open");
			$(".layer#district-not-found").fadeIn(function(){
				$("body").addClass("blur");
			});
		});
	});
	
	//Close Layer
	$(".layer-close").click(function(){
		$(this).parent().fadeOut(function(){
			$("html").removeClass("layer-open");
			$("body").removeClass("blur");
		});
	});
	
	
	//Change de valores na home
	$(".places span").click(function(){
		var elem = $(this);
		
		elem.siblings().removeClass("active");
		elem.addClass("active");
		
		var place = elem.attr("value");
		
		$.post(localStorage.getItem("baseUrl")+'/eventos/price_update',{
			event:elem.parents(".event").attr("data-event"),
			place:place,
			trip:"Ida e volta",
			quantity:"1"
		}).done(function(data){

			$.each(data.autocarros, function(key, value) {
				var priceElem = elem.parents(".event").find("h4");
				priceElem.addClass("pop").text(value.preco_unitario + " €");
				setTimeout(function(){
					priceElem.removeClass("pop")
				},200);
			});
		});
		
		localStorage.setItem("prefered_location",place);
		
		$(".event").each(function(){
			
			var elem = $(this);
			
			if($(this).find(".places span[value='"+place+"']").length > 0){
				$(".places span.active",elem).removeClass("active");
				$(".places span[value='"+place+"']",elem).addClass("active");
				
				$.post(localStorage.getItem("baseUrl")+'/eventos/price_update',{
					event:elem.attr("data-event"),
					place:place,
					trip:"Ida e volta",
					quantity:"1"
				}).done(function(data){

					$.each(data.autocarros, function(key, value) {
						var priceElem = $("h4",elem);
						priceElem.addClass("pop").text(value.preco_unitario + " €");
						setTimeout(function(){
							priceElem.removeClass("pop")
						},200);
					});
				});
			}
		});
		
	});
	
	// Defaults de homepage
	if($(".events-mosaic").length>0){
		
		if(localStorage.getItem("prefered_location")!=null){
			var prefered_location = localStorage.getItem("prefered_location");
			$(".places span[value='"+prefered_location+"']").eq(0).trigger("click");
		}
	}
	
	// Defaults de pagina de detalhe
	if($(".departures-list").length>0){
		actualiza_preco();
		if(localStorage.getItem("prefered_location")!=null){
			var prefered_location = localStorage.getItem("prefered_location");
			
			if($(".departures-list li[value='"+prefered_location+"']").length > 0){
				$(".departures-list li[value='"+prefered_location+"']").trigger("click");
			}else{
				$(".departures-list li:first-child").addClass("active");
			}
		}else{
			$(".departures-list li:first-child").addClass("active");
		}
	}
	
	//Menu active links
	var url = window.location.href;
	url = url.split("/");
	var final = url.slice(-1)[0];
	if(final=="porque"){
		$(".nav #porque").addClass("active");
	}else if(final=="como"){
		$(".nav #como").addClass("active");
	}else if(final=="historico"){
		$(".nav #historico").addClass("active");
	}else if(final=="contactos"){
		$(".nav #contactos").addClass("active");
	}else{
		$(".nav #eventos").addClass("active");
	}
	
	// Trigger de reservar
	$(".reservar").click(function(e){
		e.preventDefault();
		$("#reservation_first_name").focus();
	});
	
	// Efeito de abertura em reservar
	if($(".como-reservar").length>0){
		if($(window).width() > 768){
			var inview = new Waypoint.Inview({
				element: $('.como-reservar')[0],
				entered: function(direction) {
					console.log(this.element)
					$("li",this.element).each(function(i,obj){
						setTimeout(function(){
							$(obj).addClass("hover");
							setTimeout(function(){
								$(obj).removeClass("hover");	
							},300);
						},500*i);
					});
				}
			});
		}
	}
	
	
	//Validation hook for bootstrap
	$.validator.setDefaults({
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			if(element.parent('.input-group').length) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});
	
	$.validator.addMethod("regx", function(value, element, regexpr) {          
		return regexpr.test(value);
	}, "Deverá ser dd-mm-aaaa");
	
	//Validate Detalhe
	$("#detalhe").validate({
		rules: {
			first_name: "required",
			last_name: "required",
			phone: "required",
			birth_date: {
				regx : /^([0-9]{4}).([0-9]{2}).([0-9]{2})$/
			},
			facturacao_nome: {
				required: function(element) {
					var aux = $("#final_price_total").text();
					aux = aux.replace(" €","");

					return aux >= 100.00;
				}
			},
			facturacao_nif: {
				required: function(element) {
					var aux = $("#final_price_total").text();
					aux = aux.replace(" €","");

					return aux >= 100.00;
				},
				minlength: 9,
				maxlength: 9,
				digits: true
			},
			checkbox_termos: "required",
			email: {
				required: true,
				email: true
			},
		},

		messages: {
			first_name: "Por favor insere o teu primeiro nome.",
			last_name: "Por favor insere o teu último nome.",
			phone: "Por favor insere o teu número de telefone.",
			checkbox_termos: "Por favor confirma que leste e aceitas os Termos e Condições.",
			facturacao_nome: "Por favor insere um nome para faturação.",
			facturacao_nif: {
				required: "Por favor insere um NIF para faturação.",
				minlength: "Deve ter 9 dígitos.",
				maxlength: "Deve ter 9 dígitos.",
				digits: "Apenas pode conter dígitos"
			},
			email: "Por favor insere um e-mail válido."
		},

		submitHandler: function(form) {
			form.submit();
		}
	});
	
	//Validate Contactos
	$("#contactus").validate({

		rules: {
			name: "required",
			email: {
				required: true,
				email: true
			},
		},

		messages: {
			name: "Por favor insere o teu nome completo",
			email: "Por favor insere um e-mail válido."
		},

		submitHandler: function(form) {
			form.submit();
		}
	});
	
	//Validate pedido outro distrito
	$("#outro-distrito").validate({

		rules: {
			name: "required",
			telefone: "required",
			quantidade: {
				required: true,
				number: true
			},
			email: {
				required: true,
				email: true
			},
		},

		messages: {
			name: "Por favor insere o teu nome completo",
			telefone: "Por favor insere o teu nome completo",
			email: "Por favor insere um e-mail válido.",
			quantidade: "Por favor indica a quantidade de pessoas."
		},

		submitHandler: function(form) {
			form.submit();
		}
	});
	
	
});

function actualiza_preco() {
	//debugger;
	var posting = $.post(localStorage.getItem("baseUrl")+'/eventos/price_update',{
		event:$('#reservation_event_id').val(),
		place:$('#reservation_place_id option:selected').val(),
		trip:$('#reservation_trip option:selected').val(),
		quantity:$('#reservation_quantity option:selected').val()
	}
	);
	posting.done(function(data) {
		
		$('#target_partida').html('');
		$('#target_volta').html('');
		
		$("#sem-autocarros").hide();
		$("#com-autocarros").show();
		
		var total_autocarros = data.autocarros;
		if(total_autocarros.length > 0){	
			$.each(data.autocarros, function(key, value) {
				//value.detalhes
				//value.hora_ida
				var hora_ida = value.hora_ida.replace(":","h");
				var hora_volta = value.hora_volta.replace(":","h");
				
				if(hora_volta.indexOf(".")>-1){
					hora_volta = hora_volta.replace(".","");
				}
				
				$('#target_partida').append(hora_ida);
				$('#target_volta').append(hora_volta);
				$('#final_price_pp').text(value.preco_unitario + " €");
				$('#final_price_total').text(value.preco_final + " €");
				//se o valor final for maior que 100 obriga a facturar
				if(value.preco_final >= 100) {
					$('#dados_facturacao').show();
					$("#facturacao_nome").prop('required',true);
					$("#facturacao_nif").prop('required',true);
				} else {
					$('#dados_facturacao').hide();
					$("#facturacao_nome").prop('required',false);
					$("#facturacao_nif").prop('required',false);
				}
			});
		} else {
			$("#sem-autocarros").show();
			$("#com-autocarros").hide();
			$(".departures-list").hide();
			$(".reservar").hide();
		}
	});
	
	/*
	$("#distrito").change(function(){
		$.get("revendedores/get_concelhos",{distrito:$(this).val()}).done(function(data){
			$("#concelho").find('option').remove();  
			$.each(data.concelhos, function() {
				$("#concelho").append($("<option />").val(this.id).text(this.concelho));
			});
		});
		
	});
	*/
}
$("#reservation_quantity").change(actualiza_preco);
$("#reservation_place_id").change(function(){
	$(".departures-list li[value='"+$(this).val()+"']").trigger("click");
});
$('#reservation_trip').change(function() {
	var value = $("#reservation_trip").val();
	if (value == "Ida e volta"){
		$("#partida").show();
		$("#regresso").show();
	} else if (value == "Apenas ida"){
		$("#partida").show();
		$("#regresso").hide();
	} else if (value == "Apenas volta"){
		$("#partida").hide();
		$("#regresso").show();
	} else {
		$("#partida").hide();
		$("#regresso").hide();
	};
	actualiza_preco();
});


$('.listFriends').on('click','.removeFriend', function(){
	$(this).parent().parent().remove();
	return false;
});

$(".addFriend").click(function(e) {
	addFriend();
	return false;
});
function addFriend(){
	var numItems = $('.listFriends .row').length;
	if(numItems<9){
		var nova_linha= $('.hidden_row .row').clone();
		nova_linha.appendTo(".listFriends");
		var i=0;
		$('.cidades').each(function(){
			$(this).data('identifier',i++);
		});
	} else {
		alert('Só pode adicionar até 9 amigos');
	}
}